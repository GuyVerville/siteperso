<?php

namespace ProcessWire;


class HarmonicsPortalPage extends DefaultPage
{

    public function __construct(Template $tpl = null)
    {
        parent::__construct($tpl);
    }

    protected function getChartsByOwner()
    {
        $chartList = [];
        $statement = 'SELECT * FROM `birth_data` WHERE `owner_id` = :owner_id ORDER BY last_name';
        $query = $this->database()->prepare($statement);
        $query->bindValue(":owner_id", user()->id, \PDO::PARAM_INT);
        try {
            $query->execute();
            $result = $query->rowCount();
            if ($result !== 0) {
                while ($row = $query->fetchObject()) {
                    $chartList[] = [$row->id, $row->last_name, $row->first_name, $row->category, $row->archive_name];
                }
            }
        } catch (\Exception $e) {
            $result = 0;
        }
        return $chartList;
    }
}