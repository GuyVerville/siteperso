<?php

namespace ProcessWire;
// https://code.tutsplus.com/tutorials/how-to-authenticate-users-with-twitter-oauth-20--cms-25713

use Abraham\TwitterOAuth\TwitterOAuth;

class LoginTwitterV2Page extends DefaultPage
{
    protected $loginSettings = [];
    protected $twitterSettings = [];
    protected $twitterToken = [];

    public function __construct(Template $tpl = null)
    {
        parent::__construct($tpl);
    }

    public function getTwitterCredentials()
    {
        $this->getTwitterLogin();

        // create TwitterOAuth object
        $twitteroauth = new TwitterOAuth($this->loginSettings['consumer_key'], $this->loginSettings['consumer_secret']);
        $twitteroauth->setApiVersion('2');

        // request token of application
        $request_token = $twitteroauth->oauth(
            'oauth/request_token', [
                'oauth_callback' => $this->loginSettings['url_callback']
            ]
        );

        // throw exception if something gone wrong
        if ($twitteroauth->getLastHttpCode() != 200) {
            throw new \Exception('There was a problem performing this request');
        }

        // save token of application to session
        session()->set('oauth_token', $request_token['oauth_token']);
        session()->set('oauth_token_secret', $request_token['oauth_token_secret']);

        // generate the URL to make request to authorize our application
        $url = $twitteroauth->url(
            'oauth/authorize', [
                'oauth_token' => $request_token['oauth_token']
            ]
        );
        // and redirect
        session()->redirect($url);
    }

    public function callbackTwitter()
    {
        $this->getTwitterLogin();
        $oauthToken = session()->get('oauth_token');
        $oauthTokenSecret = session()->get('oauth_token_secret');
        $oauth_verifier = input()->get('oauth_verifier');

        // something's missing, go and login again
        if (empty($oauth_verifier) || empty($oauthToken) || empty($oauthTokenSecret)) {
            session()->redirect($this->loginSettings['url_login']);
        }
        // connect with application token
        $connection = new TwitterOAuth(
            $this->loginSettings['consumer_key'],
            $this->loginSettings['consumer_secret'],
            $oauthToken,
            $oauthTokenSecret
        );
        $connection->setApiVersion('2');
        // request user token
        $this->twitterToken = $connection->oauth('oauth/access_token', ['oauth_verifier' => $oauth_verifier]);
        $this->saveToken();
        $connection = new TwitterOAuth(
            $this->loginSettings['consumer_key'],
            $this->loginSettings['consumer_secret'],
            $this->twitterToken['oauth_token'],
            $this->twitterToken['oauth_token_secret']
        );
        $connection->setApiVersion('2');
        $message = "Votre compte Twitter est maintenant associé à votre site web.";
        $status = $connection->post(
            "statuses/update", [
                "status" => $message
            ]
        );
        wire()->message($message);
        $url = page()->editURL;
        session()->redirect($url);
    }

    protected function getTwitterLogin()
    {
        $this->loginSettings = [
            'consumer_key' => page()->consumer_token,
            'consumer_secret' => page()->consumer_token_secret,
            'url_login' => config()->twitterlogin,
            'url_callback' => config()->twittercallback
        ];
    }


    protected function saveToken(){
        page()->of(false);
        page()->oauth_token = $this->twitterToken['oauth_token'];
        page()->oauth_token_secret = $this->twitterToken['oauth_token_secret'];
        page()->save();
        page()->of(true);
    }

}