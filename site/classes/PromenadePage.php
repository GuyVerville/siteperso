<?php

namespace ProcessWire;

use Abraham\TwitterOAuth\TwitterOAuth;

class PromenadePage extends DefaultPage
{
    protected $french;
    protected $english;
    protected $twitterStatus = [];

    public function postTweetUpdate()
    {
       $credentials = pages()->get('name=twitter-connexion-v2');
        $consumer_key = $credentials->consumer_token;
        $consumer_secret = $credentials->consumer_token_secret;
        $access_token = $credentials->oauth_token;
        $access_token_secret = $credentials->oauth_token_secret;

        $this->setTwitterStatus();
        /* media to upload */
        $mediaToUpload[] = page()->images->first()->width(500)->filename();

        foreach ($this->twitterStatus as $twitterStatus) {
            /* init API */
            $connection = new TwitterOAuth($consumer_key, $consumer_secret, $access_token, $access_token_secret);

            $mediaIDS = [];

            foreach ($mediaToUpload as $key => $media_path) {
                /* Upload media to twitter API and get media ID back */
                $mediaOBJ = $connection->upload('media/upload', ['media' => $media_path]);
                /* push uploaded media ID to array */
                array_push($mediaIDS, $mediaOBJ->media_id_string);
            }

            /* create comma delimited list of media ID:s */
            $mediaIDstr = implode(',', $mediaIDS);

            /* API params */
            $arrayCfg['status'] = $twitterStatus;
            $arrayCfg['media_ids'] = $mediaIDstr;
            $statuses = $connection->post("statuses/update", $arrayCfg);
        }

        if ($statuses->id != "") {
            wire('log')->save('twitter', 'Tweet publié. ' . $statuses->id);
            page()->of(false);
            page()->mc_date_sent = time();
            page()->mc_send_campaign = 0;
            page()->save(['noHooks' => true]);
            page()->of(true);
            $url = page()->editURL();
            wire()->warning('Tweet publié!');
            session()->redirect($url);
        }
    }

    protected function setTwitterStatus()
    {
        $taxonomiesFr = "";
        $taxonomiesEn = "";

        $this->french = languages()->get('fr');
        $this->english = languages()->get('default');

        $summaryFr = $this->getTruncated(page()->getLanguageValue($this->french, 'summary'));
        $summaryEn = $this->getTruncated(page()->getLanguageValue($this->english, 'summary'));

        $taxos = page()->taxonomie->sort('title');
        foreach ($taxos as $taxo) {
            $taxonomiesFr .= "#" . $taxo->getLanguageValue($this->french, 'title');
            $taxonomiesEn .= "#" . $taxo->getLanguageValue($this->english, 'title');
        }
        $taxonomiesFr = str_replace(" ", "", $taxonomiesFr);
        $taxonomiesEn = str_replace(" ", "", $taxonomiesEn);
        $taxonomiesFr = str_replace("#", " #", $taxonomiesFr);
        $taxonomiesEn = str_replace("#", " #", $taxonomiesEn);

        $this->twitterStatus = [
            'fr' => $summaryFr . $taxonomiesFr . PHP_EOL . page()->localHttpUrl($this->french),
            'default' => $summaryEn . $taxonomiesEn . PHP_EOL . page()->localHttpUrl($this->english)
        ];
    }

    protected function getTruncated($str = ""): string
    {
        $st = str_replace("&#039;", "’", $str);
        return sanitizer()->trunc($st, 120) . '...' . PHP_EOL;
    }
}