<?php namespace ProcessWire;


class DefaultPage extends Page
{
    public function __construct(Template $tpl = null)
    {
        parent::__construct($tpl);
    }

    /**
     * @return string
     */
    public function getSEO(): string
    {
        /** @var ProcessPageView $view */
        switch (page()->template->name) {
            case "rss":
            case "structured_data_breadcrumb":
            case "harmonics":
            case "harmonicsPortal":
            case "askswisseph":
            case "svg":
            case "test":
            case "planet":
            case "variable":
            case "variables":
            case "login-twitter":
                return "";
                break;
            default:
                return page()->seo->render();
                break;
        }
    }

    /**
     * Return the AdminLinksInFrontEnd markup if logged in
     * @param string $admin
     * @return string
     * @throws WirePermissionException
     */
    public function getAdminLinks(string $admin = ""): string
    {
        if (modules()->isInstalled("AdminLinksInFrontend") === true && user()->isSuperuser()) {
            $admin = modules()->get("AdminLinksInFrontend")->render();
        }
        return $admin;
    }

    /**
     * @return string
     * @throws WireException
     */
    protected function getLanguageSwitcher(): string
    {
        $languages = $this->wire("languages");
        $html = "";
        foreach ($languages as $language) {
            if (page()->viewable($language) === false) {
                continue;
            }
            $name = $language->name;
            if (user()->language->name == $name) {
                continue;
            }
            $html .= '<a href="' . page()->localUrl($language) . '" class="language">' . $this->getLanguageCode($name) . '</a>';
        }
        return $html;
    }

    /**
     * Get user language code
     * @param string $languageName
     * @return string
     */
    public function getLanguageCode(string $languageName)
    {
        $code = "";
        if ($languageName === "fr") {
            $code = "fr";

        } else if ($languageName === "default") {
            $code = "en";
        }
        return $code;
    }

    /**
     * @return string
     */
    public function getUserLanguage()
    {
        return $this->getLanguageCode(user()->language->name);
    }

    /**
     * @param $id
     * @return string
     * @throws WireException
     */
    private function makeALi($id): string
    {
        $p = pages()->get($id);
        return "<li><a href='{$p->url}'>{$p->title}</a>";
    }

    public function getMenu(): string
    {
        $html = "<ul id='pageMenu'>";
        $html .= $this->makeALi(1001);
        $html .= $this->makeALi(1006);
        $html .= $this->makeALi(1009);
        $html .= $this->makeALi(2023);
        $html .= $this->makeALi(1000);
        $html .= "</ul>";
        $html .= "<div id='belowMenu'><div id='language-switcher'>";
        $html .= $this->getLanguageSwitcher();
        $html .= "</div>";
        if (page()->id > 1) {
            $url = pages()->get(1)->url;
            $html .= '<div id="homeIcon"><a href="' . $url . '"><img class="desktop" src="/site/templates/visuels/svg/home.svg" alt="Going home"/><img class="mobile" src="/site/templates/visuels/svg/home-w.svg" alt="Going home"/></a></div>';
        }
        if (page()->id != 1) {
            $html .= '<div id="closeMenu"><img class="desktop" src="/site/templates/visuels/svg/close.svg" /><img class="mobile" src="/site/templates/visuels/svg/close-w.svg" /></div>';
        }
        $html .= "</div>";
        return $html;
    }

    /**
     * @return object
     * @throws WireException
     */
    public function getTranslations(): object
    {
        return pages()->get("template=translations")->translated_texts;
    }

    /**
     * @return string
     */
    public function getBodyClass(): string
    {
        switch (page()->id) {
            case "1":
                $c = "home";
                break;
            default:
                $c = "regularPage";
                break;
        }
        $c .= " " . page()->template->name;
        return $c;
    }

    /**
     * @param $image
     * @return string
     */
    private function checkIfDescription($image): string
    {
        return 'alt="' . $image->description . '"';
    }


    /**
     * @param $image
     * @param string $class
     * @param string $alt
     * @param string $width
     * @param string $height
     * @param false $crop
     * @return string
     */
    protected function formatSrcImg($image, $class = "", $width = "auto", $height = "auto", $crop = false): string
    {
        if ($width === "auto") {
            $width_s = 330;
            $width_m = 650;
            $width_l = 800;
        }
        if ($height === "auto") {
            $height_s = 216;
            $height_m = 425;
            $height_l = 524;
        }
        if ($class !== "") {
            $class = " class='" . $class . "'";
        }
        $alt = $this->makeTimestampAlt();
        $html = '<picture>';
        if ($crop) {
            $html .= '<source type="image/webp" srcset= "';
            $html .= $image->size($width_s, $height_s)->webp->url . ' 300w, ';
            $html .= $image->size($width_m, $height_m)->webp->url . ' 600w, ';
            $html .= $image->size($width_l, $height_l)->webp->url . ' 1000w" />';
            $html .= '<source srcset= "';
            $html .= $image->size($width_s, $height_s)->url . ' 300w, ';
            $html .= $image->size($width_m, $height_m)->url . ' 600w, ';
            $html .= $image->size($width_l, $height_l)->url . ' 1000w" />';
            $html .= '<img loading="lazy" itemprop="sharedContent" src="';
            $html .= $image->size($width_l, $height_l)->url . '"' . $class . $alt . '>';
        } else {
            $html .= '<source type="image/webp" srcset= "';
            $html .= $image->width($width_s)->webp->url . ' 300w, ';
            $html .= $image->width($width_m)->webp->url . ' 600w, ';
            $html .= $image->width($width_l)->webp->url . ' 1000w" />';
            $html .= '<source srcset= "';
            $html .= $image->width($width_s)->url . ' 300w, ';
            $html .= $image->width($width_m)->url . ' 600w, ';
            $html .= $image->width($width_l)->url . ' 1000w" />';
            $html .= '<img loading="lazy" itemprop="sharedContent" src="';
            $html .= $image->width($width_l)->url . '"' . $class . $alt . '>';
        }
        $html .= '</picture>';
        return $html;
    }


    /**
     * @param $image
     * @return string
     */
    protected function pictureThumbnail($image)
    {
        $html = "";
        if ($image) {
            $html = '<picture>';
            $html .= '<source  type="image/webp" srcset= "' . $image->webp->URL . '" >';
            $html .= '<source  srcset= "' . $image->URL . '" >';
            $html .= '<img loading="lazy" src="' . $image->URL . ' alt="" /></picture>';
        }
        return $html;
    }


    /**
     * @param $image
     * @return string
     */
    protected function sizedThumbnail($image, $width = 50, $height = 50)
    {
        $html = "";
        if ($image) {
            $html = '<picture>';
            $html .= '<source  type="image/webp" srcset= "' . $image->size($width, $height)->webp->URL . ' />';
            $html .= '<source srcset= "' . $image->size($width, $height)->URL . ' />';
            $html .= '<img loading="lazy" src="' . $image->size($width, $height)->URL . ' alt="" /></picture>';
        }
        return $html;
    }

    /**
     * @param $image
     * @return string
     */
    public function uneImageMeta($image): string
    {
        return '<img itemprop="image" src="' . $image->URL . '" width="' . $image->width . '" height="' . $image->height . '" ' . $this->checkIfDescription($image) . '>';
    }

    public function couleurPrincipale(): string
    {
        if (page()->couleur_principale) {
            $c = '#' . page()->couleur_principale;
        } else {
            $c = '#1a3958';
        }
        return $c;
    }

    public function couleurSecondaire(): string
    {
        if (page()->couleur_secondaire) {
            $c = '#' . page()->couleur_secondaire;
        } else {
            $c = '#1a3958';
        }
        return $c;
    }

    public function replaceAccents($str)
    {
        $a = [
            'À' => 'A',
            'Á' => 'A',
            'Â' => 'A',
            'Ã' => 'A',
            'Ä' => 'A',
            'Å' => 'A',
            'Æ' => 'AE',
            'Ç' => 'C',
            'È' => 'E',
            'É' => 'E',
            'Ê' => 'E',
            'Ë' => 'E',
            'Ì' => 'I',
            'Í' => 'I',
            'Î' => 'I',
            'Ï' => 'I',
            'Ð' => 'D',
            'Ñ' => 'N',
            'Ò' => 'O',
            'Ó' => 'O',
            'Ô' => 'O',
            'Õ' => 'O',
            'Ö' => 'O',
            'Ø' => 'O',
            'Ù' => 'U',
            'Ú' => 'U',
            'Û' => 'U',
            'Ü' => 'U',
            'Ý' => 'Y',
            'ß' => 's',
            'à' => 'a',
            'á' => 'a',
            'â' => 'a',
            'ã' => 'a',
            'ä' => 'a',
            'å' => 'a',
            'æ' => 'ae',
            'ç' => 'c',
            'è' => 'e',
            'é' => 'e',
            'ê' => 'e',
            'ë' => 'e',
            'ì' => 'i',
            'í' => 'i',
            'î' => 'i',
            'ï' => 'i',
            'ñ' => 'n',
            'ò' => 'o',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ö' => 'o',
            'ø' => 'o',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'ü' => 'u',
            'ý' => 'y',
            'ÿ' => 'y',
            'Ā' => 'A',
            'ā' => 'a',
            'Ă' => 'A',
            'ă' => 'a',
            'Ą' => 'A',
            'ą' => 'a',
            'Ć' => 'C',
            'ć' => 'c',
            'Ĉ' => 'C',
            'ĉ' => 'c',
            'Ċ' => 'C',
            'ċ' => 'c',
            'Č' => 'C',
            'č' => 'c',
            'Ď' => 'D',
            'ď' => 'd',
            'Đ' => 'D',
            'đ' => 'd',
            'Ē' => 'E',
            'ē' => 'e',
            'Ĕ' => 'E',
            'ĕ' => 'e',
            'Ė' => 'E',
            'ė' => 'e',
            'Ę' => 'E',
            'ę' => 'e',
            'Ě' => 'E',
            'ě' => 'e',
            'Ĝ' => 'G',
            'ĝ' => 'g',
            'Ğ' => 'G',
            'ğ' => 'g',
            'Ġ' => 'G',
            'ġ' => 'g',
            'Ģ' => 'G',
            'ģ' => 'g',
            'Ĥ' => 'H',
            'ĥ' => 'h',
            'Ħ' => 'H',
            'ħ' => 'h',
            'Ĩ' => 'I',
            'ĩ' => 'i',
            'Ī' => 'I',
            'ī' => 'i',
            'Ĭ' => 'I',
            'ĭ' => 'i',
            'Į' => 'I',
            'į' => 'i',
            'İ' => 'I',
            'ı' => 'i',
            'Ĳ' => 'IJ',
            'ĳ' => 'ij',
            'Ĵ' => 'J',
            'ĵ' => 'j',
            'Ķ' => 'K',
            'ķ' => 'k',
            'Ĺ' => 'L',
            'ĺ' => 'l',
            'Ļ' => 'L',
            'ļ' => 'l',
            'Ľ' => 'L',
            'ľ' => 'l',
            'Ŀ' => 'L',
            'ŀ' => 'l',
            'Ł' => 'l',
            'ł' => 'l',
            'Ń' => 'N',
            'ń' => 'n',
            'Ņ' => 'N',
            'ņ' => 'n',
            'Ň' => 'N',
            'ň' => 'n',
            'ŉ' => 'n',
            'Ō' => 'O',
            'ō' => 'o',
            'Ŏ' => 'O',
            'ŏ' => 'o',
            'Ő' => 'O',
            'ő' => 'o',
            'Œ' => 'OE',
            'œ' => 'oe',
            'Ŕ' => 'R',
            'ŕ' => 'r',
            'Ŗ' => 'R',
            'ŗ' => 'r',
            'Ř' => 'R',
            'ř' => 'r',
            'Ś' => 'S',
            'ś' => 's',
            'Ŝ' => 'S',
            'ŝ' => 's',
            'Ş' => 'S',
            'ş' => 's',
            'Š' => 'S',
            'š' => 's',
            'Ţ' => 'T',
            'ţ' => 't',
            'Ť' => 'T',
            'ť' => 't',
            'Ŧ' => 'T',
            'ŧ' => 't',
            'Ũ' => 'U',
            'ũ' => 'u',
            'Ū' => 'U',
            'ū' => 'u',
            'Ŭ' => 'U',
            'ŭ' => 'u',
            'Ů' => 'U',
            'ů' => 'u',
            'Ű' => 'U',
            'ű' => 'u',
            'Ų' => 'U',
            'ų' => 'u',
            'Ŵ' => 'W',
            'ŵ' => 'w',
            'Ŷ' => 'Y',
            'ŷ' => 'y',
            'Ÿ' => 'Y',
            'Ź' => 'Z',
            'ź' => 'z',
            'Ż' => 'Z',
            'ż' => 'z',
            'Ž' => 'Z',
            'ž' => 'z',
            'ſ' => 's',
            'ƒ' => 'f',
            'Ơ' => 'O',
            'ơ' => 'o',
            'Ư' => 'U',
            'ư' => 'u',
            'Ǎ' => 'A',
            'ǎ' => 'a',
            'Ǐ' => 'I',
            'ǐ' => 'i',
            'Ǒ' => 'O',
            'ǒ' => 'o',
            'Ǔ' => 'U',
            'ǔ' => 'u',
            'Ǖ' => 'U',
            'ǖ' => 'u',
            'Ǘ' => 'U',
            'ǘ' => 'u',
            'Ǚ' => 'U',
            'ǚ' => 'u',
            'Ǜ' => 'U',
            'ǜ' => 'u',
            'Ǻ' => 'A',
            'ǻ' => 'a',
            'Ǽ' => 'AE',
            'ǽ' => 'ae',
            'Ǿ' => 'O',
            'ǿ' => 'o'
        ];
        if (key_exists($str, $a)) {
            $str = $a[$str];
        }
        return $str;
    }

    protected function makeTimeStampAlt()
    {
        return ' alt ="altPicture' . rand() . '"';
    }

    protected function renderPagination($pps)
    {
        return $pps->renderPager(array(
            'nextItemLabel' => "Next",
            'previousItemLabel' => "Prev",
            'listMarkup' => "<ul class='pagination'>{out}</ul>",
            'itemMarkup' => "<li class='{class}'>{out}</li>",
            'linkMarkup' => "<a href='{url}'>{out}</a>",
            'currentItemClass' => "active"
        ));
    }
}
