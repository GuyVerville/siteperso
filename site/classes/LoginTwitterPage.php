<?php

namespace ProcessWire;
// https://code.tutsplus.com/tutorials/how-to-authenticate-users-with-twitter-oauth-20--cms-25713
use Abraham\TwitterOAuth\TwitterOAuth;

class LoginTwitterPage extends DefaultPage
{
    protected $loginSettings = [];
    protected $twitterSettings = [];
    protected $twitterToken = [];

    public function __construct(Template $tpl = null)
    {
        parent::__construct($tpl);
        $this->getSettings();
    }

    public function loginTwitter()
    {
        $this->getTwitterLogin();

        // create TwitterOAuth object
        $twitteroauth = new TwitterOAuth($this->loginSettings['consumer_key'], $this->loginSettings['consumer_secret']);

        // request token of application
        $request_token = $twitteroauth->oauth(
            'oauth/request_token', [
                'oauth_callback' => $this->loginSettings['url_callback']
            ]
        );
        // throw exception if something gone wrong
        if ($twitteroauth->getLastHttpCode() != 200) {
            throw new \Exception('There was a problem performing this request');
        }

        // save token of application to session
        session()->set('oauth_token', $request_token['oauth_token']);
        session()->set('oauth_token_secret', $request_token['oauth_token_secret']);

        // generate the URL to make request to authorize our application
        $url = $twitteroauth->url(
            'oauth/authorize', [
                'oauth_token' => $request_token['oauth_token']
            ]
        );
        // and redirect
        session()->redirect($url);
    }

    public function callbackTwitter()
    {
        $this->getTwitterLogin();
        $oauthToken = session()->get('oauth_token');
        $oauthTokenSecret = session()->get('oauth_token_secret');
        $oauth_verifier = input()->get('oauth_verifier');

        // something's missing, go and login again
        if (empty($oauth_verifier) || empty($oauthToken) || empty($oauthTokenSecret)) {
            session()->redirect($this->loginSettings['url_login']);
        }
        // connect with application token
        $connection = new TwitterOAuth(
            $this->loginSettings['consumer_key'],
            $this->loginSettings['consumer_secret'],
            $oauthToken,
            $oauthTokenSecret
        );

        // request user token
        $this->twitterToken = $connection->oauth('oauth/access_token', ['oauth_verifier' => $oauth_verifier]);
        $this->saveToken();
        $twitter = new TwitterOAuth(
            $this->loginSettings['consumer_key'],
            $this->loginSettings['consumer_secret'],
            $this->twitterToken['oauth_token'],
            $this->twitterToken['oauth_token_secret']
        );
        $message = "Votre compte Twitter est maintenant associé à votre site web.";
        $status = $twitter->post(
            "statuses/update", [
                "status" => $message
            ]
        );
        wire()->message($message);
        $url = page()->editURL;
        session()->redirect($url);
    }

    protected function getTwitterLogin()
    {
        $this->loginSettings = [
            'consumer_key' => $this->twitterSettings['consumer_key'],
            'consumer_secret' => $this->twitterSettings['consumer_secret'],
            'url_login' => 'https://' . config()->httpHost . '/en/connexion-twitter/twitter-login',
            'url_callback' => 'https://' . config()->httpHost . '/en/connexion-twitter/twitter-callback'
        ];
    }

    protected function getSettings()
    {
        $this->twitterSettings = [
            'oauth_access_token' => "1442905291591323648-M4zg7hTqsCeYEvELvv4PCn9c39xrQX",
            'oauth_access_token_secret' => "DHVRtK20J5ckninGBA12BEknfSzvjgmdV5ITfbCDgr4AZ",
            'consumer_key' => "Wlze7wHn4njikwkTswc1RIO2M",
            'consumer_secret' => "m9m3qLqFZa6DIaJNck8dyxoQuRQtAuIw9gEtRkbu4QDVQe2LpW"
        ];

    }

    protected function saveToken(){
        page()->of(false);
        page()->oauth_token = $this->twitterToken['oauth_token'];
        page()->oauth_token_secret = $this->twitterToken['oauth_token_secret'];
        page()->save();
        page()->of(true);
    }

    protected function getToken(){

    }
}