<?php


namespace ProcessWire;

use DateTime;
use DateTimeZone;

class HarmonicsPage extends DefaultPage
{
    protected $variables = [];
    protected $results = [];
    protected $msg = '';
    protected $errors = [];
    protected $error = false;
    protected $angularities = [];
    protected $transitAngularities = [];
    protected $synastryAngularities = [];
    protected $existing = false;
    protected $signSymbols = ["a", "s", "d", "f", "g", "h", "j", "k", "l", "v", "x", "c"];
    protected $planetSymbols = [];
    protected $options = [];
    protected $addition = [];
    protected $latt = "";
    protected $lngg = "";
    protected $choosenPoints = [];
    protected $harmonicRanges = [];

    public function __construct(Template $tpl = null)
    {
        parent::__construct($tpl);
        $this->errors = ['date' => '', 'time' => '', 'lat' => '', 'lng' => '', 'timezone' => ''];
        $this->planetSymbols = $this->getPlanets();
        $this->choosenPoints = $this->getPreferences();
    }

    protected function getPlanets()
    {
        return modules()->get('RockFinder3')
            ->find('template=planet,sort=sort')
            ->addColumns(['id', 'title', 'symbol', 'sort'])
            ->getRowArray();
    }

    public function getPreferences()
    {
        $pr = session()->get('points');
        if ($pr === null) {
            $pr = $userPr = user()->preferences;
            if ($userPr === '') {
                user()->preferences = '{"points":[2088,2070,2071,2072,2073,2074,2075,2076,2077,2078,2079]}';
                $pr = user()->preferences;
                user()->save();
            }
            session()->set('points', $pr);
        }
        $pr = json_decode($pr, true);
        $pr = $pr["points"];

        return $pr;
    }

    /**
     * @return DateTime
     * @throws \Exception
     */
    protected function getNow()
    {
        $tz = new DateTimeZone('UTC');
        try {
            return new DateTime('now', $tz);
        } catch (Exception $e) {
        }
    }

    /**
     * @return mixed
     */
    protected function sendSweph()
    {
        $options = array_merge($this->options, $this->addition);


        $http = new WireHttp();
        return json_decode($http->get("https://webspiria.com/natal.php", $options), true);
    }

    /**
     * @return array
     */
    protected function getSweph()
    {
        $this->addition = [
            'pl' => 'FDIGHs -xs90377',
            'ast' => 0
        ];
        $output1 = $this->sendSweph();
        $this->addition = [
            'pl' => 's -xs433',
            'ast' => 0
        ];
        $output2 = $this->sendSweph();

        $this->addition = [
            'pl' => 's -xs10', // Hygiea
            'ast' => "0"
        ];
        $output = $this->sendSweph();

        /*$this->addition = [
            'pl' => 's -xs136199', // Eris
            'ast' => "0"
        ];
        $output3 = $this->sendSweph();*/

        $this->addition = [
            'ast' => "1"
        ];
        $outputHouses = $this->sendSweph();

        return array_merge($outputHouses, $output1, $output2, $output);
        // return array_merge($outputHouses, $output1, $output2, $output, $output3);

    }

    /**
     * @param $out
     * @return array[]
     */
    protected function sortPlanetsAndHouses($out)
    {
        $ct = 0;
        $ctb = 0;
        foreach ($out as $key => $line) {
            $row = explode(',', $line);
            if (isset($row[2])) {
                $h = trim($row[2]);
            } else {
                $h = "";
            }
            if ($ct < 12 && $ct != 10) {
                $planets[] = ["name" => trim($row[0]), "position" => (float)$row[1], "house" => $h, "symbol" => $this->planetSymbols[$ctb]->symbol, "id" => $this->planetSymbols[$ctb]->id];
                $ctb++;
            } elseif ($ct > 12 && $ct < 25) {
                $houses[] = ["name" => trim($row[0]), "position" => (float)$row[1]];
            } elseif ($ct > 28) {
                $planets[] = ["name" => trim($row[0]), "position" => (float)$row[1], "house" => $h, "symbol" => $this->planetSymbols[$ctb]->symbol, "id" => $this->planetSymbols[$ctb]->id];
                $ctb++;
            }
            $ct++;
        };
        return [$planets, $houses];
    }

    /**
     * @param $results
     * @return array
     */
    public function calculateChart($results)
    {
        $this->results = $results;
        $this->getVariables();

        $month = $this->variables['birth_month'];
        $day = $this->variables['birth_day'];
        $year = $this->variables['birth_year'];
        $minutes = $this->variables['birth_minutes'];
        $hour = $this->variables['birth_hour'];
        $sep = ".";
        $sepm = ":";
        $ew = $this->variables['east_west']; // (east is -1, west is 1 in my form)
        $lat = ($this->variables['latitude_degrees'] + ($this->variables['latitude_minutes'] / 60)) * $this->variables['north_south'];
        $lng = ($this->variables['longitude_degrees'] + ($this->variables['longitude_minutes'] / 60)) * -$this->variables['east_west'];
        $this->latt = $lat;
        $this->lngg = $lng;

        if ($this->variables['time_zone'] === 100) {
            $local_time = ($this->variables['longitude_degrees'] + $this->variables['longitude_minutes'] / 60) * 4;
            $local_time = $hour - ($local_time) / 60 * -$ew;
            $lth = floor($local_time);
            $ltm = $minutes + ceil(($local_time - $lth) * 60);
            $birthDate = $day . $sep . $month . $sep . $year;
            $birthTime = $lth . $sepm . $ltm . $sepm . "0";
        } else {
            $local_time = $hour - $this->variables['time_zone'];
            $time = mktime($local_time, $minutes, 0, $month, $day, $year);
            $birthDate = strftime("%d.%m.%Y", $time);
            $birthTime = strftime("%H:%M:%S", $time);
        }

        $this->options = [
            'birthdate' => $birthDate,
            'birthtime' => $birthTime,
            'h_sys' => 'P',
            'lng' => $lng,
            'lat' => $lat
        ];
        $output = $this->getSweph();
        $output = $this->sortPlanetsAndHouses($output);

        $planets = $output[0];
        $houses = $output[1];

        session()->set('planets', json_encode($planets));
        session()->set('houses', json_encode($houses));

        $angles = [];

        for ($p1 = 0; $p1 < 19; $p1++) {
            if (!in_array($planets[$p1]["id"], $this->choosenPoints)) continue;
            for ($p2 = $p1 + 1; $p2 < 19; $p2++) {
                if (!in_array($planets[$p2]["id"], $this->choosenPoints)) continue;
                $diff = abs($planets[$p1]["position"] - $planets[$p2]["position"]);
                if ($diff > 180) {
                    $diff = 360 - $diff;
                }
                $position = $this->MinInDegreesShort($diff);
                $this->angularities[] = [
                    "p1glyph" => $this->planetSymbols[$p1]->symbol,
                    "p2glyph" => $this->planetSymbols[$p2]->symbol,
                    "angularity" => $diff
                ];
                $angles[] = '<tr><td><span class="glyph">' . $this->planetSymbols[$p1]->symbol . '</span></td><td>' . round($diff, 2) . '</td><td><span class="glyph">' . $this->planetSymbols[$p2]->symbol . "</span></td><td> " . $position . '</td></tr>';
            }
        }

        session()->set('birthData', json_encode($this->variables));

        $h = $this->makeHarmonics(0);
        $transitHarmonics = $this->getHarmonicTransit();

        return [
            "transits" => $transitHarmonics[0],
            "planets" => $planets,
            "houses" => $houses,
            "angles" => $h[1],
            "harmonics" => $h[0],
            "harmonicsTransits" => $transitHarmonics[1][0],
            "harmonicsTransitsAngles" => $transitHarmonics[1][1],
            "variables" => $this->variables,
            "symbols" => $this->planetSymbols
        ];
    }

    /**
     * @param null $now
     * @param false $aj
     * @return array|string[]
     * @throws \Exception
     */
    public function getHarmonicTransit($now = null, $aj = false)
    {
        $planets = json_decode(session()->get('planets'), true);

        if ($now === null) {
            $now = $this->getNow();
            $month = $now->format('m');
            $day = $now->format('d');
            $year = $now->format('Y');
        } else {
            $month = (int)$now[1];
            $day = (int)$now[2];
            $year = (int)$now[0];

        }
        $time = mktime(0, 0, 0, $month, $day, $year);
        $birthDate = strftime("%d.%m.%Y", $time);
        $birthTime = strftime("%H:%M:%S", $time);
        $this->options = [
            'birthdate' => $birthDate,
            'birthtime' => $birthTime,
            'h_sys' => 'P',
            'lng' => $this->lngg,
            'lat' => $this->latt
        ];
        $output = $this->getSweph();
        $output = $this->sortPlanetsAndHouses($output);
        $transits = $output[0];
        for ($p1 = 0; $p1 < 19; $p1++) {
            if (!in_array($planets[$p1]["id"], $this->choosenPoints)) continue;
            for ($p2 = 0; $p2 < 19; $p2++) {
                if (!in_array($planets[$p2]["id"], $this->choosenPoints)) continue;
                $diff = abs($transits[$p1]["position"] - $planets[$p2]["position"]);
                if ($diff > 180) {
                    $diff = 360 - $diff;
                }
                $this->transitAngularities[] = [
                    "p1glyph" => $this->planetSymbols[$p1]->symbol,
                    "p2glyph" => $this->planetSymbols[$p2]->symbol,
                    "angularity" => $diff,
                    "angularityDegrees" => $this->MinInDegreesShort($diff)
                ];
            }
        }
        if ($aj) {
            return $this->makeHarmonics(1);
        } else {
            return [$transits, $this->makeHarmonics(1)];
        }
    }

    public function getPositionsByID($id = 0)
    {
        $dataToCompare = $this->getBirthDataBySQL($id);

        $month = (int)$dataToCompare['birth_month'];
        $day = (int)$dataToCompare['birth_day'];
        $year = (int)$dataToCompare['birth_year'];
        $hour = (int)$dataToCompare['birth_hour'];
        $minutes = (int)$dataToCompare['birth_minutes'];
        $timezone = $dataToCompare['time_zone'];
        $local_time = $hour - $timezone;
        $time = mktime($local_time, $minutes, 0, $month, $day, $year);

        $birthDate = strftime("%d.%m.%Y", $time);
        $birthTime = strftime("%H:%M:%S", $time);

        $lat = ($dataToCompare['latitude_degrees'] + ($dataToCompare['latitude_minutes'] / 60)) * $dataToCompare['north_south'];
        $lng = ($dataToCompare['longitude_degrees'] + ($dataToCompare['longitude_minutes'] / 60)) * -$dataToCompare['east_west'];

        $this->options = [
            'birthdate' => $birthDate,
            'birthtime' => $birthTime,
            'h_sys' => 'P',
            'lng' => $lng,
            'lat' => $lat
        ];
        $output = $this->getSweph();
        $output = $this->sortPlanetsAndHouses($output);
        return $output;
    }

    /**
     * @param null $now
     * @param false $aj
     * @return array|string[]
     * @throws \Exception
     */
    public function getSynastry($id = 0)
    {
        $planets = json_decode(session()->get('planets'), true);
        $output = $this->getPositionsByID($id);
        $planets2 = $output[0];
        for ($p1 = 0; $p1 < 19; $p1++) {
            if (!in_array($planets[$p1]["id"], $this->choosenPoints)) {
                continue;
            }
            for ($p2 = 0; $p2 < 19; $p2++) {
                if (!in_array($planets[$p2]["id"], $this->choosenPoints)) {
                    continue;
                }
                $diff = abs($planets2[$p1]["position"] - $planets[$p2]["position"]);
                if ($diff > 180) {
                    $diff = 360 - $diff;
                }
                $this->synastryAngularities[] = [
                    "p1glyph" => $this->planetSymbols[$p1]->symbol,
                    "p2glyph" => $this->planetSymbols[$p2]->symbol,
                    "angularity" => $diff
                ];
            }
        }
        return $this->makeHarmonics(2);
    }

    /**
     * @return mixed
     */
    public function getBirthData()
    {
        return json_decode(session()->get('birthData'), true);
    }

    /**
     * @return mixed
     */
    public function getBirthPreviousData()
    {
        return json_decode(session()->get('previousBirthData'), true);
    }

    /**
     * @param $id
     * @return array|int
     */
    public function getBirthDataBySQL($id)
    {
        $statement = 'SELECT * FROM `birth_data` WHERE `id` = :id';
        $query = database()->prepare($statement);
        $query->bindValue(":id", $id, \PDO::PARAM_STR);
        try {
            $query->execute();
            $result = $query->rowCount();
            if ($result !== 0) {
                return (array)$query->fetchObject();
            }

        } catch (\Exception $e) {
            return 0;
        }
    }

    /**
     * @param $tokenName
     * @return bool|int
     */
    public function existsInDB($tokenName)
    {
        $statement = 'SELECT `id` FROM `birth_data` WHERE `token_name` = :tokenName';
        $query = database()->prepare($statement);
        $query->bindValue(":tokenName", $tokenName, \PDO::PARAM_STR);
        try {
            $query->execute();
            $result = $query->rowCount();
            if ($result !== 0) {
                return true;
            } else {
                return false;
            }

        } catch (\Exception $e) {
            return 0;
        }
    }

    /**
     * @param $id
     * @return bool|\Exception
     */
    public function deleteBirthData($id)
    {
        $id = (int)$id;
        $statement = 'DELETE FROM birth_data WHERE id=:id';
        $query = database()->prepare($statement);
        $query->bindValue(":id", $id, \PDO::PARAM_INT);

        try {
            return $query->execute();;
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @return false|string
     */
    public function saveBirthChart()
    {
        $data = $this->getBirthData();
        $statement = "INSERT INTO `birth_data` (archive_name, token_name, first_name, last_name, email, 
city_of_birth, birth_month, birth_day, birth_year, birth_hour, birth_minutes, latitude_degrees, latitude_minutes, 
longitude_degrees, longitude_minutes, north_south, east_west, owner_id, house_system, time_zone, category) VALUES (:archive_name, :token_name, :first_name, :last_name, :email, :city_of_birth,:birth_month, :birth_day, :birth_year, :birth_hour, :birth_minutes, :latitude_degrees, :latitude_minutes, 
:longitude_degrees,:longitude_minutes, :north_south, :east_west, :owner_id, :house_system, :time_zone, :category); ";
        $query = database()->prepare($statement);
        $query->bindValue(":archive_name", $data["archive_name"], \PDO::PARAM_STR);
        $query->bindValue(":token_name", $data["tokenName"], \PDO::PARAM_STR);
        $query->bindValue(":first_name", $data["first_name"], \PDO::PARAM_STR);
        $query->bindValue(":last_name", $data["last_name"], \PDO::PARAM_STR);
        $query->bindValue(":email", $data["email"], \PDO::PARAM_STR);
        $query->bindValue(":city_of_birth", $data["city_of_birth"], \PDO::PARAM_STR);
        $query->bindValue(":birth_month", $data["birth_month"], \PDO::PARAM_INT);
        $query->bindValue(":birth_day", $data["birth_day"], \PDO::PARAM_INT);
        $query->bindValue(":birth_year", $data["birth_year"], \PDO::PARAM_INT);
        $query->bindValue(":birth_hour", $data["birth_hour"], \PDO::PARAM_INT);
        $query->bindValue(":birth_minutes", $data["birth_minutes"], \PDO::PARAM_INT);
        $query->bindValue(":latitude_degrees", $data["latitude_degrees"], \PDO::PARAM_INT);
        $query->bindValue(":latitude_minutes", $data["latitude_minutes"], \PDO::PARAM_INT);
        $query->bindValue(":longitude_degrees", $data["longitude_degrees"], \PDO::PARAM_INT);
        $query->bindValue(":longitude_minutes", $data["longitude_minutes"], \PDO::PARAM_INT);
        $query->bindValue(":north_south", $data["north_south"], \PDO::PARAM_INT);
        $query->bindValue(":east_west", $data["east_west"], \PDO::PARAM_INT);
        $query->bindValue(":owner_id", user()->id, \PDO::PARAM_INT);
        $query->bindValue(":house_system", $data["house_system"], \PDO::PARAM_INT);
        $query->bindValue(":time_zone", $data["time_zone"], \PDO::PARAM_INT);
        $query->bindValue(":category", $data["category"], \PDO::PARAM_STR);

        try {
            $query->execute();
            $result = $query->rowCount();

            $query->closeCursor();
        } catch (\Exception $e) {
            $result = "0";
        }
        return json_encode($result);
    }

    /**
     * @return array
     */
    public function updateStatus()
    {
        $data = $this->getBirthData();
        $previousData = $this->getBirthPreviousData();
        $changedData = [];
        foreach ($previousData as $key => $datum) {
            if ($data[$key] != $datum) {
                if ($key != "tokenName") {
                    $changedData[$key] = $data[$key];
                }
            }
        }
        return $changedData;
    }

    /**
     * @param $id
     * @return false|string
     */
    public function updateBirthChart($id)
    {
        $changedData = $this->updateStatus();
        if (count($changedData) > 0) {
            $statement = "UPDATE `birth_data` SET ";
            foreach ($changedData as $key => $datum) {
                if ($key != "tokenName") {
                    $statement .= $key . "=:" . $key . ",";
                }
            }
            $statement .= ")";
            $statement = str_replace(",)", " WHERE id=:id", $statement);
            $query = database()->prepare($statement);
            $changedData['id'] = $id;

        }
        $result = true;

        try {
            $query->execute($changedData);
            $result = $query->rowCount();
            $query->closeCursor();

        } catch (\Exception $e) {
            $result = 0;
        }
        return json_encode($result);
    }

    /**
     * @return array
     */
    protected function getVariables()
    {
        $idEdition = session()->get('idEdited');
        if (!isset($this->result['id']) && isset($idEdition)) {
            $this->variables['id'] = $idEdition;
        }
        if ($idEdition > 0) {
            $this->variables['archive_name'] = $this->results['archive_name'];
            $this->variables['tokenName'] = $this->results['token_name'];
        } else {
            $firstname = $this->sanitizer()->alphanumeric(trim($this->results['first_name']));
            $lastname = $this->sanitizer()->alphanumeric(trim($this->results['last_name']));
            $this->variables['tokenName'] = $lastname . $firstname . "-" . time();
            if ($this->results['archive_name'] == "") {
                $this->variables['archive_name'] = $this->results['first_name'] . " " . $this->results['last_name'];
            } else {
                $this->variables['archive_name'] = $this->results['archive_name'];
            }
        }
        $this->variables['first_name'] = $this->results['first_name'];
        $this->variables['last_name'] = $this->results['last_name'];
        $this->variables['email'] = $this->results['email'];
        $this->variables['birth_month'] = (int)$this->results['birth_month'];
        $this->variables['birth_day'] = (int)$this->results['birth_day'];
        $this->variables['birth_year'] = (int)$this->results['birth_year'];
        $this->variables['birth_hour'] = (int)$this->results['birth_hour'];
        $this->variables['birth_minutes'] = (int)$this->results['birth_minutes'];
        $this->variables['city_of_birth'] = $this->results['city_of_birth'];
        $this->variables['latitude_degrees'] = (int)$this->results['latitude_degrees'];
        $this->variables['latitude_minutes'] = (int)$this->results['latitude_minutes'];
        $this->variables['north_south'] = (int)$this->results['north_south'];
        $this->variables['longitude_degrees'] = (int)$this->results['longitude_degrees'];
        $this->variables['longitude_minutes'] = (int)$this->results['longitude_minutes'];
        $this->variables['east_west'] = (int)$this->results['east_west'];
        $this->variables['time_zone'] = (int)$this->results['time_zone'];
        $this->variables['house_system'] = (int)$this->results['house_system'];
        $this->variables['category'] = $this->results['category'];
        return $this->variables;
    }


    /**
     * @param $min
     * @return string
     */
    protected function MinInDegreesShort($min)
    {
        $n = (float)$min;
        $whole = floor($n);      // 1
        $fraction = round(($n - $whole) * 60); // .25
        if ($fraction < 10) {
            $fraction = "0" . $fraction;
        }
        return $whole . "º" . $fraction;
    }

    protected function simpleHarmnicList($planets, $type = 0)
    {
        $angles = [];
        $presentHarmonics = [];
        $harmonics = $this->calculateHarmonicsList();
        $preferences = page()->getPreferences();
        for ($p1 = 0; $p1 < 19; $p1++) {
            if (!in_array($planets[$p1]["id"], $preferences)) continue;
            for ($p2 = $p1 + 1; $p2 < 19; $p2++) {
                if (!in_array($planets[$p2]["id"], $preferences)) continue;
                $diff = abs($planets[$p1]["position"] - $planets[$p2]["position"]);
                if ($diff > 180) {
                    $diff = 360 - $diff;
                }
                $angles[] = [
                    "p1position" => $planets[$p1]["position"],
                    "p2position" => $planets[$p2]["position"],
                    "p1" => $p1,
                    "p2" => $p2,
                    "angularity" => $diff
                ];
            }
        }
        /*$seveness = ['harm7', 'harm14', 'harm21', 'harm29'];
        $fiveness = ['harm5', 'harm10', 'harm15', 'harm20','harm25','harm30'];
        $twoness = ['harm2', 'harm4', 'harm8', 'harm16','harm24','harm32'];
        $three = ['harm3', 'harm6', 'harm9', 'harm18','harm27'];*/
        foreach ($angles as $key => $angle) {
            $angles[$key]['class'] = [];
            foreach ($harmonics as $harmonic) {
                if ($type === 0) {
                    $start = $harmonic["start"];
                    $end = $harmonic["end"];
                } else {
                    $start = $harmonic["startS"];
                    $end = $harmonic["endS"];

                }
                $test = false;
                $mode = $harmonic["mode"];
                $angleTest = $angle['angularity'];
                switch ($mode) {
                    case "at":
                    case "st":
                        if ($angleTest >= $start && $angleTest <= $end) {
                            $test = true;
                        }
                        break;
                    default:
                        if ($angleTest > $start && $angleTest <= $end) {
                            $test = true;
                        }
                        break;
                }
                if ($test === true) {
                    $angles[$key]['harmonic'][] = $harmonic["harmonic"];
                    if (!in_array($harmonic["style"], $angles[$key]['class'])) $angles[$key]['class'][] = $harmonic["style"];
                    $harm = "harm" . $harmonic["harmonic"];
                    if(!in_array($harm,$presentHarmonics)){
                        $presentHarmonics[] = $harm;
                    }
                    if (!in_array($harm, $angles[$key]['class'])) $angles[$key]['class'][] = $harm;
                    $title1 = strtolower($this->planetSymbols[$angle["p1"]]->title);
                    if (!in_array($title1, $angles[$key]['class'])) $angles[$key]['class'][] = $title1;
                    $title2 = strtolower($this->planetSymbols[$angle["p2"]]->title);
                    if (!in_array($title2, $angles[$key]['class'])) $angles[$key]['class'][] = $title2;
                }
            }
        }
        return [$angles,$presentHarmonics];
    }

    /**
     * @param int $type
     * @return string[]
     */
    protected function makeHarmonics($type = 0)
    {
        $harmonics = $this->harmonicRanges;
        $natalHarmonics = [];
        $t = $u = "";
        $rowAngles = "";

        switch ($type) {
            case 0:
            case 3:
                $testAngularities = $this->angularities;
                break;
            case 1:
                $testAngularities = $this->transitAngularities;
                $t = '<sup>t</sup>';
                break;
            case 2:
                $testAngularities = $this->synastryAngularities;
                $t = '<sup>2</sup>';
                $u = '<sup>1</sup>';
                break;
        }
        for ($i = 1; $i < 33; $i++) {
            $distributedHarmonics[$i] = [];
        }

        foreach ($testAngularities as $angularity) {
            $angle = $angularity["angularity"];
            $rowAngle = "<tr><td><span class='glyph'>" . $angularity["p1glyph"] . "</span></td><td>" . $this->MinInDegreesShort(round($angle, 2)) . "</td><td><span class='glyph'>" . $angularity["p2glyph"] . "</span></td><td>";

            foreach ($harmonics as $harmonic) {
                if ($type == 0) {
                    $start = $harmonic["start"];
                    $end = $harmonic["end"];
                } else {
                    $start = $harmonic["startS"];
                    $end = $harmonic["endS"];

                }
                $test = false;
                $mode = $harmonic["mode"];
                switch ($mode) {
                    case "at":
                    case "st":
                        if ($angle >= $start && $angle <= $end) {
                            $test = true;
                        }
                        break;
                    default:
                        if ($angle > $start && $angle <= $end) {
                            $test = true;
                        }
                        break;
                }
                $style = $harmonic["style"];
                if ($test === true) {
                    $presentation = $t . "<span class='glyph'>" . $angularity["p1glyph"] . " </span><span class='" . $style . "'>" . $harmonic["harmonic"] . "</span> <span class='glyph'>" . $angularity["p2glyph"] . "</span>" . $u;
                    $rowAngle .= "<span class='" . $style . " lateral'>" . $harmonic["harmonic"] . "</span>";
                    $natalHarmonics[] = ["presentation" => $presentation, "angle" => $harmonic["harmonic"], "rowAngle" => $rowAngle];
                }
            }
            $rowAngle .= "</td>";
            $rowAngles .= $rowAngle;
        }

        foreach ($natalHarmonics as $natalHarmonic) {
            $distributedHarmonics[$natalHarmonic["angle"]][] = $natalHarmonic["presentation"];
        }
        ksort($distributedHarmonics);
        $html = "<div class='sub_harmonics'>";
        foreach ($distributedHarmonics as $key => $distributedHarmonic) {
            $html .= "<div><h3>" . $key . "</h3>";
            foreach ($distributedHarmonic as $item) {
                $html .= "<p>" . $item . "</p>";
            }
            $html .= "</div>";
        }
        $html .= "</div>";
        return [$html, $rowAngles];
    }


    /**
     * @param $pl
     */
    protected function checkRx($pl)
    {
        //if ($transit->planets[1]->rx == 'Rx') {
        if ($pl->rx == 'Rx') {
            echo '<p>' . $pl->longName . ' ' . $pl->planets[1]->rx . '</p>';
        }
    }


    /**
     * @return array
     */
    protected function getDefinitions()
    {
        $definitions = [];
        $harmonicKeywords = pages()->find('template="harmonic_keyword');
        foreach ($harmonicKeywords as $harmonicKeyword) {
            $definitions[] = ['angle' => $harmonicKeyword->harmonic_number, 'keyword' => $harmonicKeyword->keyword];
        }
        return $definitions;
    }

    /**
     * @param $id
     * @return string
     */
    protected function decButton($id)
    {
        return "<button id='minus" . $id . "' class='uk-button plus'>&mdash;</button>";
    }

    /**
     * @param $id
     * @return string
     */
    protected function incButton($id)
    {
        return "<button id='plus" . $id . "' class='uk-button plus'>+</button>";
    }

    /**
     * @return mixed
     * @throws WirePermissionException
     */
    protected function createTransitForm()
    {
        $now = $this->getNow();
        $form = modules()->get("InputfieldForm"); // Le module de formulaire.
        $form->action = ""; // Recharge la même page.
        $form->method = "post"; // Méthode de soumission des valeurs.
        $form->attr("name+id", "harmonic_form"); // Ce sera autant le nom que l’id.
        $form->attr("class", "uk-form-stacked no-form-ul"); // Toute classe CSS.

        $f = modules()->get("InputfieldHidden");
        $f->attr("id+name", session()->CSRF->getTokenName());
        $f->attr("maxlength", 30);
        $f->attr("class", "uk-input");
        $f->attr("value", session()->CSRF->getTokenValue());

        $nameid = "yearHarmonic";
        $f = modules()->get("InputfieldText");
        $f->set("label", __text("Year"));
        $f->attr("name+id", $nameid);
        $f->attr("maxlength", 5);
        $f->attr("class", "uk-input");
        $f->attr("value", $now->format('Y'));
        $f->attr("placeholder", __text('Year'));
        $f->prependMarkup($this->decButton($nameid));
        $f->appendMarkup($this->incButton($nameid));
        $form->add($f);

        $nameid = "monthHarmonic";
        $f = modules()->get("InputfieldText");
        $f->set("label", __text("Month"));
        $f->attr("name+id", $nameid);
        $f->attr("maxlength", 5);
        $f->attr("class", "uk-input");
        $f->attr("value", $now->format('m'));
        $f->attr("placeholder", __text('Month'));
        $f->prependMarkup($this->decButton($nameid));
        $f->appendMarkup($this->incButton($nameid));
        $form->add($f);

        $nameid = "dayHarmonic";
        $f = modules()->get("InputfieldText");
        $f->set("label", __text("Day"));
        $f->attr("name+id", $nameid);
        $f->attr("maxlength", 5);
        $f->attr("class", "uk-input");
        $f->attr("value", $now->format('d'));
        $f->attr("placeholder", __text('Day'));
        $f->prependMarkup($this->decButton($nameid));
        $markup = $this->incButton($nameid);
        $f->appendMarkup($markup);
        $form->add($f);
        return $form->render();
    }

    /**
     * @return mixed
     * @throws WirePermissionException
     */
    protected function createSynastryForm()
    {
        $form = modules()->get("InputfieldForm"); // Le module de formulaire.
        $form->action = ""; // Recharge la même page.
        $form->method = "post"; // Méthode de soumission des valeurs.
        $form->attr("name+id", "synastry_form"); // Ce sera autant le nom que l’id.
        $form->attr("class", "uk-form-stacked no-form-ul"); // Toute classe CSS.

        $f = modules()->get("InputfieldHidden");
        $f->attr("id+name", session()->CSRF->getTokenName());
        $f->attr("maxlength", 30);
        $f->attr("class", "uk-input");
        $f->attr("value", session()->CSRF->getTokenValue());

        $nameid = "charts";
        $chartList = $this->getChartsByOwnerForSelect();

        $f = modules()->get("InputfieldSelect");
        $f->set("label", __text("Available charts"));
        $f->attr("name+id", $nameid);
        $f->attr("maxlength", 50);
        $f->attr("class", "uk-form-width-medium uk-select");
        $f->addOptions($chartList);
        $form->add($f);
        return $form->render();
    }

    /**
     * @return array
     */
    protected function getChartsByOwnerForSelect(): array
    {
        $chartList = [];
        $statement = 'SELECT * FROM `birth_data` WHERE `owner_id` = :owner_id ORDER BY last_name';
        $query = $this->database()->prepare($statement);
        $query->bindValue(":owner_id", user()->id, \PDO::PARAM_INT);
        try {
            $query->execute();
            $result = $query->rowCount();
            if ($result !== 0) {
                $chartList["0"] = __text('Please select a name');
                while ($row = $query->fetchObject()) {
                    $chartList[(string)$row->id] = $row->last_name . " " . $row->first_name;
                }
            }
        } catch (\Exception $e) {
            $result = 0;
        }
        return $chartList;
    }

    /**
     * @param $plce
     * @return string
     */
    public function DecToZodGlyph($plce)
    {
        $SignNames = $this->signSymbols;
        $s = (int)($plce / 30.0);
        $plce -= ($s * 30);
        $d = (int)$plce;
        $m = (int)((($plce - $d) * 60.0) + .5);

        if ($m >= 60) {
            $m -= 60;
            $d++;
        }
        if ($d >= 30) {
            $d -= 30;
            $s++;
        }
        if ($s >= 12) {
            $s -= 12;
        }

        $zod = sprintf('%d <span class="glyph">%s</span> %02d', $d, $SignNames[$s], $m);
        return $zod;
    }

    public function calculateHarmonicsList()
    {
        $harmonics = [];
        $testharmonics = [];
        $circle = 360;
        $totalHarmonics = [];
        $hOrbs = [];
        $orbs = [
            2, 6, 12, 1, 3, 6
        ];
        $mode = [
            "wide", "close", "tight"
        ];
        for ($i = 3; $i < 33; $i++) {
            $j = $circle / $i;
            for ($k = 1; $k <= $i; $k++) {
                $angle = $k * $j;
                if ($angle < $circle / 2 && !in_array((string)$angle, $testharmonics)) {
                    $testharmonics[] = (string)$angle;
                    $harmonics[] = [$i, $angle];
                }
            }
        }
        for ($i = 1; $i < 34; $i++) {
            foreach ($orbs as $orb) {
                $hOrbs[$i][] = $orb / $i;
            }
        }
        $totalHarmonics[] =
            [
                ["harmonic" => "1", "style" => $mode[0], "mode" => "aw", "start" => 6, "startDeg" => $this->MinInDegreesShort(6), "end" => 12,
                    "endDeg" => $this->MinInDegreesShort(12), "startS" => 3, "startSDeg" => $this->MinInDegreesShort(3), "endS" => 6, "endSDeg" => $this->MinInDegreesShort(6)],
                ["harmonic" => "1", "style" => $mode[1], "mode" => "ac", "start" => 2, "startDeg" => $this->MinInDegreesShort(2), "end" => 6,
                    "endDeg" => $this->MinInDegreesShort(6), "startS" => 1, "startSDeg" => $this->MinInDegreesShort(1), "endS" => 3, "endSDeg" => $this->MinInDegreesShort(3)],
                ["harmonic" => "1", "style" => $mode[2], "mode" => "at", "start" => 0, "startDeg" => $this->MinInDegreesShort(0), "end" => 2,
                    "endDeg" => $this->MinInDegreesShort(2), "startS" => 0, "startSDeg" => $this->MinInDegreesShort(0), "endS" => 1, "endSDeg" => $this->MinInDegreesShort(2)],
            ];
        $totalHarmonics[] =
            [
                ["harmonic" => "2", "style" => $mode[0], "mode" => "aw", "start" => 174, "startDeg" => $this->MinInDegreesShort(174), "end" => 177,
                    "endDeg" => $this->MinInDegreesShort(177), "startS" => 177, "startSDeg" => $this->MinInDegreesShort(177), "endS" => 178.5, "endSDeg" => $this->MinInDegreesShort(178.5)],
                ["harmonic" => "2", "style" => $mode[1], "mode" => "ac", "start" => 177, "startDeg" => $this->MinInDegreesShort(177), "end" => 179,
                    "endDeg" => $this->MinInDegreesShort(179), "startS" => 178.5, "startSDeg" => $this->MinInDegreesShort(178.5), "endS" => 179.5, "endSDeg" => $this->MinInDegreesShort(179.5)],
                ["harmonic" => "2", "style" => $mode[2], "mode" => "at", "start" => 179, "startDeg" => $this->MinInDegreesShort(179), "end" => 180,
                    "endDeg" => $this->MinInDegreesShort(180), "startS" => 179.5, "startSDeg" => $this->MinInDegreesShort(179.5), "endS" => 180, "endSDeg" => $this->MinInDegreesShort(180)]
            ];
        foreach ($harmonics as $harmonic) {
            $h0 = $harmonic[0];
            $h1 = $harmonic[1];
            $col0 = $h1 - $hOrbs[$h0][0];
            $col1 = $h1 + $hOrbs[$h0][0];
            $col2 = $h1 - $hOrbs[$h0][1];
            $col3 = $h1 + $hOrbs[$h0][1];
            $col4 = $h1 - $hOrbs[$h0][2];
            $col5 = $h1 + $hOrbs[$h0][2];
            $col6 = $h1 - $hOrbs[$h0][3];
            $col7 = $h1 + $hOrbs[$h0][3];
            $col8 = $h1 - $hOrbs[$h0][4];
            $col9 = $h1 + $hOrbs[$h0][4];
            $col10 = $h1 - $hOrbs[$h0][5];
            $col11 = $h1 + $hOrbs[$h0][5];

            $colDeg = $this->MinInDegreesShort($h1);
            $col0Deg = $this->MinInDegreesShort($h1 - $hOrbs[$h0][0]);
            $col1Deg = $this->MinInDegreesShort($h1 + $hOrbs[$h0][0]);
            $col2Deg = $this->MinInDegreesShort($h1 - $hOrbs[$h0][1]);
            $col3Deg = $this->MinInDegreesShort($h1 + $hOrbs[$h0][1]);
            $col4Deg = $this->MinInDegreesShort($h1 - $hOrbs[$h0][2]);
            $col5Deg = $this->MinInDegreesShort($h1 + $hOrbs[$h0][2]);
            $col6Deg = $this->MinInDegreesShort($h1 - $hOrbs[$h0][3]);
            $col7Deg = $this->MinInDegreesShort($h1 + $hOrbs[$h0][3]);
            $col8Deg = $this->MinInDegreesShort($h1 - $hOrbs[$h0][4]);
            $col9Deg = $this->MinInDegreesShort($h1 + $hOrbs[$h0][4]);
            $col10Deg = $this->MinInDegreesShort($h1 - $hOrbs[$h0][5]);
            $col11Deg = $this->MinInDegreesShort($h1 + $hOrbs[$h0][5]);
            $totalHarmonics[] =
                [
                    ["harmonic" => $h0, "style" => $mode[0], "mode" => "aw", "start" => $col4, "startDeg" => $col4Deg, "end" => $col2,
                        "endDeg" => $col2Deg, "startS" => $col10, "startSDeg" => $col10Deg, "endS" => $col8, "endSDeg" => $col8Deg],
                    ["harmonic" => $h0, "style" => $mode[1], "mode" => "ac", "start" => $col2, "startDeg" => $col2Deg, "end" => $col0,
                        "endDeg" => $col0Deg, "startS" => $col8, "startSDeg" => $col8Deg, "endS" => $col6, "endSDeg" => $col6Deg],
                    ["harmonic" => $h0, "style" => $mode[2], "mode" => "at", "start" => $col0, "startDeg" => $col0Deg, "end" => $h1,
                        "endDeg" => $colDeg, "startS" => $col6, "startSDeg" => $col6Deg, "endS" => $h1, "endSDeg" => $colDeg],
                    ["harmonic" => $h0, "style" => $mode[2], "mode" => "st", "start" => $h1, "startDeg" => $colDeg, "end" => $col1,
                        "endDeg" => $col1Deg, "startS" => $h1, "startSDeg" => $colDeg, "endS" => $col7, "endSDeg" => $col7Deg],
                    ["harmonic" => $h0, "style" => $mode[1], "mode" => "sc", "start" => $col1, "startDeg" => $col1Deg, "end" => $col3,
                        "endDeg" => $col3Deg, "startS" => $col7, "startSDeg" => $col7Deg, "endS" => $col9, "endSDeg" => $col9Deg],
                    ["harmonic" => $h0, "style" => $mode[0], "mode" => "sw", "start" => $col3, "startDeg" => $col3Deg, "end" => $col5,
                        "endDeg" => $col5Deg, "startS" => $col9, "startSDeg" => $col9Deg, "endS" => $col11, "endSDeg" => $col11Deg],
                ];
        }
        $this->harmonicRanges = [];
        foreach ($totalHarmonics as $totalHarmonic) {
            foreach ($totalHarmonic as $item) {
                $k = $item['start'];
                $start = $item['startDeg'];
                if ($k < 100) {
                    $start = "0" . $start;
                }
                if ($k < 10) {
                    $start = "0" . $start;
                }
                $nom = $item["harmonic"] . $item["mode"];
                if ($start === "174º00" && $nom === "2aw") {
                    $start .= "a";
                }
                $start = str_replace('º', '-', $start);
                $this->harmonicRanges[$start] = $item;
            }
        }

        ksort($this->harmonicRanges);
        return $this->harmonicRanges;
    }

}