<?php namespace ProcessWire;
$root = $_SERVER["DOCUMENT_ROOT"];

require_once("$root/site/mobiledetect/namespaced/Detection/MobileDetect.php");
use Detection\MobileDetect;

function autoDectectLanguage(): string {
	$session = wire("session");
	$chosenlanguage = "";
	$cookieName = "chosenlanguage";
	$domain = "." . wire("config")->httpHost;
	if(isset($_COOKIE[$cookieName])) {
		$chosenlanguage = $_COOKIE[$cookieName];
	} else if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
		$chosenlanguage = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
		if($chosenlanguage != "fr" && $chosenlanguage != "en") {
			$chosenlanguage = "en";
		}
		$_COOKIE[$cookieName] = $chosenlanguage;
		$expiration = strtotime("+1 month");
		$path = "/";
		setcookie($cookieName, $chosenlanguage, $expiration, $path, $domain);

	}
	if($_SERVER['REQUEST_URI'] == "/" && $chosenlanguage != "en") {
		$session->redirect("/" . $chosenlanguage . "/");
	}
	return $chosenlanguage;
}


/*
 * ================================= automatic actions performed on every pages
 */

$mobileDetect = new \Detection\MobileDetect();
session()->set('isRobot',$mobileDetect->isBot());
session()->set('isMobile',$mobileDetect->isMobile());
session()->set('isTablet',$mobileDetect->isTablet());


