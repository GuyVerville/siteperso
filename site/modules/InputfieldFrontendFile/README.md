# InputfieldFrontendFile

Provides file uploads for ProcessWire LoginRegisterPro module  
(https://processwire.com/store/login-register-pro/).

Copyright 2021 by Ryan Cramer Design, LLC.

Also uses the dropzone.js library by Matias Meno (https://dropzonejs.com).

The following files are part of LoginRegisterPro (a commercial module),
please do not distribute:

- InputfieldFrontendFile.module
- InputfieldFrontendFile.js

For information, instructions and documentation please see:   
<https://processwire.com/store/login-register-pro/file-uploads/>

Contact: <https://processwire.com/contact/>

