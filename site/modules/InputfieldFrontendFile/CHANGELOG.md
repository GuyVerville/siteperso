# InputfieldFrontendFile Changelog

## [0.0.2] - 2021-05-01

- Fixed issue with module config not saving changes to mime types textarea. 
