/**
 * InputfieldFrontendFile 
 * 
 * Part of the LoginRegisterPro module
 * 
 * Copyright 2021 by Ryan Cramer Design, LLC
 * 
 */
var InputfieldFrontendFile = {

	// trash icon
	trashIcon: "<i class='fa fa-trash'></i>",
	
	// edit icon
	editIcon: "<i class='fa fa-pencil'></i>",

	// all initialized dropzones
	dropzones: {}, 

	// common dropzone options shared among all dropzones (local options passed in initDropzone)
	dropzoneOptions: {}, 
	
	// option to override alert function
	alertFunction: null, 
	
	/**
	 * Initialize and add events
	 * 
	 */
	init: function() {
		
		let $inputfields = jQuery('.InputfieldFrontendFile');
		
		if(!$inputfields.length) return;
		
		$inputfields
			.on('click', '.dz-remove', this.clickRemove)
			.on('click', '.pw-dz-edit', this.clickEdit)
			.on('click', '.dz-filename a', this.clickEdit)
			.on('mouseover', '.dz-remove', function() { 
				jQuery(this).closest('.dz-preview').addClass('pw-dz-remove-ready'); 
			})
			.on('mouseout', '.dz-remove', function() {
				jQuery(this).closest('.dz-preview').removeClass('pw-dz-remove-ready'); 
			});
	
		if(this.alertFunction === null) {
			this.alertFunction = function(msg) {
				alert(msg);
			}; 
		}
	},
	
	/**
	 * Set options that will be shared among all dropzones
	 *
	 * @param opt
	 *
	 */
	dropzoneSetOptions: function(opt) {
		
		if(typeof opt.trashIcon !== "undefined") {
			this.trashIcon = opt.trashIcon;
			delete opt.trashIcon;
		}	
		if(typeof opt.editIcon !== "undefined") { 
			this.editIcon = opt.editIcon;
			delete opt.editIcon;
		}
		if(typeof opt.alertFunction !== "undefined") {
			this.alertFunction = opt.alertFunction;
			delete opt.alertFunction;
		}

		let $ = jQuery;
		$.extend(this.dropzoneOptions, opt);
	},

	/**
	 * Initialize a dropzone
	 * 
	 * @param id HTML element id attribute of the dropzone
	 * @param options Options to customize the dropzone, use any option supported by Dropzone class in dropzone.js
	 * 
	 */
	dropzoneInit: function(id, options) {

		let $ = jQuery;
		let opt = $.extend({}, this.dropzoneOptions, options);
	
		Dropzone.autoDiscover = false;
		
		if(opt.resizeWidth || opt.resizeHeight) {
			// we treat these as maxWidth and maxHeight
			opt.transformFile = function(file, done) {
				// if not an image or no resize dimensions used, exit now...
				if(!file.type.match(/image.*/)) return done(file);
				// ...otherwise resize the image
				return InputfieldFrontendFile.resizeImage(id, file, opt.resizeWidth, opt.resizeHeight, opt.resizeMethod, done);
			};
		}
		
		let dz = new Dropzone('div#' + id, opt);
		let $dz = jQuery('#' + id); 
		let $inputfield = $dz.closest('.Inputfield');
		
		dz.on('addedfile', function(file) {
			setTimeout(function() {
				InputfieldFrontendFile.fileAdded($inputfield, file);
			}, 250); 
		});
		
		dz.on('success', function(file, response) {
			if(typeof response !== 'object') {
				response = JSON.parse(response);
			}
			if(!response.success) {
				if(response.messages.length) InputfieldFrontendFile.alert(response.messages[0]); 
				dz.removeFile(file);
				InputfieldFrontendFile.checkNumFiles($inputfield);
			}
		});
	
		this.dropzones[id] = dz;
		this.checkNumFiles($inputfield);
	
		if(jQuery.ui) {
			// if jQuery UI is available then make the files/images sortable
			$dz.sortable({
				stop: function(event, ui) {
					if(event && ui) {} // ignore
					InputfieldFrontendFile.sortableStop($dz.children());
				}
			});
		}
		
		$inputfield.on('click', '.pw-dz-back', this.clickBack); 
	},

	/**
	 * Handler for jQuery UI sortable stop event
	 * 
	 * @param $items All of the sortable items
	 * 
	 */
	sortableStop: function($items) {
		$items.each(function() {
			// remove style attribute that jQuery UI adds to items
			let $item = jQuery(this);
			$item.removeAttr('style');
			// remove success class if file was uploaded, otherwise it re-shows the dz-success-mark animation
			$item.removeClass('dz-success');
		}); 
	},

	/**
	 * Handler for dropzone transformFile method that performs a resize treating width/height as max-width/max-height
	 * 
	 * @param id HTML id attribute of the dropzone
	 * @param file Dropzone file object with width and height properties
	 * @param resizeWidth Max allowed width
	 * @param resizeHeight Max allowed height
	 * @param resizeMethod Either 'contain' or 'crop' for when both width and height are provided
	 * @param done Function to call upon completion
	 * @returns {*}
	 * 
	 */
	resizeImage: function(id, file, resizeWidth, resizeHeight, resizeMethod, done) {
		let dz = this.dropzones[id];
		if(resizeWidth > 0 && resizeHeight > 0) { // both max width and max height: determine which to use
			if(file.width > resizeWidth && file.height > resizeHeight) {
				// both dimensions larger than allowed: keep all specified settings
			} else if(file.width > resizeWidth) {
				resizeHeight = null; // resize only width
			} else if(file.height > resizeHeight) {
				resizeWidth = null; // resize only height
			}
		} else if(resizeWidth > 0 && file.width > resizeWidth) {
			resizeHeight = null; // resize only width
		} else if(resizeHeight > 0 && file.height > resizeHeight) {
			resizeWidth = null; // resize only height
		} else {
			return done(file); // resize not needed
		}
		// pass to dropzone to perform the actual resize
		return dz.resizeImage(file, resizeWidth, resizeHeight, resizeMethod, done);
	},

	/**
	 * Event handler for remove link click
	 * 
	 * @returns {boolean}
	 * 
	 */
	clickRemove: function() {
		let $a = jQuery(this);
		let $preview = $a.closest('.dz-preview');
		let $checkbox = $preview.find('input[type=checkbox]');
		if($preview.hasClass('pw-dz-removed')) {
			// restore
			$preview.removeClass('pw-dz-removed');
			$checkbox.removeAttr('checked');
		} else {
			// delete
			$preview.addClass('pw-dz-removed');
			$checkbox.attr('checked', 'checked');
		}
		return false;
	},

	/**
	 * Event handler for edit link click
	 * 
	 * This populates inputs in the .pw-dz-editor element with values from the 
	 * file or image JSON data (in .pw-dz-data) and then hides the files list
	 * and shows the .pw-dz-editor.
	 * 
	 * @returns {boolean}
	 * 
	 */
	clickEdit: function() {
		let $a = jQuery(this);
		let $item = $a.closest('.dz-preview');
		let $filename = $item.find('.dz-filename');
		let href = $filename.children('a').attr('href');
		let filename = jQuery.trim($filename.text());
		let $inputfield = $a.closest('.Inputfield');
		let $dz = $inputfield.find('.dropzone');
		let $editor = $inputfield.find('.pw-dz-editor');
		let $imgWrap = $editor.find('.pw-dz-img');
		let $img = $imgWrap.find('img');
		let $data = $item.find('.pw-dz-data');
		let $inputs = $editor.find('.pw-dz-inputs').find(':input');
		let isImg = $item.hasClass('dz-image-preview');
		let dataJSON = $data.length ? $data.val() : '';
		let filesize = jQuery.trim($item.find('.dz-size').text());
		let filedate = $filename.attr('data-pw-date'); 
		let dimensions = $item.find('.dz-image').attr('data-pw-size');
		
		// if there are inputs populate them 
		if($inputs.length && dataJSON.indexOf('{') === 0) {
			let data = JSON.parse(dataJSON);
			$inputs.each(function() {
				let $input = jQuery(this);
				let name = $input.attr('data-name');
				if(typeof data[name] !== "undefined") {
					$input.val(data[name]); // populate data value to input
				}
				$input.on('change', function() {
					// populate input value back to data
					data[name] = $input.val();
					$data.val(JSON.stringify(data));
				}); 
			});
		}	
		
		$editor.find('.pw-dz-file').text(filename);
		$editor.find('.pw-dz-view').attr('href', href);
		$editor.find('.pw-dz-date').text(filedate); 
		
		if(dimensions === 'x') {
			$editor.find('.pw-dz-size').text(filesize);
		} else {
			$editor.find('.pw-dz-size').text(dimensions + ' ' + filesize);
		}
		
		if(isImg) {
			$img.attr('src', href).removeAttr('hidden');
			$img.closest('a').attr('href', href);
		} else {
			$img.attr('hidden', true);
		}
	
		// hide the files list and show the editor
		jQuery('html, body').animate({ scrollTop: $inputfield.offset().top }, 200, 'linear');
		$dz.fadeOut('fast', function() {
			$dz.attr('hidden', true);
			$editor.hide().removeAttr('hidden').fadeIn('fast', function() {
			});
		});
		
		return false;
	},

	/**
	 * Event handler for back link(s) in the .pw-dz-editor screen
	 * 
	 * Hides the editor and shows the files list again.
	 * 
	 * @returns {boolean}
	 * 
	 */
	clickBack: function() {
		let $a = jQuery(this);
		let $inputfield = $a.closest('.Inputfield');
		let $dz = $inputfield.find('.dropzone');
		let $editor = $a.closest('.pw-dz-editor');
		let $inputs = $editor.find('.pw-dz-inputs').find(':input');
		
		$inputs.each(function() {
			let $input = jQuery(this);
			$input.off('change');
		}); 
		
		$editor.fadeOut('fast', function() {
			$editor.attr('hidden', true);
			$dz.hide().removeAttr('hidden');
			$dz.fadeIn('fast');
		});
		
		return false;
	},

	/**
	 * Event called when a file(s) have been uploaded/added
	 * 
	 * Finds all items in the dropzone that are not yet ready and gets them setup.
	 * 
	 * @param $inputfield The inputfield that files were added into
	 * @param file The file that was added (dropzone file object)
	 * 
	 */
	fileAdded: function($inputfield, file) {
		if(file) {} // for reference or future use, not currently used here
		$inputfield.find('.dz-preview:not(.pw-dz-ready):not(.dz-error)').each(function() {
			let $item = jQuery(this);
			InputfieldFrontendFile.setupNewFileItem($item, $inputfield); 	
		});
		InputfieldFrontendFile.checkNumFiles($inputfield);
	},

	/**
	 * Setup a new file item after it has been uploaded
	 * 
	 * @param $item The .dz-preview file item that needs to be setup
	 * @param $inputfield The Inputfield where it lives within
	 * 
	 */
	setupNewFileItem: function($item, $inputfield) {
		let name = $inputfield.attr('data-fieldname');
		let filename = $item.find('.dz-filename').children('span').text();
		let $removeIcon = jQuery(InputfieldFrontendFile.trashIcon);
		let $removeLink  = jQuery("<a class='dz-remove' href='#' data-dz-remove=''></a>").append($removeIcon);
		let $inputRemove = jQuery("<input hidden type='checkbox' />");
		let $inputNames = jQuery("<input type='hidden' />");
		let $inputData = jQuery("<input type='hidden' />").addClass('pw-dz-data');
		let data = { name: filename };
		let $editor = $inputfield.find('.pw-dz-editor');
		
		// let $editIcon = jQuery(InputfieldFrontendFile.editIcon);
		// let $editLink  = jQuery("<a class='pw-dz-edit' href='#'></a>").append($editIcon);

		// setup placeholders for all editor inputs in the data object
		$editor.find(':input').each(function() {
			let $input = jQuery(this);
			let inputName = $input.attr('data-name');
			if(inputName) data[inputName] = '';
		});

		$inputRemove.attr('name', name + '__removed[]').val(filename);
		$inputNames.attr('name', name + '__names[]').val(filename);
		$inputData.attr('name', name + '__data[]').val(JSON.stringify(data));
		
		$item.addClass('pw-dz-ready')
			.append($removeLink)
			.append($inputRemove)
			.append($inputNames)
			.append($inputData)
			.removeClass('dz-processing')
			.removeClass('dz-success');
		
		// $item.append($editLink);
	},

	/**
	 * Check if number of files exceeds max allowed and hide or move the .dz-default uploaded instructions
	 * 
	 * @param $inputfield
	 * @returns {*}
	 * 
	 */
	checkNumFiles: function($inputfield) {
		let $div = $inputfield.find('.dropzone');
		let numFiles = $div.find('.pw-dz-ready').length;
		let maxFiles = parseInt($inputfield.attr('data-maxfiles'));
		let $dzDefault = $div.find('.dz-default');
		if(maxFiles > 0 && numFiles >= maxFiles) {
			// hide upload instructions when max files has been reached
			$dzDefault.hide();
		}  else {
			// otherwise move it to the end
			$div.append($dzDefault);
			$dzDefault.show();
		}
		return numFiles;
	},

	/**
	 * Default alert function for error messages, can be overridden
	 * 
	 * @param msg
	 * 
	 */
	alert: function(msg) {
		alert(msg);
	}
};

jQuery(document).ready(function() {
	InputfieldFrontendFile.init();
}); 