<?php namespace ProcessWire;

/**
 * ProcessWire Login Register Pro: Configuration
 *
 * DO NOT DISTRIBUTE, this is a commercial module.
 *
 * Copyright 2019 by Ryan Cramer Design, LLC
 *
 */
class LoginRegisterProConfig extends Wire {

	/**
	 * @var LoginRegisterPro
	 * 
	 */
	protected $module;

	/**
	 * Construct
	 *
	 * @param LoginRegisterPro $module
	 * 
	 */
	public function __construct(LoginRegisterPro $module) {
		$this->module = $module;
		parent::__construct();
	}

	/**
	 * Get config Inputfields
	 * 
	 * @param InputfieldWrapper $inputfields
	 *
	 */
	public function getConfigInputfields(InputfieldWrapper $inputfields) {

		$module = $this->module;
		$modules = $this->modules;
		$hasForgot = $this->modules->isInstalled('ProcessForgotPassword');
		$hasThrottle = $this->modules->isInstalled('SessionLoginThrottle');
		$togglesLabel = $this->_('Toggles');
		$installLabel = $this->_('Install');
		$configureLabel = $this->_('Configure');
		$requiresModuleLabel = $this->_('Requires core “%s” module');
		$logUrl = $this->config->urls->admin . 'setup/logs/view/login-register-pro/'; 
		$docsUrl = 'https://processwire.com/store/login-register-pro/docs/';

		$this->getKeyInputfield($module, $inputfields);

		/** @var InputfieldCheckboxes $f */
		$f = $modules->get('InputfieldCheckboxes');
		$f->attr('name', 'features');
		$f->label = $this->_('Features');
		$f->description = $this->_('What features would you like to enable? (all are recommended)'); 
		$f->notes = sprintf($this->_('Please see the [LoginRegisterPro documentation](%s) for instructions.'), $docsUrl); 
		$f->icon = 'sliders';
		$f->set('themeOffset', 1); 
		$f->addOption('login', $this->_('**Login:** front-end login form'));
		$f->addOption('register', $this->_('**Register:** new user registration form with email confirmation'));
		$f->addOption('profile', $this->_('**Profile:** edit profile form for logged-in users'));
		$f->addOption('log', sprintf($this->_('**Log:** user activity in %s log'), "[login-register-pro]($logUrl)"));
		$f->attr('value', $module->features);
		$inputfields->add($f);
		
		/*** LOGIN ******************************************************************************/

		/** @var InputfieldFieldset $fieldset */
		$fieldset = $modules->get('InputfieldFieldset');
		$fieldset->attr('_id+name', '_login_settings');
		$fieldset->label = $this->_('Login');
		$fieldset->description = $this->_('Settings for the login form.');
		$fieldset->showIf = 'features=login';
		$fieldset->set('themeOffset', 1);
		$fieldset->icon = 'sign-in';
		$fieldset->collapsed = Inputfield::collapsedYes;
		$inputfields->add($fieldset);

		/** @var InputfieldCheckboxes $f */
		$f = $modules->get('InputfieldCheckboxes');
		$f->attr('name', 'logins');
		$f->label = $togglesLabel;
		$f->icon = 'sliders';
		$f->addOption('throttle', $this->_('Throttle logins to block dictionary attacks') . '*');
		$f->addOption('honey', $this->_('Protect login form with JS-based honeypot'));
		$f->addOption('roles', $this->_('Choose which roles are allowed to use the front-end login form')); 
		$f->addOption('forgot', $this->_('Allow use of “forgot password” reset action') . '†');
		$f->addOption('tfa', $this->_('Allow two-factor authentication (see instructions to setup)'));
		$f->addOption('noindex', $this->_('Block search engines/robots from indexing URLs having login/register/forgot output (recommended)'));
		$f->attr('value', $module->logins);
		if($hasThrottle) {
			$url = './edit?name=SessionLoginThrottle&collapse_info=1';
			$label = $this->_('Always enabled while “%s” module is installed');
			$f->notes .= '* ' . sprintf($label, 'SessionLoginThrottle') . ' - ' . '[' . $configureLabel . "]($url)\n";
		} else {
			$url = './installConfirm?name=SessionLoginThrottle';
			$f->notes .= '* ' . sprintf($requiresModuleLabel, 'SessionLoginThrottle') . ' - ' . '[' . $installLabel . "]($url)\n";
		}
		if($hasForgot) {
			$url = './edit?name=ProcessForgotPassword&collapse_info=1';
			$f->notes .= '† ' . sprintf($requiresModuleLabel, 'ProcessForgotPassword') . " - [$configureLabel]($url)\n";
		} else {
			$url = './installConfirm?name=ProcessForgotPassword';
			$f->notes .= '† ' . sprintf($requiresModuleLabel, 'ProcessForgotPassword') . " - [$installLabel]($url)\n";
		}
		$fieldset->add($f);
		
		/** @var InputfieldCheckboxes $f */
		$f = $modules->get('InputfieldCheckboxes');
		$f->attr('name', 'loginRoles');
		$f->label = $this->_('Roles allowed to login on the front-end');
		$f->description = $this->_('If no roles are selected then ALL users (regardless of roles) are allowed to login with this module.');
		$f->notes = sprintf($this->_('Select only the “%s” role to limit login just to users that have registered with this module.'), LoginRegisterPro::defaultRoleName);
		$f->showIf = 'logins=roles';
		$f->icon = 'gears';
		$value = array();
		foreach($module->wire('roles') as $role) {
			if($role->name == 'guest') continue;
			$option = "$role->name:$role->id";
			$f->addOption($option, $role->name);
			if(in_array($option, $module->loginRoles) || in_array($role->name, $module->loginRoles)) $value[] = $option;
		}
		$f->attr('value', $value);
		$fieldset->add($f);
		
		if(version_compare($this->wire()->config->version, '3.0.163', '>=')) {
			// Tfa specific configuration
			/** @var ProcessLogin $processLogin */
			$processLogin = $modules->getModule('ProcessLogin', array('noInit' => true, 'noCache' => true)); 
			$tfaFieldset = $processLogin->getTfaConfigInputfields(array(
				'tfaAutoType' => $module->tfaAutoType,
				'tfaAutoRoleIDs' => $module->tfaAutoRoleIDs,
				'tfaRecRoleIDs' => array(), 
				'tfaRememberDays' => $module->tfaRememberDays,
				'tfaRememberFingerprints' => $module->tfaRememberFingerprints,
			));
			$tfaFieldset->showIf = 'logins=tfa';
			$f = $tfaFieldset->getChildByName('tfaRecRoleIDs');
			if($f) $tfaFieldset->remove($f);
			$fieldset->add($tfaFieldset);
		}

		
		/*** REGISTER ******************************************************************************/
	
		/** @var InputfieldFieldset $fieldset */
		$fieldset = $modules->get('InputfieldFieldset');
		$fieldset->attr('_id+name', '_register_settings');
		$fieldset->label = $this->_('Register');
		$fieldset->description = $this->_('Settings for the registration form and processing.');
		$fieldset->showIf = 'features=register';
		$fieldset->icon = 'paw';
		$fieldset->set('themeOffset', 1);
		$fieldset->collapsed = Inputfield::collapsedYes;
		$inputfields->add($fieldset);
		
		/** @var InputfieldCheckboxes $f */
		$f = $modules->get('InputfieldCheckboxes');
		$f->attr('name', 'registers');
		$f->label = $togglesLabel;
		if($module->recaptchaSite && $module->recaptchaSecret) {
			$recaptchaIcon = ' ✓ ';	
			$f->notes = $recaptchaIcon . $this->_('reCAPTCHA settings are populated.');
		} else {
			$recaptchaIcon = '* ';	
			$f->notes = $recaptchaIcon . $this->_('To use you must also populate reCAPTCHA API keys near the end of this configuration screen.');
		}
		$f->icon = 'sliders';
		$f->addOption('recaptcha', $this->_('Use Google reCAPTCHA on registration form') . $recaptchaIcon);
		$f->addOption('honey', $this->_('Protect registration form with JS-based honeypot'));
		$f->addOption('lists', $this->_('Limit what emails are allowed to register (whitelist and/or blacklist)'));
		$f->addOption('stopforumspam', $this->_('Block emails or IPs submitted to stopforumspam.com from registering'));
		$f->addOption('hidemail', $this->_('Prevent email addresses from appearing in register/confirm URLs'));
		// $f->addOption('noalias', $this->_('Prevent email aliases (like foo+baz@bar.com) from creating more than 1 account'));  
		$f->attr('value', $module->registers);
		$fieldset->add($f);
	
		$f = $this->getFieldsAsmSelect('registerFields', $this->_('Registration form fields'));
		$fieldset->add($f);
		
		/** @var InputfieldCheckboxes $f */
		$f = $modules->get('InputfieldCheckboxes');
		$f->attr('name', 'registerRoles');
		$f->label = $this->_('Role(s) to add to newly registered and confirmed users');
		$f->description = $this->_('We recommend you add the “login-register” role for newly registered users.');
		$f->notes = $this->_('Roles with page-edit permission are not shown.');
		$f->showIf = 'features=register';
		$f->icon = 'gears';
		$f->columnWidth = 50;
		$value = array();
		foreach($module->wire('roles') as $role) {
			if($role->name == 'guest' || $role->name == 'superuser') continue;
			if($role->hasPermission('page-edit')) continue;
			$option = "$role->name:$role->id";
			$f->addOption($option, $role->name);
			if(in_array($option, $module->registerRoles) || in_array($role->name, $module->registerRoles)) $value[] = $option;
		}
		$f->attr('value', $value);
		$fieldset->add($f);
		
		/** @var InputfieldInteger $f */
		$f = $modules->get('InputfieldInteger');
		$f->attr('name', 'registerExpire');
		$f->label = $this->_('Number of seconds after which non-confirmed registrations expire');
		$f->notes = $this->_('600=10 minutes, 3600=1 hour, 86400=1 day');
		$f->icon = 'clock-o';
		$f->attr('value', $module->registerExpire);
		$f->columnWidth = 50;
		$fieldset->add($f);
	
		if(wireClassExists('WireRandom')) {
			/** @var InputfieldRadios $f */
			$f = $modules->get('InputfieldRadios');
			$f->attr('name', 'codeMethod');
			$f->label = $this->_('Confirmation code type');
			$f->description = 
				$this->_('The confirmation code is randomly generated at runtime.') . ' ' .
				$this->_('Cryptographically secure random generation is used when available to PHP.');
			$f->addOption('alphanumeric', $this->_('Alphanumeric'));
			$f->addOption('ALPHANUMERIC', $this->_('Alphanumeric uppercase'));
			$f->addOption('alpha', $this->_('Alpha'));
			$f->addOption('ALPHA', $this->_('Alpha uppercase'));
			$f->addOption('numeric', $this->_('Numeric'));
			$f->attr('value', $module->codeMethod);
			$f->columnWidth = 50;
			$fieldset->add($f);

			/** @var InputfieldInteger $f */
			$f = $modules->get('InputfieldInteger');
			$f->attr('name', 'codeLength');
			$f->label = $this->_('Confirmation code length');
			$f->description = $this->_('Enter a fixed length between 6 and 60 for randomly generated confirmation codes, or specify 0 for random length between 10 and 40.');
			$f->min = 0;
			$f->max = 60;
			$f->attr('value', $module->codeLength);
			$f->columnWidth = 50;
			$f->notes = $this->_('Example with current type and length settings:') . "\n`" . $module->codes()->create($module->codeLength, $module->codeMethod) . '`';
			$fieldset->add($f);
		}
		
		/** @var InputfieldCheckboxes $f */
		$f = $modules->get('InputfieldCheckboxes');
		$f->attr('name', 'confirms');
		$f->label = $this->_('Confirmation screen options'); 
		$f->description = 
			$this->_('The confirmation screen is what appears after the user clicks the link from their email and it asks for their confirmation code.') . ' ' . 
			$this->_('Select additional options you would also like to enable here.'); 
		$f->addOption('pass', $this->_('Ask user to enter password as another confirmation'));
		$f->addOption('login', $this->_('Ask user to login after account creation (leave this unchecked to login automatically)'));
		$f->attr('value', $module->confirms); 
		$fieldset->add($f);
		
		/** @var InputfieldTextarea $f */
		$f = $modules->get('InputfieldTextarea');
		$f->attr('name', 'emailWhitelist');
		$f->label = $this->_('Email domain/host whitelist');
		$f->icon = 'thumbs-o-up';
		$f->description =
			$this->_('Specify exact email domains/hostnames allowed to register, one per line.');
		$f->notes =
			$this->_('An example of where this is useful is if you are running a company intranet and only want employees to register.') . ' ' .
			$this->_('You may specify exact host/domain like `domain.com` or `subdomain.domain.com`.') . ' ' .
			$this->_('If nothing is specified here, then this feature is not used.');
		$f->showIf = 'registers=lists';
		$f->columnWidth = 50;
		$f->attr('value', $module->emailWhitelist);
		$fieldset->add($f);

		/** @var InputfieldTextarea $f */
		$f = $modules->get('InputfieldTextarea');
		$f->attr('name', 'emailBlacklist');
		$f->label = $this->_('Email blacklist');
		$f->icon = 'thumbs-o-down';
		$f->description =
			$this->_('Specify partial match strings (one per line) to match in any part of email.');
		$f->notes =
			$this->_('Emails that match are disallowed. You may specify entire hosts/domains, or any string to match in any part of the email address.') . ' ' .
			$this->_('Note that this is different from how the email whitelist works.');
		$f->showIf = 'registers=lists';
		$f->columnWidth = 50;
		$f->attr('value', $module->emailBlacklist);
		$fieldset->add($f);

	
		
		/*** CONFIRMATION EMAIL *************************************************************************/

		/** @var InputfieldFieldset $fieldset */
		$fieldset = $modules->get('InputfieldFieldset');
		$fieldset->attr('id+name', '_email_settings');
		$fieldset->label = $this->_('Confirm');
		$fieldset->showIf = 'features=register';
		$fieldset->description =
			$this->_('Email settings are used for the confirmation email that is sent after new account registrations.') . ' ' .
			$this->_('In email subject and body, you may use placeholders: `{host}` for HTTP host, `{url}` for confirmation URL, `{code}` for confirmation code, or any `{field}` in your registration form.');

		$fieldset->icon = 'envelope-o';
		$fieldset->set('themeOffset', 1);
		$fieldset->collapsed = Inputfield::collapsedYes;
		$inputfields->add($fieldset);

		/** @var InputfieldEmail $f */
		$f = $modules->get('InputfieldText');
		$f->attr('name', 'emailSubject');
		$f->label = $this->_('Subject');
		$f->attr('value', $module->emailSubject);
		$fieldset->add($f);

		/** @var InputfieldEmail $f */
		$f = $modules->get('InputfieldEmail');
		$f->attr('name', 'fromEmail');
		$f->label = $this->_('From email address');
		$f->description = $this->_('Email address to use for notifications sent to the user (or blank for default).');
		$f->attr('value', $module->fromEmail);
		$f->columnWidth = 50;
		$fieldset->add($f);

		/** @var InputfieldEmail $f */
		$f = $modules->get('InputfieldText');
		$f->attr('name', 'fromName');
		$f->label = $this->_('From name');
		$f->description = $this->_('Optional company/person name that accompanies the “from email” address.');
		$f->attr('value', $module->fromName);
		$f->columnWidth = 50;
		$fieldset->add($f);

		/** @var InputfieldTextarea $f */
		$f = $modules->get('InputfieldTextarea');
		$f->attr('name', 'emailHtml');
		$f->label = $this->_('Body HTML');
		$f->attr('value', $module->emailHtml);
		$f->attr('rows', 7);
		$f->columnWidth = 50;
		$fieldset->add($f);

		/** @var InputfieldTextarea $f */
		$f = $modules->get('InputfieldTextarea');
		$f->attr('name', 'emailText');
		$f->label = $this->_('Body TEXT');
		$f->attr('value', $module->emailText);
		$f->attr('rows', 7);
		$f->columnWidth = 50;
		$fieldset->add($f);
		
		/** @var InputfieldRadios $f */
		$mailer = $this->wire('mail')->new();
		$mailerName = $mailer ? $mailer->className() : 'WireMail';
		$f = $modules->get('InputfieldRadios');
		$f->attr('name', 'wireMailer');
		$f->label = $this->_('Email send method');
		$f->addOption('',
			$this->_('Auto-select:') . ' ' .
			'[span.detail]' . sprintf($this->_('Currently %s'), $mailerName) . '[/span]'
		);
		$f->addOption('WireMail',
			$this->_('PHP mail():') . ' ' .
			'[span.detail]'  . $this->_('base WireMail') . '[/span]'
		);
		foreach($modules->findByPrefix('WireMail') as $wireMailerName) {
			$info = $modules->getModuleInfoVerbose($wireMailerName);
			$label = str_replace('WireMail', '', $wireMailerName);
			$f->addOption($wireMailerName, "$label: [span.detail] $info[summary] [/span]");
		}
		$f->notes = sprintf($this->_('For more options install [WireMail modules](%s).'), 'https://modules.processwire.com/search/?q=WireMail');
		$f->attr('value', $module->wireMailer);
		$fieldset->add($f);
	
		/** @var InputfieldEmail $f */
		$f = $this->modules->get('InputfieldEmail');
		$f->attr('name', 'notifyEmail');
		$f->label = $this->_('Admin notification email');
		$f->description = 
			$this->_('Email address to send notifications to when new users are confirmed/created or files are uploaded.') . ' ' . 
			$this->_('Other related notifications may also be sent to this address.') . ' ' . 
			$this->_('Leave blank to disable.');
		$f->attr('value', $module->notifyEmail);
		$fieldset->add($f);


		/*** PROFILE ******************************************************************************/

		/** @var InputfieldFieldset $fieldset */
		$fieldset = $modules->get('InputfieldFieldset');
		$fieldset->attr('_id+name', '_profile_settings');
		$fieldset->label = $this->_('Profile');
		$fieldset->description = $this->_('Settings for the profile form and processing.');
		$fieldset->showIf = 'features=profile';
		$fieldset->icon = 'universal-access';
		$fieldset->set('themeOffset', 1);
		$fieldset->collapsed = Inputfield::collapsedYes;
		$inputfields->add($fieldset);
		
		$f = $this->getFieldsAsmSelect('profileFields', $this->_('Profile form fields'));
		$f->icon = 'address-card-o';
		$fieldset->add($f);

		/** @var InputfieldCheckboxes $f */
		$f = $modules->get('InputfieldCheckboxes');
		$f->attr('name', 'askPassFields');
		$f->icon = 'lock';
		$f->label = $this->_('Require password for profile changes');
		$f->description =
			$this->_('Require current password for changes to checked fields in Profile form.') . ' ' .
			$this->_('If you make changes to the selected profile fields above, save and return here to show them in this field.');
		$profileFields = $module->fieldIds($module->get('profileFields'), 'name');
		if(!isset($profileFields['email'])) $profileFields['email'] = $this->wire('fields')->get('email')->id;
		foreach($profileFields as $fieldName => $fieldId) {
			$f->addOption($fieldId, $fieldName);
		}
		$f->val($module->fieldIds($module->askPassFields));
		$fieldset->add($f);
		
		/** @var InputfieldCheckboxes $f */
		$f = $modules->get('InputfieldCheckboxes');
		$f->attr('name', 'profiles');
		$f->label = $togglesLabel;
		$f->icon = 'sliders';
		$f->addOption('delete', $this->_('Allow users to delete their own account'));
		$f->attr('value', $module->profiles);
		$fieldset->add($f);
		
		/** @var InputfieldText $f */
		$f = $modules->get('InputfieldText');
		$f->attr('name', 'maxFilesize');
		$f->label = $this->_('Max allowed file size for file upload fields (if used)');
		$f->icon = 'cloud-upload';
		$f->description = 
			$this->_('Specify in bytes or megabytes, or leave blank to delegate to PHP’s settings.') . ' ' . 
			$this->_('When using megabytes, append “M” to the value to clarify it is a megabytes value, i.e. “5M” for 5 megabytes.'); 
		$f->notes = sprintf($this->_('Value cannot exceed PHP’s `upload_max_filesize` setting, which is currently `%s`'), ini_get('upload_max_filesize')); 
		$f->attr('size', 6); 
		$f->attr('value', $module->maxFilesize); 
		$fieldset->add($f);

		/*** RECAPTCHA ******************************************************************************/
		
		$fieldset = $module->recaptcha()->getConfigInputfields();
		$fieldset->set('themeOffset', 1);
		$fieldset->collapsed = Inputfield::collapsedYes;
		$inputfields->add($fieldset);
		
		/*** OTHER **********************************************************************************/
	
		$fieldset = $this->modules->get('InputfieldFieldset');
		$fieldset->attr('_id+name', '_other_settings');
		$fieldset->label = $this->_('Other');
		$fieldset->description = $this->_('Other settings unrelated to any specific feature.');
		$fieldset->icon = 'life-buoy';
		$fieldset->set('themeOffset', 1);
		$fieldset->collapsed = Inputfield::collapsedYes;
		$inputfields->add($fieldset);
	
		/** @var InputfieldInteger $f */
		$f = $this->modules->get('InputfieldInteger');
		$f->attr('name', 'pageId'); 
		$f->label = $this->_('Main page ID (set automatically at runtime)'); 
		$f->description = $this->_('ID of the main page used by this module for all features. This is the page that uses your “login-register” template.'); 
		$f->notes = $this->_('This value is set automatically at front-end runtime. Change it only if this module is generating incorrect feature links.'); 
		$f->attr('value', $module->pageId); 
		$fieldset->add($f);
		
	}
	
	/**
	 * Get Inputfield for key
	 *
	 * @param LoginRegisterPro $module
	 * @param InputfieldWrapper $inputfields
	 * @return Inputfield|null
	 *
	 */
	public function getKeyInputfield(LoginRegisterPro $module, InputfieldWrapper $inputfields) {

		/** @var Inputfield $f */
		$f = $this->wire('modules')->get('InputfieldText');
		$name = 'cen' . 'seK' . 'ey';
		$name = "li$name";
		$moduleName = $module->className();
		$f->attr('id+name', $name);
		$key = $module->get($name) ? $module->get($name) : '';
		$input = $this->wire('input');
		$session = $this->wire('session');
		$config = $this->wire('config');
		$result = false;
		$notes = "If you did not purchase $moduleName for this site, please [purchase here](https://processwire.com/talk/store/).";
		$suppressNote = "If you want to prevent your key from appearing here in the module config, append a dash/hyphen to it and save again.";

		if(strpos($key, '-')) {
			// showing of key is suppressed
			$f2 = $this->wire('modules')->get('InputfieldMarkup');
			$f2->description = "$moduleName is a commercially licensed and supported module. $notes";
			$inputfields->add($f2);
			return null;
		}

		if($input->post($name) && rtrim($input->post($name),'-') !== $key) {
			// validate 
			$http = new WireHttp();
			$val = trim($this->wire('sanitizer')->text($input->post($name)));
			if(strlen($val)) {
				$data = array(
					'action' => 'validate',
					'li' . 'cense' => rtrim($val, '-'),
					'host' => $config->httpHost,
					'ip' => ip2long($session->getIP())
				);
				try {
					$result = $http->post('https://processwire.com/validate-product/', $data);
				} catch(\Exception $e) { }
				if(!$result) try {
					$result = $http->post('http://processwire.com/validate-product/', $data);
				} catch(\Exception $e) { }
				if($result === 'valid') {
					$key = $val;
					$f->notes = "Validated!";
					$this->message("$moduleName key has been validated!");
				} else if(empty($result)) {
					$key = '';
					$this->error("Error making http connection for validation. Please email ryan@processwire.com for manual validation.");
				} else {
					$key = '';
					$this->error("Unable to validate key: $result");
				}
			}
		}

		if(empty($key)) $input->post->__unset($name);
		$f->attr('value', $key);
		$f->required = true;
		$f->label = $this->_('Product key');
		if($key) $f->label .= " - VALIDATED!";
		$f->attr('value', $config->demo ? 'disabled for demo mode' : $key);
		$f->icon = $key ? 'check-square-o' : 'question-circle';
		$f->description = "Paste in your $moduleName product key.";
		$f->notes = $notes;
		if($key) $f->notes = "$suppressNote\n$f->notes";
		if($key) $f->collapsed = Inputfield::collapsedYes;
		$inputfields->add($f);

		return $f;
	}


	/**
	 * Get AsmSelect Inputfield for configuring registration or profile fields
	 *
	 * @param string $name
	 * @param string $label
	 * @return InputfieldAsmSelect
	 *
	 */
	public function getFieldsAsmSelect($name, $label) {

		$module = $this->module;
		$adminUrl = $this->config->urls->admin;

		/** @var ProcessTemplate|null $processTemplate */
		static $processTemplate = null;
		if($processTemplate === null) $processTemplate = $this->modules->get('ProcessTemplate');
		$processTemplate->headline($module->className()); // since ProcessTemplate may set headline

		$userTemplate = $this->templates->get('user');
		$userTemplateUrl = $adminUrl . 'setup/template/edit?id=' . $userTemplate->id;
		$userFieldgroup = $userTemplate->fieldgroup;
		$requiredFieldNames = array('email', 'pass');
		$contextNamespace = $name;
		$value = $module->fieldIds($module->get($name));
		$formFieldsDocUrl = 'https://processwire.com/store/login-register-pro/docs/#register-and-profile-form-fields';

		$editLink = $adminUrl .
			"setup/field/edit" .
			"?id={value}" .
			"&fieldgroup_id=$userFieldgroup->id" .
			"&modal=1" .
			"&process_template=1" .
			"&context_namespace=$contextNamespace" .
			"&context_label=" . urlencode($label);

		/** @var InputfieldAsmSelect $f */
		$f = $this->modules->get('InputfieldAsmSelect');
		$f->attr('name', $name);
		$f->icon = 'address-book-o';
		$f->label = $label;
		$f->description =
			sprintf($this->_('Please read the [Register and Profile form fields](%s) section of the documentation for instructions and important considerations.'), $formFieldsDocUrl) . ' ' . 
			sprintf($this->_('If you do not see all the fields you need, add them to the [user](%s) template first and then come back here.'), $userTemplateUrl) . ' ' .
			$this->_('After adding fields, save, then come back and you may click the fields to customize additional details.'); 

		foreach($requiredFieldNames as $requiredName) {
			$requiredField = $this->fields->get($requiredName);
			if($requiredField && !in_array($requiredField->id, $value)) $value[] = $requiredField->id;
		}

		$f->setAsmSelectOption('sortable', true);
		$f->setAsmSelectOption('fieldset', true);
		$f->setAsmSelectOption('editLink', $editLink);
		$f->setAsmSelectOption('hideDeleted', false);

		foreach($module->getUserFieldgroupFields($contextNamespace) as $fieldName => $field) {
			if($name === 'registerFields' && wireInstanceOf($field->type, 'FieldtypeFile')) continue;
			if(wireInstanceOf($field->type, 'FieldtypeRepeater')) continue;
			$fieldLabel = $fieldName;
			if(in_array($fieldName, $requiredFieldNames)) $fieldLabel .= ' ' . $this->_('(required)');
			$attrs = $processTemplate->getAsmListAttrs($field, true);
			$f->addOption($field->id, $fieldLabel, $attrs);
		}

		$f->attr('value', $value);

		return $f;
	}
}
