<?php namespace ProcessWire;

/**
 * LoginRegisterException
 * 
 * Exception used rather than WireException when getMessage() returns something okay for user to see
 * 
 */
class LoginRegisterException extends WireException { }

class LoginRegisterProException extends LoginRegisterException { }