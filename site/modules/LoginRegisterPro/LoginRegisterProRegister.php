<?php namespace ProcessWire;

/**
 * ProcessWire Login Register Pro: Registration form
 *
 * DO NOT DISTRIBUTE, this is a commercial module.
 *
 * Copyright 2020 by Ryan Cramer Design, LLC
 * 
 * @method InputfieldForm build() Build the form
 * @method InputfieldForm|bool process(array $data = array()) Proces the form 
 * @method string render(array $data = array()) Render the form markup
 * @method string execute() Render or process the form and return markup or redirect to self
 * @method InputfieldForm ready(InputfieldForm $form) Called when form is ready to be rendered or processed
 * 
 * @method bool allowRegistration(InputfieldForm $form) Allow registration with given submitted form data?
 * @method void buildConfirmationEmail(WireMail $mail, $emailTo, $confirmCode, $confirmUrl) 
 * @method bool|int sendConfirmationEmail($email, $code) Send confirmation email.
 *
 */
class LoginRegisterProRegister extends LoginRegisterProForm {
	
	/**
	 * Execute render or processing of register form
	 *
	 * @return string
	 *
	 */
	public function ___execute() {
		
		$processed = false;
		$form = $this->form();
		
		if($this->submitted()) {
			try {
				$processed = $this->process();
				if(1 > 2) throw new LoginRegisterException(); // to satisfy IDE
			} catch(LoginRegisterException $e) {
				$processed = false;
				$error = $e->getMessage();
				if($error) $form->error($error);
			}
		}
		
		if($processed) {
			$emailInput = $form->getChildByName('register_email');
			$email = $emailInput ? $emailInput->val() : '';
			if($this->allow('hidemail')) $email = $this->loginRegister()->tools()->encodeEmail($email);
			$this->loginRegister->redirect(array(
				'register_confirm' => 1,
				'register_email' => $email,
			));
		} else {
			// exception occurred or disguised error like honeypot
		}
		
		return $this->render();
	}

	/**
	 * Create a new registration and send confirmation email on success
	 * 
	 * This method is not currently used by LoginRegisterPro and is here purely for API
	 * usage if someone needs it.
	 * 
	 * A LoginRegisterException is thrown on all error conditions.
	 * 
	 * @param string $email Email address for new account
	 * @param string $pass Password for new account
	 * @param array $data Associative array of any other fields to populate when new account created
	 * @return bool Returns true on success, throws LoginRegisterException on error
	 * @throws LoginRegisterException
	 * 
	 */
	public function create($email, $pass, array $data) {
		
		$sanitizer = $this->sanitizer;
		$codes = $this->loginRegister->codes();
		$email = $sanitizer->email($email);
		$password = new Password();
		$form = $this->form();
		$emailInput = $form->getChildByName('register_email'); /** @var InputfieldEmail $emailInput */
		$passInput = $form->getChildByName('register_pass'); /** @var InputfieldPassword $passInput */
		$password->pass = $pass;
		$emailInput->val($email);
		$passInput->val($pass);
		$error = '';
		$userName = '';
		
		if(strlen($email)) {
			$userName = $this->loginRegister->emailToName($email, array(
				// 'noalias' => $this->allow('noalias'), 
			));
		}

		if(empty($email) || empty($userName)) {
			$error = 'Invalid email'; // @todo translatable pharases here
		} else if(empty($pass)) {
			$error = 'Password required';
		} else if(!$this->loginRegister->allowUserEmail($email)) {
			$error = 'Email already in use or not allowed';
		} else if(!$this->loginRegister->allowUserName($userName, $error)) {
			$error = 'Registration error - ' . $error;
		} else if(!$passInput->isValidPassword($pass)) {
			$error = 'Password error - ' . implode(', ', $passInput->getErrors(true));
		} else if(!$this->allowRegistration($form)) {
			$error = 'Registration disallowed';
		}

		if(strlen($error)) throw new LoginRegisterException($error);
		
		$values = array(
			'register_name' => $userName,
			'register_email' => $email, 
			'register_pass' => array($password->hash, $password->salt),
		);
		
		// ensure all given data values begin with "register_" prefix	
		foreach($data as $key => $value) {
			if(strpos($key, 'register_') !== 0) $key = "register_$key";
			if(empty($values[$key])) $values[$key] = $value;
		}

		// setup confirmation code
		do {
			$code = $codes->create($this->loginRegister->codeLength, $this->loginRegister->codeMethod);
		} while(!$codes->add($code, $email, LoginRegisterProCodes::typeRegister, $values));

		// send confirmation email
		$this->sendConfirmationEmail($email, $code);
		$this->logSuccess("New registration", $email);
		
		return true;
	}

	/**
	 * Build
	 * 
	 * @return InputfieldForm
	 * 
	 */
	public function ___build() {

		$userTemplate = $this->wire('templates')->get('user');
		$registerFields = $this->loginRegister->fieldIds($this->loginRegister->registerFields);
		$languages = $this->wire('languages'); /** @var Languages|null $languages */

		/** @var InputfieldForm $form */
		$form = parent::___build();
		$form->description = $this->_('Register for an account');

		if(!$this->allow('email', 'login')) {
			/** @var InputfieldText $f */
			$f = $this->modules->get('InputfieldText');
			$f->attr('id+name', 'register_name');
			$f->label = $this->_('Login name');
			$f->description = $this->_('Use letters, numbers, hyphens or underscores. No spaces.');
			$f->required = true;
			$f->attr('required', 'required');
			$form->add($f);
		}

		// if(!in_array('email', $registerFields)) $registerFields[] = 'email';
		// if(!in_array('pass', $registerFields)) $registerFields[] = 'pass';
		$nullPage = new NullPage();
		$this->wire($nullPage);

		foreach($registerFields as $fieldId) {
			$field = $userTemplate->fieldgroup->getFieldContext($fieldId, 'registerFields');
			if(!$field) continue;
			if($field->name == 'roles') continue;
			$inputfield = $field->getInputfield($nullPage);
			$inputfield->attr('id+name', 'register_' . $inputfield->attr('name'));
			if($field->name == 'email' || $field->name == 'pass') $inputfield->required = true;
			/*
			if($field->name == 'pass' && $inputfield->get('newPassLabel') === 'New password') {
				$inputfield->set('newPassLabel', $this->_('Password'));
			}
			*/
			if($inputfield->required && $inputfield instanceof InputfieldText) {
				if(!$inputfield->showIf && !$inputfield->requiredIf) {
					$inputfield->attr('required', 'required');
				}
			}
			if($field->name === 'language' && $languages) {
				// make current user language default selection
				$inputfield->val($this->wire('user')->language);
			}
			$form->add($inputfield);
		}

		// honeypot
		/** @var InputfieldTextarea $f */
		$f = $this->modules->get('InputfieldTextarea');
		$f->attr('id+name', 'register_comment');
		$f->label = $this->_('Comment');
		$f->wrapAttr('style', 'display:none');
		$form->add($f);

		if($this->allow('recaptcha')) {
			$f = $this->modules->get('InputfieldMarkup');
			$f->attr('name', 'profile_recaptcha');
			$f->attr('value', $this->loginRegister->recaptcha()->render()); 
			//$siteKey = $this->sanitizer->entities($this->loginRegister->recaptchaSite);
			// $f->value = "<div class='g-recaptcha' data-sitekey='$siteKey'></div>";
			$form->add($f);
		}

		/** @var InputfieldSubmit $f */
		$f = $this->modules->get('InputfieldSubmit');
		$f->attr('id+name', 'register_submit');
		$f->attr('value', $this->_('Register'));
		$form->add($f);

		return $this->ready($form);
	}

	/**
	 * Process
	 * 
	 * @return bool
	 * @throws LoginRegisterException
	 * 
	 */
	public function ___process() {
		
		/** @var Sanitizer $sanitizer */
		$sanitizer = $this->wire('sanitizer');

		/** @var Languages|null $languages */
		$languages = $this->wire('languages');

		$form = parent::___process();

		if($this->allow('recaptcha')) {
			$f = $form->getChildByName('profile_recaptcha');
			if($f && !$this->loginRegister->recaptcha()->process()) {
				throw new LoginRegisterException($this->_('Failed reCAPTCHA input test'));
			}
		}

		$emailInUseError = $this->_('Email address is already in use or not allowed, please use another.');
		$nameIsEmail = false;

		// check if honeypot is populated
		$honeypotField = $form->getChildByName('register_comment');
		$honeypot = $honeypotField->attr('value');
		if(strlen($honeypot)) return false;

		// get requested password
		$passField = $form->getChildByName('register_pass');
		$pass = $passField->attr('value');
		$password = new Password();
		$this->wire($password);
		$password->pass = $pass;

		// validate email
		$emailField = $form->getChildByName('register_email');
		$email = $sanitizer->email($emailField->attr('value'));
		if(strlen($email) && !$this->loginRegister->allowUserEmail($email)) {
			if(!count($emailField->getErrors())) $emailField->error($emailInUseError);
			$emailField->attr('value', '');
		}

		// validate requested user name
		$error = '';
		if($this->allow('email', 'login')) {
			$nameIsEmail = true;
			$name = $this->loginRegister->emailToName($emailField->attr('value'), array(
				// 'noalias' => $this->allow('noalias'), 
			));
			if(!$this->loginRegister->allowUserName($name, $error)) {
				if(empty($error)) $error = $emailInUseError; // fallback just in case
				if(!count($emailField->getErrors())) $emailField->error($emailInUseError);
			}
		} else {
			$nameField = $form->getChildByName('register_name');
			$name = $nameField->attr('value');
			if(!$this->loginRegister->allowUserName($name, $error)) {
				if(empty($error)) $error = "Fail"; // unlikely fallback 
				$nameField->error($error);
			}
		}

		// determine if any errors are present
		$errors = $form->getErrors();
		if(!count($errors) && !empty($error)) $errors[] = $error; // unlikely fallback

		// if there were errors, the form will be displayed again
		if(empty($errors) && (!strlen($name) || !strlen($pass) || !strlen($email))) {
			$errors[] = $this->_('Missing required value');
		}
		
		if(!$this->allowRegistration($form)) {
			if(empty($errors)) $errors[] = $this->_('Registration disallowed');
		}
		
		if(count($errors)) {
			// errors occurred, we will report and return
			foreach($errors as $error) {
				$this->error($error);
				$this->logError($error, ($nameIsEmail && !empty($email)) ? $email : $name);
			}
			throw new LoginRegisterException('');
		}
		
		// if we reach this point, there were no errors 
		$values = array(
			'register_name' => $name,
			'register_pass' => array($password->hash, $password->salt),
		);
		
		// populate all field values into array
		foreach($form->getAll() as $f) {
			$nameAttr = $f->attr('name');
			if($nameAttr === 'register_submit' || $nameAttr === 'register_pass') continue;
			if(strpos($nameAttr, 'register_') !== 0) continue;
			if($f instanceof InputfieldPassword) continue;
			$value = $f->attr('value');
			if(is_object($value)) {
				$value = "$value"; // @todo confirm working with Page fields
				if(ctype_digit($value)) $value = (int) $value;
			}
			$values[$nameAttr] = $value;
		}

		// ensure selected language is used in confirmation email
		if(isset($values['register_language']) && $languages) {
			$languageField = $form->getChildByName('register_language');
			$language = $languageField ? $languageField->val() : null;
			if($language && $language->id && $language->id != $this->wire('user')->language->id) {
				$languages->setLanguage($language);
			}
		}

		// setup confirmation code
		$codes = $this->loginRegister->codes();
		$codeLength = $this->loginRegister->codeLength;
		$codeMethod = $this->loginRegister->codeMethod;
		$codeResult = null;
		$n = 0;
		
		do {
			$code = $codes->create($codeLength, $codeMethod);
			$codeResult = $codes->add($code, $email, LoginRegisterProCodes::typeRegister, $values);
		} while(!$codeResult && ++$n < 100);
		
		if(!$codeResult) return false;

		// send confirmation email
		$this->sendConfirmationEmail($email, $code);
		$this->logSuccess("New registration", $nameIsEmail ? $email : $name);

		return true;	
	}
	
	/**
	 * Process submitted Recaptcha v2 request and return true on success, false on fail
	 *
	 * @return bool
	 *
	protected function processRecaptcha() {

		$response = $this->input->post('g-recaptcha-response');
		if(empty($response)) return false;
		if(strlen($response) > 2048) $response = substr($response, 0, 2048);

		$secret = $this->loginRegister->recaptchaSecret;
		if(empty($secret)) return false;

		$http = new WireHttp();
		$result = $http->post('https://www.google.com/recaptcha/api/siteverify', array(
			'secret' => $secret,
			'response' => $response,
			'remoteip' => $this->session->getIP()
		));

		if(!$result) return false;
		$result = json_decode($result, true);

		return isset($result['success']) ? (bool) $result['success'] : false;
	}
	 */

	/**
	 * Allow registration with given data? (just for hooks)
	 * 
	 * #pw-hooker
	 * 
	 * @param InputfieldForm $form
	 * @return bool
	 * 
	 */
	public function ___allowRegistration(InputfieldForm $form) {
		$allow = true;
		
		if($this->allow('stopforumspam')) {
			$email = $form->getChildByName('register_email')->val();
			$ip = $this->session->getIP();
			if($this->matchesStopForumSpam($email, $ip)) $allow = false;
		}
		
		return $allow;
	}

	/**
	 * Does given email or IP appear in stopforumspam.com database?
	 * 
	 * @param string $email
	 * @param string $ip
	 * @return bool
	 * 
	 */
	protected function matchesStopForumSpam($email, $ip) {
	
		$matches = false;
		$http = new WireHttp();
		$url = "http://api.stopforumspam.org/api?json";
		if(strlen($ip)) $url .= "&ip=" . urlencode($ip);
		if(strlen($email)) $url .= "&email=" . urlencode($email);
		
		try {
			$data = $http->getJSON($url);
		} catch(\Exception $e) {
			$this->logError($e->getMessage());
			$data = null;
		}
		
		// http://api.stopforumspam.org/api?ip=192.168.1.2&email=testing@xrumer.ru&json
		// Example: {
		//  "success":1,
		//  "ip":{"frequency":0,"appears":0,"asn":null},
		//  "email":{"lastseen":"2019-12-12 14:38:19","frequency":255,"appears":1,"confidence":99.95}
		// }
		
		if(!empty($data) && !empty($data['success'])) {
			if(!empty($data['email']['appears'])) {
				$matches = true;
				$this->logError("Registrant email $email failed stopforumspam.com check");
			}
			if(!empty($data['ip']['appears'])) {
				$matches = true;
				$this->logError("Registrant IP $ip failed stopforumspam.com check");
			}
		}
		
		return $matches;
	}
	
	/********************************************************************************************
	 * EMAIL: CONFIRMATION
	 *
	 */

	/**
	 * Build the confirmation email body
	 *
	 * This hook receives the WireMail object and should directly populate the bodyHTML, body, and
	 * optionally subject or anything else.
	 *
	 * @param WireMail $mail
	 * @param string $emailTo
	 * @param string $confirmCode
	 * @param string $confirmUrl
	 *
	 */
	protected function ___buildConfirmationEmail(WireMail $mail, $emailTo, $confirmCode, $confirmUrl) {

		$vars = array(
			'url' => $confirmUrl, 
			'code' => $confirmCode, 
			'host' => $this->config->httpHost, 
			'fromEmail' => $this->loginRegister->fromEmail,
			'fromName' => $this->loginRegister->fromName,
		);
		
		foreach($this->form()->getAll() as $f) {
			$name = $f->attr('name');
			if(strpos($name, 'register_') !== 0) continue;
			$name = str_replace('register_', '', $name);
			if(!isset($vars[$name])) $vars[$name] = $f->val();
		}
		
		$emailHtml = $this->loginRegister->emailHtml;
		$emailText = $this->loginRegister->emailText;
		$emailSubject = $this->loginRegister->emailSubject;

		$mail->to($emailTo);
		$mail->bodyHTML(wirePopulateStringTags($emailHtml, $vars, array('entityEncode' => true)));
		$mail->body(wirePopulateStringTags($emailText, $vars)); 
		$mail->subject(wirePopulateStringTags($emailSubject, $vars));
		
		if($vars['fromEmail']) $mail->from($vars['fromEmail']);
		if($vars['fromName']) $mail->fromName($vars['fromName']);
	}

	/**
	 * Send an email with a confirmation code they have to click on (or paste in)
	 *
	 * @param string $email
	 * @param string $confirmCode
	 * @return int 1 on success 0 on fail
	 *
	 */
	public function ___sendConfirmationEmail($email, $confirmCode) {

		$confirmUrl = $this->loginRegister->url(array(
			'register_confirm' => $confirmCode,
			'register_email' => $this->allow('hidemail') ? $this->loginRegister()->tools()->encodeEmail($email) : $email, 
		));

		$mail = $this->loginRegister->getWireMail();
		$this->buildConfirmationEmail($mail, $email, $confirmCode, $confirmUrl);

		return $mail->send();
	}


}	