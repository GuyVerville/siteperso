<?php namespace ProcessWire;

/**
 * ProcessWire Login Register Pro: Profile form
 *
 * DO NOT DISTRIBUTE, this is a commercial module.
 *
 * Copyright 2021 by Ryan Cramer Design, LLC
 * 
 * @method InputfieldForm build() Build the form
 * @method InputfieldForm|bool process(array $data = array()) Proces the form
 * @method string render(array $data = array()) Render the form markup
 * @method string execute() Render or process the form and return markup or redirect to self
 * @method InputfieldForm ready(InputfieldForm $form) Called when form is ready to be rendered or processed
 * 
 * @method bool saveReady(User $user, array $changes) Hook called right before profile is saved, return false to disallow
 * @method void saved(User $user, array $changes) Hook called after user has updated their profile/saved
 * @method bool emailChangeReady(User $user, $oldEmail, $newEmail) Called right before email change, return false to disallow change
 * @method void emailChanged(User $user, $oldEmail, $newEmail) Hook called after email has been changed but before user saved
 * @method bool userNameChangeReady(User $user, $oldName, $newName)
 * @method void userNameChanged(User $user, $oldName, $newName)
 * @method bool deleteUserReady(User $user) Hook called right before user deletes themself, return false to disallow deletion
 * @method void deletedUser(User $user) Hook called right after user has deleted themselves
 *
 */
class LoginRegisterProProfile extends LoginRegisterProForm {

	/**
	 * Contains value of HTTP_X_FIELDNAME when ajax field is processed
	 * 
	 * @var string
	 * 
	 */
	protected $ajaxFieldName = '';
	
	/**
	 * Render or process profile form
	 *
	 * @return string
	 *
	 */
	public function ___execute() {
		
		if($this->loginRegister->notifyEmail) {
			$this->addHookBefore('InputfieldFrontendFile::filesAdded', $this, 'hookFilesAdded');
		}
		
		if(isset($_SERVER['HTTP_X_FIELDNAME'])) {
			$this->ajaxFieldName = $this->sanitizer->fieldName($_SERVER['HTTP_X_FIELDNAME']);
		}
		if($this->submitted()) {
			$this->process();
			if($this->ajaxFieldName) {
				$this->ajaxResponse();
			} else {
				$this->loginRegister->redirect($this->url());
			}
		}
		return $this->render();
	}
	
	protected function ajaxResponse() {
		$response = array(
			'success' => true,
			'messages' => array(),
		);
		foreach($this->loginRegister->getNotices() as $notice) {
			if($notice instanceof NoticeError) $response['success'] = false;
			$response['messages'][] = $notice->text; 
		}
		ob_end_clean();
		header('Content-Type: application/json');
		echo json_encode($response); 
		exit; 
	}
	
	public function submitted() {
		return parent::submitted() || strlen($this->ajaxFieldName) > 0;
	}

	/**
	 * Build profile form 
	 * 
	 * @return InputfieldForm
	 * 
	 */
	public function ___build() {
		
		/** @var User $user */
		$user = $this->wire('user');
		/** @var Fields $fields */
		$fields = $this->wire('fields');
		
		$of = $user->of();
		$user->of(false);
		$askPassFields = $this->loginRegister->fieldNames($this->loginRegister->askPassFields);
		$profileFields = $this->loginRegister->fieldIds($this->loginRegister->profileFields);
		// $userTemplate = $this->wire('templates')->get('user');

		$form = parent::___build();
		$form->description = $this->_('Edit profile');
		
		$ajaxFieldName = str_replace('profile_', '', $this->ajaxFieldName);
		
		// do we have a front-end safe file Inputfield?
		$hasFileInputfield = $this->modules->isInstalled('InputfieldFrontendFile');

		foreach($profileFields as $fieldId) {

			$field = $fields->get($fieldId); // field without context intentional
			if(!$field || $field->name == 'roles') continue;
			$fieldProperties = array();
			$isFileField = false;
			
			if($ajaxFieldName && $field->name !== $ajaxFieldName) continue; // just a single field via ajax
			
			if($field->type instanceof FieldtypeFile) {
				// convert FieldtypeFile to use InputfieldFrontendFile for input
				if(!$hasFileInputfield) continue; // skip field if not installed
				$fieldProperties['inputfieldClass'] = $field->get('inputfieldClass');
				$field->setQuietly('inputfieldClass', 'InputfieldFrontendFile');
				$isFileField = true;
			}

			$contextField = $user->fieldgroup->getFieldContext($field, 'profileFields');
			$inputfield = $contextField->getInputfield($user);
			if(!$inputfield) $inputfield = $user->getInputfield($field->name);
			if(!$inputfield) continue;
			if($ajaxFieldName && !$inputfield->get('allowProcessAjax')) continue; // not allowed for ajax
			
			$inputfield->setAttribute('value', $user->getUnformatted($field->name));
			$inputfield->set('hasPage', $user);
			$inputfield->set('hasField', $contextField); 
			$inputfield->set('hasFieldtype', $field->type); 
			
			if($isFileField) {
				$inputfield->set('uploadUrl', $form->attr('action'));
				if($this->loginRegister->maxFilesize) {
					// because InputfieldFile does not use a configurable maxFilesize
					$inputfield->set('maxFilesize', $this->loginRegister->maxFilesize);
				}
			}
			
			if($field->name === 'email') {
				$inputfield->attr('required', 'required');
			}

			if($field->name === 'tfa_type') {
				if(!$inputfield->val()) {
					// initialize manually so it can add hooks (it just does some visual/wording tweaks)
					$tfa = $this->wire(new Tfa()); /** @var Tfa $tfa */
					$tfa->init();
				}
				if(!in_array('tfa_type', $askPassFields)) {
					// require this to be an ask pass field
					$askPassFields[] = 'tfa_type';
				}
				// keep name as tfa_type (no 'profile_' prefix)
			} else {
				$inputfield->attr('name', 'profile_' . $inputfield->attr('name'));
			}

			if(in_array($field->name, $askPassFields)) {
				// password required to change value for this field
				$inputfield->set('_askPass', true);
				$inputfield->addClass('ProfilePassAsk', 'wrapClass');
			}
			
			$form->add($inputfield);
		
			// set any changed values back to what they were
			foreach($fieldProperties as $key => $val) {
				$field->setQuietly($key, $val);
			}
		}

		if($this->allow('delete') && !$ajaxFieldName) {

			$deleteRequestKey = '';
			if($this->input->requestMethod('POST')) $deleteRequestKey = $this->sessionGet('deleteRequestKey');
			if(!strlen($deleteRequestKey)) {
				$rand = new WireRandom();
				$deleteRequestKey = $rand->alphanumeric(0, array('fast' => true));
				$this->sessionSet('deleteRequestKey', $deleteRequestKey);
			}
			/** @var InputfieldCheckbox $inputfield */
			$inputfield = $this->modules->get('InputfieldCheckbox');
			$inputfield->attr('name', 'profile_delete');
			$inputfield->attr('id', 'ProfileDelete');
			$inputfield->attr('value', $deleteRequestKey);
			$inputfield->label = $this->_('Delete your account?');
			$inputfield->label2 = $this->_('Check this box to request account deletion');
			$form->add($inputfield);

			$inputfield = $this->modules->get('InputfieldText');
			$inputfield->attr('name', 'profile_delete_email');
			$inputfield->attr('id', 'ProfileDeleteEmail');
			$inputfield->label = $this->_('Are you sure you want to delete your account?');
			$inputfield->description =
				$this->_('Enter your EMAIL address to confirm you want to permanently DELETE your account now.');
			$inputfield->addClass('ProfilePassAsk InputfieldStateHidden', 'wrapClass');
			$inputfield->set('_askPass', true);
			$form->add($inputfield);
			$askPassFields[] = 'profile_delete_email';
		}

		if(count($askPassFields)) {
			/** @var InputfieldPassword $inputfield */
			$inputfield = $this->modules->get('InputfieldText');
			$inputfield->attr('type', 'password');
			$inputfield->attr('name', 'profile_pass_ask');
			$inputfield->label = $this->_('Your current password');
			$inputfield->description = $this->_('One or more fields you changed above require your current password in order to proceed.');
			$inputfield->attr('id', 'ProfilePassAsk');
			$inputfield->addClass('InputfieldStateHidden', 'wrapClass');
			// ensure ProfilePass field is visible if javascript happens to be disabled
			$form->appendMarkup .=
				"<noscript>" .
				"<p><strong>" . $this->_('Please enable Javascript to access all profile editing features.') . "</strong></p>" .
				"<style type='text/css'>#wrap_ProfilePassAsk.InputfieldStateHidden { display: block; }</style>" .
				"</noscript>";
			$form->add($inputfield);
		}

		/** @var InputfieldSubmit $f */
		$f = $this->modules->get('InputfieldSubmit');
		$f->attr('id+name', 'profile_submit');
		$form->add($f);

		if($of) $user->of(true);

		return $this->ready($form);
	}

	/**
	 * Process
	 * 
	 * @return bool|mixed|InputfieldForm
	 * 
	 */
	public function ___process() {

		/** @var User $user */
		$user = $this->wire('user');
		$of = $user->of();
		$user->of(false);
		$user->resetTrackChanges();
		
		$form = parent::___process();

		/** @var WireInput $input */
		$input = $this->wire('input');

		$ajaxFieldName = $this->ajaxFieldName;
		
		/*
		$tfaField = $form->getChildByName('tfa_type');
		if($tfaField) {
			if($tfaField->val()) {
				$this->modules->getModule($tfaField->val());
			}
		}
		*/

		$totalErrors = 0;
		$updates = array(); // changes to DB or non-DB related update

		$passVerifyError = $this->_('Value not updated because existing/current password was not verified');
		$passVerified = false;
		$askPassFields = $this->loginRegister->fieldNames($this->loginRegister->askPassFields);
		$askPassInput = $form->getChildByName('profile_pass_ask');

		if($askPassInput) {
			$pass = $askPassInput->val();
			if(strlen($pass)) {
				$passVerified = $user->pass->matches($pass);
			}
		}

		if(!$ajaxFieldName) {
			
			// password
			$passField = $form->getChildByName('profile_pass');
			if($passField) {
				$pass = $passField->val();
				if(strlen($pass)) {
					if(in_array('pass', $askPassFields) && !$passVerified) {
						$passField->error($passVerifyError);
					} else {
						$user->set('pass', $pass);
						$updates['pass'] = $passField->label;
					}
				}
			}

			// email
			$emailField = $form->getChildByName('profile_email');
			if($emailField) {
				$emailChanged = strtolower($emailField->val()) != strtolower($user->email);
				if($emailChanged && in_array('email', $askPassFields) && !$passVerified) {
					$emailField->error($passVerifyError);
					$emailChanged = false;
				}
				if($emailChanged) {
					if($this->processEmailChange($user, $emailField)) {
						$updates['email'] = $emailField->label;
					}
				}
			}
		}

		$skipNames = array('pass', 'submit', 'roles', 'email', 'pass_ask', 'delete', 'delete_email');

		// all other form fields
		foreach($form->getAll() as $f) {
			$name = $f->attr('name');
			
			if($ajaxFieldName) {
				if($name !== $ajaxFieldName || !$f->get('allowProcessAjax')) continue;
			}	
			
			$name = str_replace('profile_', '', $name);
			$numErrors = count($f->getErrors());
			$totalErrors += $numErrors;

			if(in_array($name, $skipNames)) continue; // not allowed for processing OR already processed above
			
			$formValue = $f->val();
			$userValue = $user->getUnformatted($name);
		
			if($f->isChanged()) {
				$isChanged = true;
			} else if(is_array($userValue) || is_array($formValue)) {
				if(is_array($formValue) && $userValue instanceOf WireData) {
					$isChanged = $formValue != $userValue->getArray(); // i.e. Combo
				} else {
					$isChanged = $userValue != $formValue;
				}
			} else {
				$isChanged = "$userValue" !== "$formValue";
			}

			if(!$numErrors && $user->hasField($name) && $isChanged) {
				if($f->getSetting('_askPass') && !$passVerified) {
					$f->error($passVerifyError);
					$totalErrors++;
				} else {
					$user->set($name, $formValue);
					$updates[$name] = $f->label;
				}
			}
		}

		// delete user option
		$allowDelete = $this->allow('delete') && $input->post('profile_delete') && $passVerified;
		if($allowDelete && !$ajaxFieldName && !$user->isSuperuser()) {
			// logout and redirect on success, returns on fail
			$this->processProfileFormDeleteUser($user, $form);
		}

		$savedLabel = $this->_('Saved profile');
		$updatedLabel = $this->_('Updated: %s');
		$noChangesLabel = 'no changes';
		$changes = $user->getChanges(); // changes to DB
		
		foreach($updates as $name => $label) {
			$this->message(sprintf($updatedLabel, $label));
		}

		if($this->saveReady($user, $changes)) {
			$user->save();
			$this->saved($user, $changes);
			$this->message($savedLabel);
			$changes = array_unique(array_merge(array_keys($updates), array_values($changes)));
			$changesLabel = implode(', ', $changes);
			if(empty($changesLabel)) $changesLabel = $noChangesLabel;
			$this->logSuccess("Profile updated ($changesLabel)", $user);
		} else if(count($updates)) {
			// one or more updates that do not involve DB changes (like file isTemp changes)
		} else {
			$this->message($savedLabel . ' ' . $this->_('(no changes)'));
		}

		if($of) $user->of(true);

		return $totalErrors === 0;
	}
	
	/**
	 * Process email address changes in profile form
	 *
	 * @param User $user
	 * @param Inputfield $emailField
	 * @return bool True if email updated, false if not
	 *
	 */
	protected function processEmailChange(User $user, Inputfield $emailField) {

		$email = $this->sanitizer->email($emailField->val());
		if(!strlen($email)) return false;
		$emailPrev = $user->email;
		$userName = '';

		if(!$this->loginRegister->allowUserEmail($email, $user, false)) {
			// email is in use
			if(!count($emailField->getErrors())) {
				$emailField->error($this->_('Email address was already in use, so changed back to previous.'));
			}
			$emailField->val($user->email);
			$this->logError("Attempted to change to email already in use ($email)", $user);
			$user->of(false); // anything that find/get and matches $user can enable of() again
			return false;
		}

		// email changed allowed
		if($this->allow('email', 'login')) {
			// update name to be consistent with email
			$userName = $this->loginRegister->emailToName($email);
			$userNamePrev = $this->loginRegister->emailToName($emailPrev);
			if($user->name === $userNamePrev) {
				// username is mirroring email address, so update username along with email
				$reason = '';
				if($this->loginRegister->allowUserName($userName, $reason, $user)) {
					// user’s name will be updated along with their email
				} else {
					// email change resulted in user name not allowed
					$emailField->error($this->_('Unable to change to new email address') . " ($reason)");
					$emailField->val($user->email);
					$this->logError("Can’t change email to $email ($reason)", $user);
					$userName = '';
					$email = '';
				}
			} else {
				// user.name is not mirroring email address, do not change user.name
				// this is the case for admin users or users created prior to LRP install
				$userName = '';
			}
		}

		$user->of(false); // anything that find/get and matches $user can enable of() again

		if(strlen($email) && $email !== $user->email) { 
			if($this->emailChangeReady($user, $emailPrev, $email)) {
				$user->set('email', $email);
				if(strlen($userName) && $userName != $user->name) { 
					$userNamePrev = $user->name;
					if($this->userNameChangeReady($user, $userNamePrev, $userName)) {
						// update user.name
						$user->set('name', $userName);
						$this->userNameChanged($user, $userNamePrev, $userName);
					} else {
						$this->logError("User name change blocked by userNameChangeReady hook: $userNamePrev to $userName", $user);
					}
				}
				$this->emailChanged($user, $emailPrev, $email);
				return true;
			} else {
				$this->logError("Email change blocked by emailChangeReady hook: $emailPrev to $email", $user);
			}
		}

		return false;
	}

	/**
	 * User deletion function in profile form
	 *
	 * @param User $user
	 * @param InputfieldForm $form
	 *
	 */
	protected function processProfileFormDeleteUser(User $user, InputfieldForm $form) {

		$input = $this->input;
		$deleteRequestKey = $input->post('profile_delete');
		$emailConfirm1 = $input->post('profile_delete_email');
		$emailConfirm2 = $form->getChildByName('profile_delete_email')->val();

		if($user->isSuperuser()) {
			$form->error($this->_('Account deletion not supported for superuser.'));
			return;
		}

		if($deleteRequestKey !== $this->sessionGet('deleteRequestKey')) {
			$form->error($this->_('Invalid data found in delete request'));
			return;
		}

		if(!$emailConfirm1 || ($emailConfirm1 !== $emailConfirm2) || ($emailConfirm2 !== $user->email)) {
			$form->error($this->_('The email you entered to confirm delete did not match your actual email address'));
			return;
		}

		$userName = $this->allow('email', 'login') ? $user->email : $user->name;
		$success = false;

		try {
			if($this->deleteUserReady($user)) {
				$success = $this->wire('users')->delete($user);
				if(!$success) throw new LoginRegisterException('Unknown error deleting user');
			}
		} catch(\Exception $e) {
			$this->error($this->_('Error deleting your account, please contact administrator for assistance.'));
			$this->logError("Error deleting user account: " . $e->getMessage(), $userName);
		}

		if($success) {
			$this->deletedUser($user);
			$this->message($this->_('Your account has been permanently deleted.'));
			$this->logSuccess('User deleted their account', $userName);
			$this->loginRegister->emailAdmin(
				$this->_('User deleted account'),
				sprintf($this->_('User completed deletion of their own account: %s'), $userName)
			);
			$this->loginRegister->executeLogout(); // redirects from here
		}
	}

	/**
	 * Hook to InputfieldFrontendFile::filesAdded()
	 * 
	 * @param HookEvent $event
	 * 
	 */
	public function hookFilesAdded(HookEvent $event) {

		/** @var Sanitizer $sanitizer */
		$sanitizer = $this->wire('sanitizer');
		/** @var User $user */
		$user = $this->wire('user');
		$userName = $user->get('email|name');
		$body = sprintf($this->_('User %s has uploaded a file:'), $userName);
		$bodyHTML = "<html><body>\n<p>" . $sanitizer->entities1($body) . "</p>";
		$files = $event->arguments(0);
		$config = $this->wire('config');
		$editUrl = ($config->https ? 'https' : 'http') . "://$config->httpHost" . $user->editUrl();
		$body .= "\n";
		
		foreach($files as $pagefile) {
			/** @var Pagefile|Pageimage $pagefile */
			$ext = strtoupper($pagefile->ext());
			$info = "$pagefile->filesizeStr $ext";
			if($pagefile instanceof Pageimage) $info .= " {$pagefile->width}x{$pagefile->height}";
			
			$httpUrl = $pagefile->httpUrl();
			$basename = $pagefile->basename;
			$body .= "\n$basename ($info)\n$httpUrl\n";
			
			$httpUrl = $sanitizer->entities($httpUrl); 
			$basename = $sanitizer->entities($basename);
			$img = '';
			if(in_array($ext, array('JPG', 'JPEG', 'GIF', 'PNG'))) {
				$img = "\n<p><img src='$httpUrl' alt='' /></p>";
			}
			
			$bodyHTML .= "$img\n<p><a href='$httpUrl'>$basename</a> ($info)</p>";
		}

		$reviewLabel = $this->_('You can review or remove files here:');
		$body .= "\n$reviewLabel\n$editUrl\n\n";
		$bodyHTML .= "\n<p>$reviewLabel <a href='$editUrl'>" . $this->_('edit user') . "</a></p>";
		
		$subject = sprintf($this->_('New file uploads: %s'), $userName);
	
		$this->loginRegister->emailAdmin($subject, $body, $bodyHTML);
	}
	
	protected function ___saveReady(User $user, array $changes) { 
		if($changes) {}
		return $user->isChanged();
	}

	protected function ___saved(User $user, array $changes) { 
		if($user && $changes) {}
	}
	
	protected function ___emailChangeReady(User $user, $oldEmail, $newEmail) {
		if($user && $oldEmail && $newEmail) {}
		return true;
	}
	
	protected function ___userNameChangeReady(User $user, $oldName, $newName) {
		if($user && $oldName && $newName) {}
		return true;
	}
	
	protected function ___emailChanged(User $user, $oldEmail, $newEmail) {
		$this->logSuccess("Email changed from $oldEmail to $newEmail", $user);
	}
	
	protected function ___userNameChanged(User $user, $oldName, $newName) {
		$this->logSuccess("User name changed from $oldName to $newName", $user);
	}
	
	protected function ___deleteUserReady(User $user) {
		return $user->id > 0;
	}
	
	protected function ___deletedUser(User $user) { }

}	