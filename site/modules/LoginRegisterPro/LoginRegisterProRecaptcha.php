<?php namespace ProcessWire;

/**
 * ProcessWire Login Register Pro: reCAPTCHA v2 checkbox implementation 
 *
 * Copyright 2019 by Ryan Cramer Design, LLC
 * 
 * @method string getScriptUrl()
 * @method string renderScript()
 * @method string render()
 * @method bool process()
 *
 */

class LoginRegisterProRecaptcha extends Wire {
	
	protected $siteKey = '';
	protected $secretKey = '';
	protected $gotScript = false;

	/**
	 * LoginRegisterProRecaptcha constructor.
	 *
	 * @param string $siteKey Site (public) key from Google
	 * @param string $secretKey Secret (private) key from Google
	 * 
	 */
	public function __construct($siteKey, $secretKey) {
		$this->siteKey = $siteKey;
		$this->secretKey = $secretKey;
	}
	
	public function ___getScriptUrl() {
		$this->gotScript = true;
		return 'https://www.google.com/recaptcha/api.js';
	}
	
	public function ___renderScript() {
		$url = $this->getScriptUrl();
		return "<script src='$url' async defer></script>";
	}

	/**
	 * Render Recaptcha input widget
	 * 
	 * @return string
	 * 
	 */
	public function ___render() {
		$out = '';
		if(!$this->gotScript) $out .= $this->renderScript();
		$siteKey = $this->sanitizer->entities($this->siteKey);
		$out .= "<div class='g-recaptcha' data-sitekey='$siteKey'></div>";
		return $out;
	}

	/**
	 * Process Recapthca respose, return true if valid, false if not
	 * 
	 * @return bool
	 * @throws WireException
	 * 
	 */
	public function ___process() {

		$response = (string) $this->input->post('g-recaptcha-response');
		$length = strlen($response); 
		
		if(!$length) {
			$this->log('post.g-recaptcha-response was empty');
			return false;
		}
		
		if($length > 2048) {
			$this->log("post.g-recaptcha-response was truncated because too long ($length bytes)");
			$response = substr($response, 0, 2048);
		}

		$secret = $this->secretKey;
		if(empty($secret)) {
			$this->log('API secret key is empty');
			return false;
		}
		
		$http = new WireHttp();

		try {
			$url = 
				'https://www.google.com/recaptcha/api/siteverify' . 
				'?secret=' . urlencode($secret) . 
				'&response=' . urlencode($response) . 
				'&remoteip=' . urlencode($this->session->getIP());

			$result = $http->post($url);
			/* post method below was not working in one instance (server side firewall of POST data?)
			$result = $http->post('https://www.google.com/recaptcha/api/siteverify', array(
				'secret' => $secret,
				'response' => $response,
				'remoteip' => $this->session->getIP()
			));
			*/
		} catch(\Exception $e) {
			$this->log(wireClassName($e) . " thrown during HTTP post - " . $e->getMessage());
			return false;
		}

		if(empty($result)) {
			$httpError = $http->getError();
			$httpCode = $http->getHttpCode(true);
			if($httpError !== $httpCode) $httpError .= " ($httpCode)";
			$this->log("Received empty response from HTTP post to reCAPTCHA - $httpError");
			return false;
		}
	
		$data = json_decode($result, true);
		
		if($data === false) {
			$this->log("Received non-JSON response from HTTP post to reCAPTCHA - $result");
			return false;
		}
		
		$errorCodes = empty($data['error-codes']) ? '' : '(' . implode(', ', $data['error-codes']) . ') ';
	
		if(!isset($data['success'])) {
			$this->log("reCAPTCHA response has no 'success' key $errorCodes - " . print_r($data, true));
			return false;
		}
		
		if(!$data['success']) {
			$this->log("reCAPTCHA 'success' is false $errorCodes"); 
			return false;
		}
		
		return (bool) $data['success']; 
	}
	
	public function ___log($str = '', array $options = array()) {
		if(strlen($str) && empty($options['name'])) {
			$options['name'] = 'recaptcha-errors';
		}
		return parent::___log($str, $options);
	}

	/**
	 * Get Inputfields to configure reCAPTCHA
	 * 
	 * @return InputfieldFieldset
	 * 
	 */
	public function getConfigInputfields() {
		
		$modules = $this->modules;

		/** @var InputfieldFieldset $fieldset */
		$fieldset = $modules->get('InputfieldFieldset');
		$fieldset->attr('id+name', '_recaptcha_settings');
		$fieldset->icon = 'google';
		$fieldset->label = $this->_('reCAPTCHA');
		$fieldset->description =
			sprintf(
				$this->_('This feature uses the [Google reCAPTCHA v2](%s) checkbox service to confirm that the user is a real person.'),
				'https://developers.google.com/recaptcha/docs/display'
			) . ' ' .
			sprintf(
				$this->_('To use it, you must [obtain an API key pair](%s), and enter them below.'),
				'https://www.google.com/recaptcha/admin'
			);

		/** @var InputfieldText $f */
		$f = $modules->get('InputfieldText');
		$f->attr('name', 'recaptchaSite');
		$f->label = $this->_('Site key');
		$f->attr('value', $this->siteKey);
		$f->columnWidth = 50;
		$fieldset->add($f);

		/** @var InputfieldText $f */
		$f = $modules->get('InputfieldText');
		$f->attr('name', 'recaptchaSecret');
		$f->label = $this->_('Secret key');
		$f->attr('value', $this->secretKey);
		$f->columnWidth = 50;
		$fieldset->add($f);
		
		return $fieldset;
	}
	
}