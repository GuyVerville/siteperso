<?php namespace ProcessWire;

/**
 * ProcessWire Login Register Pro: Tools
 *
 * DO NOT DISTRIBUTE, this is a commercial module.
 *
 * Copyright 2021 by Ryan Cramer Design, LLC
 *
 */

class LoginRegisterProTools extends Wire {

	protected $alphaLower = 'abcdefghijklmnopqrstuvwxyz';
	protected $alphanumericLower = 'abcdefghijklmnopqrstuvwxyz0123456789';
	protected $alphanumericMap = 'aA0bBcCdDeE1fFgG2hHiI3jJkK4lLmM5nNoO6pPqQ7rRsStTuUvV8wWxX9yYzZ';

	/**
	 * Encode an email address for use in URL
	 * 
	 * @param string $email
	 * @param string $at
	 * @return string
	 * 
	 */
	public function encodeEmail($email, $at = '@') {

		$alpha = $this->alphaLower;
		$alphanumeric = $this->alphanumericLower;
		$lphanumeric = ltrim($alphanumeric, 'a');

		$email = strtolower($email);
		$atpos = strpos($email, $at);
		if($atpos === false) return '';
		list($a, $b) = explode($at, $email, 2);

		if($atpos < strlen($alphanumeric)) {
			$atLetter = substr($alphanumeric, $atpos, 1);
		} else {
			$atLetter = '0' . $atpos;
		}

		$val = $a . $b . $atLetter;

		$dotLetter = '';
		$dashLetter = '';
		$digitLetter = '';
		$underLetter = '';
		$plusLetter = '';

		for($n = 0; $n < strlen($lphanumeric); $n++) {
			$letter = $lphanumeric[$n];
			if(strpos($val, $letter) !== false) continue;
			if(!$dotLetter) {
				$dotLetter = $letter;
			} else if(!$dashLetter) {
				$dashLetter = $letter;
			} else if(!$digitLetter) {
				$digitLetter = $letter;
			} else if(!$underLetter) {
				$underLetter = $letter;
			} else if(!$plusLetter) {
				$plusLetter = $letter;
			}
			if($dotLetter && $dashLetter && $digitLetter && $underLetter && $plusLetter) break;
		}

		if($dotLetter) {
			$val = str_replace('.', $dotLetter, $val);
			$val .= $dotLetter;
		} else {
			$val .= 'a';
		}
		if($dashLetter) {
			$val = str_replace('-', $dashLetter, $val);
			$val .= $dashLetter;
		} else {
			$val .= 'a';
		}
		if($underLetter) {
			$val = str_replace('_', $underLetter, $val);
			$val .= $underLetter;
		} else {
			$val .= 'a';
		}
		if($plusLetter) {
			$val = str_replace('+', $plusLetter, $val);
			$val .= $plusLetter;
		} else {
			$val .= 'a';
		}

		$rand = new WireRandom();
		$garbage = strtolower($rand->alpha(0, array('minLength' => 0, 'maxLength' => strlen($alpha)-1)));
		$garbageLength = strlen($garbage);
		$garbageLetter = substr($alpha, $garbageLength, 1);
		$val = $garbage . $val . $garbageLetter;
		$val = str_rot13($val);

		for($n = 0; $n < strlen($alpha); $n++) {
			$letter = substr($alpha, $n, 1);
			if(strpos($val, "$letter$letter") !== false) {
				$val = str_replace("$letter$letter", strtoupper($letter), $val);
			}
		}

		$splitLength = floor((strlen($val)+1) / 2);
		$s1 = substr($val, 0, $splitLength);
		$s2 = substr($val, $splitLength);
		$id = substr($val, 0, 1);
		$val = $s1 . $id . $s2; // repeat first character in middle as identity
		
		$val = $this->randomizeString($val);
		

		return $val;
	}

	/**
	 * Decode a previously encoded email address
	 * 
	 * @param string $val
	 * @param string $at
	 * @return string
	 * 
	 */
	function decodeEmail($val, $at = '@') {
		
		if(strlen($val) < 5 || strlen($val) > 1024) return '';

		$alpha = $this->alphaLower;
		$alphanumeric = $this->alphanumericLower;

		$val = $this->unrandomizeString($val);

		$splitLength = floor((strlen($val)) / 2);
		$id = substr($val, $splitLength, 1);
		if(substr($val, 0, 1) !== $id) return '';
		$val = substr($val, 0, $splitLength) . substr($val, $splitLength+1);

		for($n = 0; $n < strlen($alpha); $n++) {
			$letter = substr($alpha, $n, 1);
			if(strpos($val, strtoupper($letter)) !== false) {
				$val = str_replace(strtoupper($letter), "$letter$letter", $val);
			}
		}

		$val = str_rot13($val);

		$re = '/^(.+?)([a-z0-9]|0\d+)([a-z0-9])([a-z0-9])([a-z0-9])([a-z0-9])([a-z0-9])$/';
		if(!preg_match($re, $val, $matches)) return '';

		$val = $matches[1];
		$atLetter = $matches[2];
		$dotLetter = $matches[3];
		$dashLetter = $matches[4];
		$underLetter = $matches[5];
		$plusLetter = $matches[6]; 
		$garbageLetter = $matches[7];
		$garbageLength = strpos($alphanumeric, $garbageLetter);

		if(strlen($atLetter) > 1 && strpos($atLetter, '0') === 0) {
			$atpos = (int) $atLetter;
		} else {
			$atpos = strpos($alphanumeric, $atLetter);
		}

		$val = substr($val, $garbageLength);
		$email = substr($val, 0, $atpos) . $at . substr($val, $atpos);

		if($dotLetter !== 'a') $email = str_replace($dotLetter, '.', $email);
		if($dashLetter !== 'a') $email = str_replace($dashLetter, '-', $email);
		if($underLetter !== 'a') $email = str_replace($underLetter, '_', $email);
		if($plusLetter !== 'a') $email = str_replace($plusLetter, '+', $email);

		return $email;
	}

	/**
	 * Randomize string 
	 * 
	 * @param string $str
	 * @return string 
	 * 
	 */
	public function randomizeString($str) {
		$alphanumeric = $this->alphanumericMap;
		$length = strlen($alphanumeric);
		$alphanumeric = $alphanumeric . $alphanumeric;
		$randNum = mt_rand(1, $length);
		$s = '';
		for($n = 0; $n < strlen($str); $n++) {
			$c1 = substr($str, $n, 1);
			$pos = strpos($alphanumeric, $c1);
			if($pos === false) {
				$c2 = $c1;
			} else {
				$pos += $randNum;
				$c2 = substr($alphanumeric, $pos, 1);
			}
			$s .= $c2;
		}
		$randNum = dechex($randNum);
		if(strlen($randNum) < 2) $randNum = "0$randNum";
		if(strtoupper(substr($s, 0, 5)) === substr($s, 0, 5)) $randNum = strtoupper("$randNum");
		
		$value = $s . $randNum;
		
		if(!ctype_alnum($value)) {
			$value = '_' . trim(base64_encode($value), '=');
			$value = str_replace('/', '_', $value);
		}
		
		return $value;
	}

	/**
	 * Unrandomize a string
	 * 
	 * @param $str
	 * @return string
	 * 
	 */
	public function unrandomizeString($str) {
		$alphanumeric = $this->alphanumericMap;
		$length = strlen($alphanumeric);
		$alphanumeric .= $alphanumeric;

		if(strpos($str, '_') === 0) {
			$str = substr($str, 1);
			$str = str_replace('_', '/', $str);
			$str = base64_decode($str);
		}
		
		$randNum = substr($str, -2);
		$randNum = hexdec($randNum);
		if($randNum < 1 || $randNum > $length) return '';
		$str = substr($str, 0, strlen($str)-2);
		$s = '';
		for($n = 0; $n < strlen($str); $n++) {
			$c1 = substr($str, $n, 1);
			$pos = strrpos($alphanumeric, $c1);
			if($pos === false) {
				$c2 = $c1;
			} else {
				$pos -= $randNum;
				$c2 = substr($alphanumeric, $pos, 1);
			}
			$s .= $c2;
		}
		return $s;
	}
	
}