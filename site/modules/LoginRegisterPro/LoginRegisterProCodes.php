<?php namespace ProcessWire;

/**
 * ProcessWire Login Register Pro: Codes Database Manager
 *
 * DO NOT DISTRIBUTE, this is a commercial module.
 *
 * Copyright 2019 by Ryan Cramer Design, LLC
 * 
 * @method string create($length = 40, $method = 'alphanumeric') Create a new confirmation code
 * @method bool add($code, $email, $type, array $values)
 * @method bool delete($code, $email, $type)
 * @method bool|int|null|array validate($code, $email, $type, $pass = false)
 *
 */
class LoginRegisterProCodes extends LoginRegisterProClass {

	/**
	 * Table created by this class
	 * 
	 */
	const table = 'lrp_codes';
	
	/**
	 * DB code type: registration
	 *
	 */
	const typeRegister = 0;

	/**
	 * DB code type: reset
	 *
	 */
	const typeReset = 1;

	/**
	 * validate: Invalid code or email
	 * 
	 */
	const validateInvalid = false;

	/**
	 * validate: Bad password
	 * 
	 */
	const validateBadpass = 0;

	/**
	 * Validate: Max password attempts reached (overflow)
	 * 
	 */
	const validateOverflow = null;

	/**
	 * Create a new confirmation code
	 *
	 * @param int $length
	 * @param string $method One of: alphanumeric, ALPHANUMERIC, alpha, ALPHA, numeric
	 * @return string
	 *
	 */
	public function ___create($length = 40, $method = 'alphanumeric') {
		if(wireClassExists('WireRandom')) {
			$rand = new WireRandom();
			$options = array(
				'disallow' => '0OlI'
			);
			if(strtoupper($method) === $method) {
				$options['lower'] = false;
				$method = strtolower($method);
			}
			$code = $rand->$method($length, $options);
		} else {
			$pw = new Password();
			$code = $pw->randomBase64String($length);
		}
		return $code;
	}

	/**
	 * Add a new confirmation code and values associated with it
	 * 
	 * @param string $code
	 * @param string $email
	 * @param int $type
	 * @param array $values
	 * @return bool
	 * 
	 */
	public function ___add($code, $email, $type, array $values) {
		
		if(!strlen($code)) return false;
		$this->checkTable();

		/** @var WireDatabasePDO $database */
		$database = $this->wire('database');
		$table = self::table;
		$sql = "INSERT INTO $table SET `type`=:type, `code`=:code, `email`=:email, `data`=:data, `created`=:created";
		
		$query = $database->prepare($sql);
		$query->bindValue(":type", $type, \PDO::PARAM_INT);
		$query->bindValue(":code", $code);
		$query->bindValue(":email", $email);
		$query->bindValue(":data", json_encode($values));
		$query->bindValue(":created", time());

		try {
			$result = $query->execute();
		} catch(\Exception $e) {
			$result = false;
		}

		return $result;
	}
	
	/**
	 * Delete confirmation code
	 *
	 * @param string $code
	 * @param string $email
	 * @param int $type
	 * @return bool
	 *
	 */
	public function ___delete($code, $email, $type) {
		$table = self::table;
		$sql = "DELETE FROM $table WHERE `type`=:type AND `code`=:code AND `email`=:email";
		$query = $this->database->prepare($sql);
		$query->bindValue(':type', $type);
		$query->bindValue(':code', $code);
		$query->bindValue(':email', $email);
		$query->execute();
		$qty = $query->rowCount();
		$query->closeCursor();
		return $qty > 0;
	}
	
	/**
	 * Validate a confirmation code, email and pass, and return the values saved with that code
	 *
	 * Confirmation code is deleted from DB after being validated (if using DB data)
	 *
	 * @param string $code
	 * @param string $email
	 * @param int $type
	 * @param string|bool $pass Password if applicable
	 * @return bool|int|null|array Returns array on success or a LoginRegisterProCodes::validate* constant on fail: 
	 *  - Boolean false if invalid code or email (validateInvalid)
	 *  - Int 0 if valid except for password (validateBadpass)
	 *  - Null if max password attempts reached (validateOverflow)
	 *  - Array of saved form values if valid
	 *
	 */
	public function ___validate($code, $email, $type, $pass = false) {

		/** @var WireDatabasePDO $database */
		$database = $this->wire('database');
		$table = self::table;
		$sql =
			"SELECT `code`, `email`, `data`, `qty` FROM $table " .
			"WHERE `type`=:type AND `code`=:code AND `email`=:email";
		$query = $database->prepare($sql);
		$query->bindValue(':type', $type, \PDO::PARAM_INT);
		$query->bindValue(':code', $code);
		$query->bindValue(':email', $email);
		$query->execute();
		$count = $query->rowCount();
		$row = $count ? $query->fetch(\PDO::FETCH_ASSOC) : null;
		$query->closeCursor();

		// if we didn't find something valid then exit now
		if(!$count || empty($row) || $row['code'] !== $code) return false;

		// decode data and make sure it is valid
		$data = json_decode($row['data'], true);
		if(empty($data) || empty($data['register_pass'])) return false;

		if($pass !== false) {
			/** @var Password $password Validate password */
			$password = $this->wire(new Password());
			$password->hash = $data['register_pass'][0];
			$password->salt = $data['register_pass'][1];

			if(!$password->matches($pass)) {
				if($row['qty'] > 3) {
					// max attempts reached
					$this->delete($code, $email, $type);
					return null;
				}
				$sql = "UPDATE $table SET qty=qty+1 WHERE `type`=:type AND `code`=:code AND `email`=:email";
				$query = $database->prepare($sql);
				$query->bindValue(':type', $type);
				$query->bindValue(':code', $code);
				$query->bindValue(':email', $email);
				$query->execute();
				return 0;
			}
		}

		$this->delete($code, $email, $type);

		return $data;
	}

	/**
	 * Maintenance: delete rows more than $maxAge seconds old
	 *
	 * @param int $maxAge Max age in seconds
	 * @return int Number of registers deleted
	 *
	 */
	public function deleteExpired($maxAge = 86400) {
		$database = $this->wire('database');
		$table = self::table;
		try {
			$query = $database->prepare("DELETE FROM $table WHERE created<=:created");
			$query->bindValue(":created", time() - $maxAge, \PDO::PARAM_INT);
			$query->execute();
			$result = $query->rowCount();
			$query->closeCursor();
			if($result > 0) $this->logSuccess("Deleted $result expired registrations");
		} catch(\Exception $e) {
			$result = 0;
		}
		return $result;
	}
	
	/**
	 * Check that registration table(s) are ready to use
	 *
	 * @throws WireException|\PDOException
	 *
	 */
	public function checkTable() {
		
		/** @var WireDatabasePDO $database */
		$database = $this->wire('database');
		$table = self::table;
		$query = $database->prepare("SHOW TABLES LIKE '$table'");
		$query->execute();
		$count = $query->rowCount();
		$query->closeCursor();
	
		if($count) return;

		$engine = $this->config->dbEngine;
		$charset = $this->config->dbCharset;
		
		$database->exec(
			"CREATE TABLE $table (" .
			"`type` TINYINT NOT NULL, " .
			"`code` VARCHAR(60) NOT NULL, " .
			"`email` VARCHAR(120) NOT NULL, " .
			"`data` MEDIUMTEXT, " .
			"`created` INT UNSIGNED NOT NULL, " .
			"`qty` TINYINT UNSIGNED NOT NULL DEFAULT 0, " .
			"PRIMARY KEY (`type`, `code`, `email`), " .
			"INDEX created (created) " .
			") ENGINE=$engine DEFAULT CHARSET=$charset"
		);
		
		$this->message("Created table: $table");
	}

	/**
	 * Install
	 * 
	 * @throws WireException
	 * 
	 */
	public function install() {
		$this->checkTable();
	}

	/**
	 * Uninstall
	 * 
	 */
	public function uninstall() {
		try {
			$this->wire('database')->exec("DROP TABLE " . self::table);
			$this->message("Dropped table: " . self::table);
		} catch(\Exception $e) {
			// ignore
		}
	}
}