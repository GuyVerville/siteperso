# LoginRegisterPro Changelog

## Version 6 & 7 (Aug 2021)

- Minor fixes and improvements
- Add option for obfuscating/encrypting email in URLs
- Version 7 fixes an issue with the email encryption

## Version 5 (Feb 2021) 

- Minor fixes and improvements

## Version 4 (Jul 2020)

- Added support for “remember me” in two-factor auth (PW 3.0.159+)
- Added support for forced two-factor auth (PW 3.0.163+)
- Various minor improvements and fixes
- Additional details to be added

## Version 3 (Jan 2020)

- Adds support for front-end file/image upload fields. To support, you must also 
  install InputfieldFrontendFile, available for download in the LoginRegisterPro
  support board. 
  
- Adds support for loading jQuery UI when the Inputfield requests it.   

- Detects if jQuery is being loaded more than once and provides an alert/warning.
  
## Version 2 (Jan 2020)

If upgrading from v1 please visit the module config screen and enable the new
“Block search engines from indexing...” option, unless for some reason it would 
not be suitable for your case.

- Fixes issues with doubled script/style tags.

- Adds a "noindex" option to Login configuration to prevent search engine indexing.

- Improved detection of what links should be displayed and when. 

- Adds a new `$loginRegister->removeLink($name)` method to remove links by name. 

- Adds more options for `$loginRegister->execute($options)` call for more control.

- Adds a new `$loginRegister->renderHead()` method as an alternative to the existing
  renderScripts() and renderStyles() methods.  

## Version 1 (Dec 2019)

- First release version.

