<?php namespace ProcessWire;

/**
 * ProcessWire Login Register Pro: Login form
 *
 * DO NOT DISTRIBUTE, this is a commercial module.
 *
 * Copyright 2021 by Ryan Cramer Design, LLC
 * 
 * @method InputfieldForm build() Build the form
 * @method bool process() Process the form
 * @method string render() Render the form markup
 * @method string execute() Render or process the form and return markup or redirect to self
 * @method InputfieldForm ready(InputfieldForm $form) Called when form is ready to be rendered or processed
 * 
 * @method success(User $user) Hook called on login success
 * @method fail($userName) Hook called on login fail
 * 
 */

class LoginRegisterProLogin extends LoginRegisterProForm {

	/**
	 * Tfa instance populated by execute() method, only when allowed
	 * 
	 * @var Tfa|null 
	 * 
	 */
	protected $tfa = null;

	/**
	 * Name used to login, populated by login() method
	 * 
	 * @var string
	 * 
	 */
	protected $loginName = '';
	
	/**
	 * Execute render or processing of login form
	 *
	 * @return string
	 *
	 */
	public function ___execute() {
		
		$this->useHoneypot = $this->allow('honey');

		// two factor authentication
		$tfa = $this->allow('tfa') ? $this->tfa() : null;
		if($tfa && $tfa->active()) {
			if($tfa->success()) {
				$this->loginRegister->redirect($this->url());
			} else {
				return $tfa->render();
			}
		}

		if($this->submitted()) {
			try {
				if($this->process()) {
					$this->loginRegister->redirect($this->url());
				}
			} catch(\Exception $e) {
				// this is not likely to be reached
				$error = $e->getMessage();
				$this->logError($error, $this->loginName);
			}
		}

		return $this->render();
	}

	/**
	 * Build login form
	 * 
	 * @return InputfieldForm
	 * 
	 */
	public function ___build() {
		
		/** @var Modules $modules */
		$modules = $this->wire('modules');
		/** @var Sanitizer $sanitizer */
		$sanitizer = $this->wire('sanitizer');
		/** @var WireInput $input */
		$input = $this->wire('input');

		$form = parent::___build();
		$form->description = $this->_('Login');

		$useEmail = $this->allow('email', 'login');

		/** @var InputfieldText $nameField */
		if($useEmail) {
			$nameField = $modules->get('InputfieldEmail');
			$nameField->set('label', $this->_('Email')); // Login form: email field label
		} else {
			$nameField = $modules->get('InputfieldText');
			$nameField->set('label', $this->_('Username')); // Login form: username field label
		}
		
		$nameField->attr('id+name', 'login_name');
		$nameField->attr('class', $this->className() . 'Name');
		$nameField->collapsed = Inputfield::collapsedNever;
		$nameField->required = true;
		$nameField->attr('required', 'required');
		$form->add($nameField);

		$nameValue = $sanitizer->text($input->get('login_name'));
		if(strlen($nameValue)) {
			if($useEmail && !strpos($nameValue, '@') && $this->loginRegister->allow('hidemail', 'registers')) {
				$nameValue = $this->loginRegister->tools()->decodeEmail($nameValue);
			}
			$nameValue = $useEmail ? $sanitizer->email($nameValue) : $sanitizer->pageName($nameValue);
			if(strlen($nameValue)) $nameField->attr('value', $nameValue);
		}

		/** @var InputfieldText $passField */
		$passField = $modules->get('InputfieldText');
		$passField->set('label', $this->_('Password')); // Login form: password field label
		$passField->attr('id+name', 'login_pass');
		$passField->attr('type', 'password');
		$passField->attr('class', $this->className() . 'Pass');
		$passField->collapsed = Inputfield::collapsedNever;
		$passField->required = true;
		$passField->attr('required', 'required');
		$form->add($passField);

		/** @var InputfieldSubmit $submitField */
		$submitField = $modules->get('InputfieldSubmit');
		$submitField->attr('name', 'login_submit');
		$submitField->attr('value', $this->_('Login')); // Login form: submit login button 
		$submitField->appendMarkup = $this->session->CSRF->renderInput();
		$form->add($submitField);

		return $this->ready($form);
	}

	/**
	 * Render login form
	 * 
	 * @return string
	 * 
	 */
	public function ___render() {
		return parent::___render(); // . $this->loginRegister->renderLinks();
	}

	/**
	 * Process login form
	 * 
	 * @return bool
	 * @throws WireException
	 * 
	 */
	public function ___process() {
		
		$form = parent::___process();
		$sanitizer = $this->wire()->sanitizer;
		$nameIsEmail = $this->allow('email', 'login');
		$passField = $form->getChildByName('login_pass');
		$nameField = $form->getChildByName('login_name');
		$name = $nameField->attr('value');
		$pass = $passField->attr('value');
		$pass = substr($pass, 0, 128);
		$email = $nameIsEmail && strpos($name, '@') ? $this->loginRegister()->sanitizeEmail($name) : '';
		$errorMsg = '';
		$errorLog = '';
		$user = null;

		if(!strlen($name) || !strlen($pass)) return false;

		if($nameIsEmail && strlen($email)) {
			$emailValue = $sanitizer->selectorValue($email); 
			$matches = $this->wire()->users->find("include=all, email=$emailValue");
			$qty = count($matches);

			if($qty === 1) {
				// only 1 user has this email
				$name = $matches->first()->name;
				$user = $this->login($name, $pass, $errorMsg, $errorLog);
				if($user) $name = $user->name;

			} else if($qty > 1) {
				// multiple users have this email
				$errorMsg = $this->_('Cannot login because more than one account uses this email address.');
				$errorLog = "$email - Login blocked because email used by multiple ($qty) accounts";
			}

			$success = $user && $user->id && strtolower($user->email) === strtolower($email) && $user->name === $name;

		} else if($nameIsEmail) {
			$errorMsg = $this->_('Please use your email address to login');
			$success = false;

		} else {
			// login by user name
			$name = $sanitizer->pageName($name);
			if(!strlen($name)) return false;
			$user = $this->login($name, $pass, $errorMsg, $errorLog);
			$success = $user && $user->id && $user->name === $name;
		}

		if($success) {
			// successful login
			$loginName = $nameIsEmail ? $user->email : $user->name;
			$this->success($user);
			$this->message(sprintf($this->_('Successful login for %s'), $loginName));
			$this->logSuccess('Login', $loginName);
			$result = true;
		} else {
			// login failed
			$this->fail($name);
			if(!$errorMsg) $errorMsg = $this->_('Login failed');
			if(!$errorLog) $errorLog = 'Login failed';
			$this->error($errorMsg);
			$this->logError($errorLog, $name);
			$passField->attr('value', '');
			$result = false;
		}

		return $result;
	}

	/**
	 * Login user
	 * 
	 * @param string $name
	 * @param string $pass
	 * @var string $errorMsg
	 * @var string $errorLog
	 * @return User|null
	 * 
	 */
	protected function login($name, $pass, &$errorMsg, &$errorLog) {
		
		$user = null;
		$tfa = $this->tfa;

		if(!strlen($name)) return null;
		$this->loginName = $name;
		
		if($tfa) {
			$tfa->start($name, $pass);
		}
		
		try {
			$user = $this->wire()->session->login($name, $pass);
			if($user && $user->id) $this->loginName = $user->name;
		} catch(\Exception $e) {
			$errorLog = $e->getMessage();
			if(wireClassName($e) === 'SessionLoginThrottleException' 
				|| strpos($e->getFile(), 'SessionLoginThrottle') !== false) {
				// error okay to report to uesr
				$errorMsg = $errorLog;
			}
		}
		
		return $user;
	}

	/**
	 * Get current Tfa (two-factor auth) module
	 *
	 * @return Tfa|bool
	 * @throws WireException
	 *
	 */
	public function tfa() {

		if($this->tfa !== null) return $this->tfa;

		$tfas = $this->wire()->modules->findByPrefix('Tfa');

		if(count($tfas)) {
			$tfaSettings = array(
				'startUrl' => $this->url(),
				'formAttrs' => array('id' => 'LoginRegisterProTfaForm'),
				'inputLabel' => $this->_('Authentication Code'),
				'showCancel' => false,
				'submitLabel' => '',
				//'cancelLabel' => 'Cancel'
				//'inputAttrs' => array('id' => 'pwlr_auth_code', 'autofocus' => 'autofocus'),
				//'submitAttrs' => array('id' => 'Inputfield_login_submit'),
			);
			$this->tfa = $this->wire(new Tfa());
			if(wireClassExists('RememberTfa')) {
				// ProcessWire 3.0.159+
				$tfaSettings['rememberDays'] = $this->loginRegister->tfaRememberDays;
				$tfaSettings['rememberFingerprints'] = $this->loginRegister->tfaRememberFingerprints;
				// ProcessWire 3.0.163+
				$tfaSettings['autoType'] = $this->loginRegister->tfaAutoType;
				$tfaSettings['autoRoleIDs'] = $this->loginRegister->tfaAutoRoleIDs;
			}
			$this->tfa->setArray($tfaSettings);
		} else {
			$this->tfa = false;
		}

		return $this->tfa;
	}

	/**
	 * Hook called on login success
	 * 
	 * #pw-hooker
	 * 
	 * @param User $user
	 * 
	 */
	protected function ___success(User $user) { }

	/**
	 * Hook called on login fail
	 * 
	 * #pw-hooker
	 * 
	 * @param string $userName
	 * 
	 */
	protected function ___fail($userName) { }

}