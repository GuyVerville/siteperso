# ProcessWire Login/Register Pro

This is a commercial module for ProcessWire 3.x. DO NOT DISTRIBUTE.   
Copyright (C) 2021 by Ryan Cramer Design, LLC

This is a module for ProcessWire 3.x that provides an all-in-one, self 
contained process for rendering and processing user registration, login, 
and profile edits in a manner that is reliable and secure, and with a lot 
of options. LoginRegisterPro takes care of giving you a well qualified and 
authenticated user, and you decide what to do with that user; whether that 
is using ProcessWire’s access control system to control what they can see, 
or using the API to control what happens next. This module is developed by 
and commercially supported by Ryan Cramer, lead developer of ProcessWire CMS.

Full product information and documentation can be found at: 
<https://processwire.com/store/login-register-pro/>

- [Requirements](https://processwire.com/store/login-register-pro/docs/#requirements)
- [Installation](https://processwire.com/store/login-register-pro/docs/#installation)
- [Integration](https://processwire.com/store/login-register-pro/docs/#integration)
- [Customization](https://processwire.com/store/login-register-pro/docs/#customization)


### Terms and conditions

This is a commercial module developed by Ryan Cramer Design, LLC for ProcessWire CMS.
You may not copy or distribute this module, except on site(s) you have registered it 
for with Ryan Cramer Design, LLC. It is of course okay to make copies for use on staging 
or development servers specific to the site you registered for. 

In no event shall the developers of this module be liable for any special, indirect, 
consequential, exemplary, or incidental damages whatsoever, including, without limitation, 
damage for loss of business profits, business interruption, loss of business information, 
loss of goodwill, or other pecuniary loss whether based in contract, tort, negligence, 
strict liability, or otherwise, arising out of the use or inability to use this module, 
even if developers of module have been advised of the possibility of such damages. 

This module is provided “as-is” without warranty of any kind, either expressed or 
implied, including, but not limited to, the implied warranties of merchantability and
fitness for a particular purpose. The entire risk as to the quality and performance
of the program is with you. Should the program prove defective, you assume the cost 
of all necessary servicing, repair or correction. 

The above is what we understand to be necessary legalese for provided software, but 
please know that should you run into any trouble getting the module to work properly, 
we are here to help. Please email support or visit the support forum. Support is 
available at <https://processwire.com/talk/> in the LoginRegisterPro VIP support board. 
You must be logged in with the account you purchased under in order to access the board. 
You can also contact support by sending a PM in the forums to “ryan”, or you can 
email ryan@processwire.com.

### Support and upgrades

This service/software includes 1-year of support (and upgrades as available) through 
the ProcessWire VIP support forum and/or email. The support/upgrades can optionally
be renewed on a yearly basis. If your support/upgrades period has already expired and
you don't have a renewal invoice, simply contact Ryan to reset the renewal invoice
or create a new one. 

Please use the [support forum](https://processwire.com/talk/) for support inquiries. 
To open a support thread, login with the account you purchased under and go to the  
Login Register Pro VIP support board. For sales or renewal inquries, or for confidential 
support inquries that might involve sharing URLs, logins, passwords, etc., please send
a private message (PM) in the forums to user “ryan”. 

-----
Copyright 2021 by Ryan Cramer Design, LLC
