<?php namespace ProcessWire;

/**
 * ProcessWire Login Register Pro: Abstract Form
 *
 * DO NOT DISTRIBUTE, this is a commercial module.
 *
 * Copyright 2021 by Ryan Cramer Design, LLC
 *
 * @method InputfieldForm build() Build the form
 * @method InputfieldForm|bool process(array $data = array()) Proces the form 
 * @method string render(array $data = array()) Render the form markup
 * @method string execute() Render or process the form and return markup or redirect to self
 * @method InputfieldForm ready(InputfieldForm $form) Called when form is ready to be rendered or processed
 *
 */
abstract class LoginRegisterProForm extends LoginRegisterProClass {

	/**
	 * @var InputfieldForm|null
	 * 
	 */
	protected $form = null;

	/**
	 * Check for honeypot as part of submit?
	 * 
	 * @var bool
	 * 
	 */
	protected $useHoneypot = false;

	/**
	 * Get the URL that shows this form
	 * 
	 * @param string|array $queryString
	 * @return string
	 * 
	 */
	public function url($queryString = '') {
		return $this->loginRegister->actionUrl($this->name, $queryString);
	}
	
	/**
	 * Get the InputfieldForm
	 * 
	 * @return InputfieldForm
	 * 
	 */
	public function form() {
		if(!$this->form) $this->form = $this->build();
		return $this->form;
	}

	/**
	 * Build the InputfieldForm
	 * 
	 * @return InputfieldForm
	 * 
	 */
	public function ___build() {
		if($this->form) return $this->form;	
		$name = $this->name;
		/** @var InputfieldForm $form */
		$form = $this->modules->get('InputfieldForm');
		$form->addClass(ucfirst($name) . 'Form');
		$form->attr('name', $name);
		$form->attr('method', 'post');
		$form->attr('action', $this->url());
		$this->form = $form;
		return $form;
	}

	/**
	 * Process the InputfieldForm 
	 * 
	 * @return mixed|InputfieldForm
	 * 
	 */
	public function ___process() {
		$this->wire('session')->CSRF()->validate();
		$this->form()->processInput($this->input->post);
		return $this->form;
	}

	/**
	 * Render the form
	 * 
	 * @return string
	 * 
	 */
	public function ___render() {
		$out = '';
		if($this->allow('honey')) {
			$out .= 
				"<noscript><p>" . 
				$this->_('Error: Javascript not detected and this form requires it.') . ' ' . 
				$this->_('Please enable Javascript in your browser before using this form.') . 
				"</p></noscript>";
		}
		$out .= $this->form()->render();
		$out = str_replace('</form>', $this->wire('session')->CSRF->renderInput() . '</form>', $out); 
		return $out;
	}

	/**
	 * Execute render or process
	 * 
	 * @return string
	 * 
	 */
	public function ___execute() {
		$this->form();
		if($this->submitted()) {
			if($this->process()) {
				$this->redirect($this->url());
			}
		}
		return $this->render();
	}
	
	/**
	 * Hook called when any form is ready
	 *
	 * @param InputfieldForm
	 * @return InputfieldForm
	 *
	 */
	public function ___ready(InputfieldForm $form) {
		$form->removeAttr('id');
		if(strtolower($form->attr('method')) == 'post') {
			$value = $this->sanitizer->entities($form->attr('name'));
			$form->appendMarkup .= "<input type='hidden' name='pwlrp' value='$value' />";
		}
		$this->readyDependencies($form, $this->name()); 
		return $form;
	}

	/**
	 * Prefix Inputfield dependencies so they continue to work in their form namespace
	 * 
	 * @param InputfieldForm $form
	 * @param string $formName Form name to use for prefix
	 * 
	 */
	protected function readyDependencies(InputfieldForm $form, $formName) {
	
		$prefix = $formName . '_';
		$fieldNames = array();
		$dependencyInputfields = array();
		$dependencyNames = array('showIf', 'requiredIf');

		// identify any dependencies
		foreach($form->getAll() as $inputfield) {
			$name = $inputfield->attr('name');
			if(strpos($name, $prefix) === 0) list(, $name) = explode($prefix, $name);
			$fieldNames[$name] = $name;
			foreach($dependencyNames as $property) {
				$selector = $inputfield->getSetting($property);
				if(empty($selector) || strpos($selector, $prefix) === 0) continue;
				$dependencyInputfields[$name] = $inputfield;
				break;
			}
		}

		// update dependency fields to proper namespace for form, i.e. "title" => "profile_title"
		foreach($dependencyInputfields as $name => $inputfield) {
			foreach($dependencyNames as $typeIf) {
				$selectorStr = $inputfield->getSetting($typeIf);
				if(empty($selectorStr)) continue;
				$selectors = new Selectors($selectorStr);
				foreach($selectors as $selector) {
					$selectorFields = $selector->field();
					if(strpos($selectorFields, $prefix) !== 0) {
						$selectorFields = $prefix . $selectorFields;
						if(strpos($selectorFields, '|')) {
							$selectorFields = str_replace('|', "|$prefix", $selectorFields);
						}
						$selector->field = $selectorFields;
					}
				}
				$inputfield->set($typeIf, (string) $selectors);
			}
		}

		// if dependencies are present, add inputfields.js to the scripts
		if(count($dependencyInputfields)) {
			$config = $this->wire()->config;
			$config->scripts->add($config->urls('wire') . 'templates-admin/scripts/inputfields.js');
		}
	}

	/**
	 * Get the submit button name
	 * 
	 * @return string
	 * 
	 */
	public function submitName() {
		return $this->name . '_submit';
	}

	/**
	 * Has the form been submitted? 
	 * 
	 * @return bool
	 * 
	 */
	public function submitted() {
		$submitted = $this->input->post($this->submitName());
		if($submitted && $this->useHoneypot && !$this->checkHoneypot()) $submitted = false;
		return $submitted;
	}

	/**
	 * Check submitted honeypot value
	 * 
	 * @return bool
	 * @throws WireException
	 * 
	 */
	protected function checkHoneypot() {
		$input = $this->input; 
		$jsVal = (string) $input->post('lrpf'); // lrpf is honeypot field name
		if(!strpos($jsVal, ':')) return false;
		// split: "login_name:ryan@processwire.com" => "login_name" and "ryan@processwire.com"
		list($name, $testVal) = explode(':', $jsVal, 2);
		$name = $this->sanitizer->fieldName($name);
		if(empty($name) || empty($testVal)) return false;
		$sourceVal = $input->post($name);
		if($sourceVal !== $testVal) return false; // invalid
		return true;
	}
	
}