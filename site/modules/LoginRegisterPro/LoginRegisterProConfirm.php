<?php namespace ProcessWire;

/**
 * ProcessWire Login Register Pro: Confirmation form
 * 
 * DO NOT DISTRIBUTE, this is a commercial module.
 * 
 * Copyright 2019 by Ryan Cramer Design, LLC
 * 
 * @method InputfieldForm build() Build the form
 * @method InputfieldForm|bool process(array $data = array()) Proces the form
 * @method string render(array $data = array()) Render the form markup
 * @method string execute() Render or process the form and return markup or redirect to self
 * @method InputfieldForm ready(InputfieldForm $form) Called when form is ready to be rendered or processed
 * @method confirmed(User $user) Method called after user has been confirmed and is ready to login
 *
 */
class LoginRegisterProConfirm extends LoginRegisterProForm {
	
	/**
	 * Ask them to login after confirmation? (false=auto-login)
	 *
	 * @var bool
	 *
	 */
	protected $confirmLogin = true;

	/**
	 * Ask for password on confirmation screen?
	 * 
	 * @var bool
	 * 
	 */
	protected $confirmPass = true;
	
	/**
	 * Password entered if we asked them to confirm it
	 *
	 * @var string
	 *
	 */
	protected $confirmedPass = '';

	/**
	 * New user that was created
	 * 
	 * @var User|null
	 * 
	 */
	protected $createdUser = null;

	/**
	 * Construct
	 *
	 * @param LoginRegisterPro $loginRegister
	 * @param string $name
	 * 
	 */
	public function __construct(LoginRegisterPro $loginRegister, $name = '') {
		parent::__construct($loginRegister, $name);
		$confirms = $loginRegister->confirms;
		$this->confirmPass = in_array('pass', $confirms);
		$this->confirmLogin = in_array('login', $confirms);
	}

	/**
	 * Get created user after a form process
	 * 
	 * @return null|User
	 * 
	 */
	public function getCreatedUser() {
		return $this->createdUser;
	}
	
	/**
	 * Execute
	 * 
	 * @return string
	 * 
	 */
	public function ___execute() {

		$out = '';
		$this->form(); // forces build now
		
		if($this->submitted()) {
			try {
				/** @var User|bool $user */
				$this->process();
				$user = $this->getCreatedUser();
				if($user && $user->id) {
					$this->confirmed($user);
				} else {
					// i.e. honeypot or other non-Exception failure
				}
				if(1 > 2) throw new LoginRegisterException(); // ignore
			} catch(LoginRegisterException $e) {
				$this->error($e->getMessage());
				$this->logError($e->getMessage());

			} catch(\Exception $e) {
				$this->error($this->_('Unable to complete confirmation, please re-register and try again'));
				$this->logError($e->getMessage());
			}

		} else {
			$out = $this->render();
		}

		return $out;
	}

	/**
	 * Build the form where user can paste and submit a confirmation code
	 *
	 * This form is displayed after a successful registration form submission, but
	 * is only submitteed if user opts not to click the link in their email.
	 *
	 * @return InputfieldForm
	 *
	 */
	public function ___build() {
		if($this->form) return $this->form;
		
		/** @var Modules $modules */
		$modules = $this->wire('modules');

		/** @var WireInput $input */
		$input = $this->wire('input');

		/** @var InputfieldForm $form */
		$form = parent::___build();
		$form->attr('action', $this->loginRegister->actionUrl('register_confirm')); 
	
		/** @var InputfieldText $codeInput */
		$codeInput = $modules->get('InputfieldText');
		$codeInput->attr('name', 'register_confirm');
		$codeInput->label = $this->_('Confirmation code');
		$codeInput->required = true;
		$codeInput->attr('required', 'required');
		$form->add($codeInput);
		
		/** @var InputfieldEmail $emailInput */
		$emailInput = $modules->get('InputfieldEmail');
		$emailInput->label = $this->_('Email address');
		$emailInput->required = true;
		$emailInput->attr('required', 'required');
		$emailInput->attr('name', 'register_email');
		$form->add($emailInput);

		if($this->confirmPass) {
			/** @var InputfieldText $passInput */
			$passInput = $modules->get('InputfieldText');
			$passInput->attr('name', 'register_pass');
			$passInput->attr('type', 'password');
			$passInput->label = $this->_('Password');
			$passInput->required = true;
			$passInput->attr('required', 'required');
			if($input->get('badpass')) $passInput->error($this->_('Incorrect password, please try again'));
			$form->add($passInput);
		} else {
			$passInput = null;
		}

		/** @var InputfieldSubmit $f */
		$f = $modules->get('InputfieldSubmit');
		$f->attr('name', 'confirm_submit');
		$form->add($f);

		if($input->post('confirm_submit')) {
			// confirmation form submitted

		} else if($input->get('register_confirm')) {
			
			$code = $this->sanitizer->text($input->get('register_confirm'));
			if(strlen($code) && $code !== "1") $codeInput->attr('value', $code);

			$email = $input->get('register_email');
			if(!strpos($email, '@') && $this->loginRegister()->allow('hidemail', 'registers') && strlen($email) > 4) {
				$email = $this->loginRegister()->tools()->decodeEmail($email);
			}
			$email = $this->sanitizer->email($email);
			if(strlen($email)) $emailInput->attr('value', $email);
			
			if($code === "1") {
				$this->message(
					$this->_('A confirmation code has been emailed to you.') . ' ' .
					$this->_('When you receive the email, click the link it contains, or paste the confirmation code below.')
				);
			} else if(strlen($code)) {
				$this->message(
					$this->_('Please fill in the fields below and click the submit button to complete your registration.')
				);
			}
		}

		return $this->ready($form);
	}

	/**
	 * Process the confirmation, create new user and log them in
	 * 
	 * Populates the $this->createdUser property. 
	 *
	 * This can be triggered by a URL clicked in an email, or by submission of the confirmation form.
	 * 
	 * Note: This will perform a redirect back to itself on password failure. 
	 *
	 * @return InputfieldForm
	 * @throws LoginRegisterException|WireException Throws this exception for most error conditions
	 *
	 */
	public function ___process() {

		/** @var Session $session */
		$session = $this->wire('session');
		
		/** @var Users $users */
		$users = $this->wire('users');
	
		$form = $this->form();
		$pass = false;
		
		parent::___process();
		
		// get submitted values
		$code = $form->getChildByName('register_confirm')->val();
		if(!strlen($code)) return $form;
		
		$email = $form->getChildByName('register_email')->val();
		if(!strlen($email)) return $form;
		
		if($this->confirmPass) {
			$pass = $form->getChildByName('register_pass')->val();
			if(!strlen($pass)) return $form;
		}

		// validate that the code in the DB is the same one present in the URL
		$values = $this->loginRegister->codes()->validate($code, $email, LoginRegisterProCodes::typeRegister, $pass);

		if($values === LoginRegisterProCodes::validateBadpass) { // 0
			// all valid except password failed, let them try again
			if($this->loginRegister()->allow('hidemail', 'registers')) {
				$registerEmail = $this->loginRegister()->tools()->encodeEmail($email);
			} else {
				$registerEmail = $email;
			}
			$this->loginRegister->redirect(array(
				'register_confirm' => $code, 
				'register_email' => $registerEmail,
				'badpass' => 1
			)); 
			return $form;

		} else if($values === LoginRegisterProCodes::validateOverflow) { // null
			throw new LoginRegisterException($this->_('Registration failed because max password attempts reached, please re-register and try again.'));

		} else if($values === LoginRegisterProCodes::validateInvalid || !$values) { // false
			throw new LoginRegisterException($this->_('Invalid confirmation code or email'));
		}

		// check if email is now in use
		if(!$this->loginRegister->allowUserEmail($values['register_email'])) {
			throw new LoginRegisterException($this->_('Email address already taken'));
		}

		$error = '';
		if(!$this->loginRegister->allowUserName($values['register_name'], $error)) {
			throw new LoginRegisterException($this->_('Unable to complete registration') . ' - ' . $error);
		}

		$user = $users->newUser();

		// populate values to new user
		foreach($values as $key => $value) {
			if(strpos($key, 'register_') !== 0) continue;
			$key = str_replace('register_', '', $key);
			$_key = strtolower($key);
			if($_key === 'roles' || $_key === 'pass') continue;
			if($key != 'name' && !$user->template->fieldgroup->hasField($key)) continue;
			$user->set($key, $value);
		}

		// setup password
		if($this->confirmPass) $this->confirmedPass = $pass;
		$password = new Password();
		$this->wire($password);
		$password->hash = $values['register_pass'][0];
		$password->salt = $values['register_pass'][1];
		$user->set('pass', $password);

		// add role to user if defined
		foreach($this->loginRegister->registerRoles as $roleName) {
			$role = $this->loginRegister->getRole($roleName);
			if($role->id && !$role->hasPermission('page-edit')) {
				$user->addRole($role);
			}
		}

		// create the user
		try {
			$this->loginRegister->createUserReady($user, $values);
			$user->save();
			$this->loginRegister->createdUser($user); // trigger created user hook
			$this->createdUser = $user;
			$session->removeFor($this, true); // remove session data

		} catch(\Exception $e) {
			throw new WireException('Unable to create user');
		}
	
		$this->confirmed($user);
		
		return $form;
	}

	/**
	 * Method/hook called when new user has been created
	 * 
	 * Next step is auto-login or redirect to login form
	 * 
	 * @param User $user
	 * 
	 */
	protected function ___confirmed(User $user) {
	
		if($this->allow('email', 'login')) {
			$loginName = $user->email;
			$loginNameForUrl = $loginName;
			if($this->loginRegister->allow('hidemail', 'registers')) {
				$loginNameForUrl = $this->loginRegister->tools()->encodeEmail($loginName);
			} 
		} else {
			$loginName = $user->name;
			$loginNameForUrl = $loginName;
		}
		
		$this->logSuccess('Confirmed and created new user', $user);
	
		if($this->loginRegister->notifyEmail) {
			$subject = $this->_('New user confirmed'); 
			$body = sprintf($this->_('A new user account was confirmed and created for: %s'), $user->email); 
			$this->loginRegister->emailAdmin($subject, $body);
		}

		if($this->confirmLogin) {
			// redirect to main page, tell them it was a success and ask them to login
			$this->redirect($this->loginRegister->actionUrl('register_confirmed', array('login_name' => $loginNameForUrl)));
			return;
		}
		
		// automatically login user now
		if($this->confirmPass) {
			$authUser = $this->session->login($user, $this->confirmedPass);
		} else {
			$authUser = $this->session->forceLogin($user);
		}

		if($authUser && $authUser->id && $authUser->id === $user->id) {
			$this->loginRegister->login->success($authUser);
			$this->session->removeFor($this->loginRegister, true); // remove session data
			$redirectUrl = $this->loginRegister->redirectUrl;
			if(!$redirectUrl) $redirectUrl = $this->loginRegister->actionUrl('register_confirmed', array('login' => 1));
			$this->redirect($redirectUrl);

		} else {
			$this->loginRegister->login->fail($user->name);
			$this->logError("Unable to auto-login created user", $user); 
			$this->redirect($this->loginRegister->actionUrl('register_confirmed', array('login_name' => $loginNameForUrl))); 
		}
	}
}