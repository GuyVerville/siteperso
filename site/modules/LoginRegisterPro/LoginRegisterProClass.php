<?php namespace ProcessWire;

/**
 * ProcessWire Login Register Pro: abstract helper base class
 *
 * DO NOT DISTRIBUTE, this is a commercial module.
 *
 * Copyright 2020 by Ryan Cramer Design, LLC
 * 
 * @property LoginRegisterPro $loginRegister
 * @property string $name
 *
 */
abstract class LoginRegisterProClass extends Wire {
	
	/**
	 * @var LoginRegisterPro
	 *
	 */
	protected $loginRegister;

	/**
	 * @var string
	 * 
	 */
	protected $name = '';

	/**
	 * Construct
	 *
	 * @param LoginRegisterPro $loginRegister
	 * @param string $name
	 *
	 */
	public function __construct(LoginRegisterPro $loginRegister, $name = '') {
		$this->loginRegister = $loginRegister;
		if(empty($name)) $name = strtolower(str_replace('LoginRegisterPro', '', $this->className()));
		$this->name = $name;
		parent::__construct();
	}

	/**
	 * Get name
	 * 
	 * @return string
	 * 
	 */
	public function name() {
		return $this->name;
	}

	/**
	 * Get a LoginRegisterPro url
	 *
	 * @param string|array $queryString
	 * @return string
	 *
	 */
	public function url($queryString = '') {
		return $this->loginRegister->url($queryString);
	}

	/**
	 * Redirect to URL
	 *
	 * @param string|array $url
	 * @param bool $permanent Permanent (301) redirect? (default=false)
	 *
	 */
	public function redirect($url, $permanent = false) {
		return $this->loginRegister->redirect($url, $permanent);
	}

	/**
	 * Log an error
	 *
	 * @param string $msg
	 * @param User|string $user
	 *
	 */
	public function logError($msg, $user = '') {
		$this->loginRegister->logError($msg, $user);
	}

	/**
	 * Log a success
	 *
	 * @param string $msg
	 * @param User|string $user
	 *
	 */
	public function logSuccess($msg, $user = '') {
		$this->loginRegister->logSuccess($msg, $user);
	}

	/**
	 * Error notification
	 * 
	 * @param string $text
	 * @param int $flags
	 * @return Wire 
	 * 
	 */
	public function error($text, $flags = 0) {
		$this->loginRegister->error($text, $flags);
		return $this;
	}

	/**
	 * Message notification
	 *
	 * @param string $text
	 * @param int $flags
	 * @return Wire
	 *
	 */
	public function message($text, $flags = 0) {
		$this->loginRegister->message($text, $flags);
		return $this;
	}

	/**
	 * Warning notification
	 *
	 * @param string $text
	 * @param int $flags
	 * @return Wire
	 *
	 */
	public function warning($text, $flags = 0) {
		$this->loginRegister->warning($text, $flags);
		return $this;
	}

	/**
	 * Is the requested option allowed by the module config? 
	 *
	 * @param string $name
	 * @param string $src Source of toggles/options with default being those for this class (features, login, register, confirm, profile)
	 * @return bool
	 *
	 */
	public function allow($name, $src = '') {
		if(empty($src)) $src = $this->name();
		return $this->loginRegister->allow($name, $src);
	}

	/**
	 * Set session value in LoginRegisterPro namespace
	 *
	 * @param string $key
	 * @param mixed $value
	 *
	 */
	public function sessionSet($key, $value) {
		$this->loginRegister->sessionSet($key, $value);
	}

	/**
	 * Get session value in LoginRegisterPro namespace
	 *
	 * @param string $key
	 * @param mixed $fallbackValue
	 * @return mixed
	 *
	 */
	public function sessionGet($key, $fallbackValue = null) {
		return $this->loginRegister->sessionGet($key, $fallbackValue);
	}

	/**
	 * @return LoginRegisterPro
	 * 
	 */
	public function loginRegister() {
		return $this->loginRegister;
	}
	
	public function __get($key) {
		if($key === 'loginRegister') return $this->loginRegister; 
		if($key === 'name') return $this->name;
		return parent::__get($key);
	}
}