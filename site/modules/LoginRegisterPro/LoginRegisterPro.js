/**
 * Login Register Pro JS
 * 
 * Copyright 2020 by Ryan Cramer Design, LLC
 * 
 */

var LoginRegisterPro = {

	isInit: false,
	isReady: false,
	options: {
		debug: false,
		faUrl: ''
	},

	// js for profile form	
	profile: function() {
		var $ = jQuery;
		$('.Inputfield').on('change', function() {
			var $f = $(this);
			if($f.hasClass('ProfilePassAsk')) {
				// reveal hidden pass field
				$('#wrap_ProfilePassAsk').removeClass('InputfieldStateHidden');
				$('#ProfilePassAsk').attr('required', 'required');
			}
		});
		$('#ProfileDelete').on('click', function() {
			var $checkbox = $(this);
			setTimeout(function() {
				var $f = $('#wrap_ProfileDeleteEmail');
				if($checkbox.is(':checked')) {
					$f.removeClass('InputfieldStateHidden');
				} else {
					$f.addClass('InputfieldStateHidden');
				}
			}, 100);
		});
	},

	// js for login form	
	login: function() {
		this.non('login_name');
	},

	// js for register form
	register: function() {
		this.non('register_email');
	},

	// js for hp
	non: function(nam) {
		var $a = jQuery('input[name=' + nam +']');	
		var $b = jQuery('<input />').attr('type', 'hidden').attr('name', 'lrpf');
		var $f = $a.closest('form');
		if($a.length) $f.append($b).on('submit', function() { $b.val(nam + ':' + $a.val()); });
	}, 

	// debug log	
	log: function(msg) {
		if(this.options.debug) console.log(msg);
	},

	// called after this LoginRegisterPro.js is available
	construct: function(options) {
		jQuery.extend(this.options, options);
	},

	// called after div.LoginRegisterPro is ready and in markup
	init: function() {
		if(this.isInit) return;
		var $icon = jQuery('div.LoginRegisterPro').find('i.fa:eq(0)');
		if($icon.length && $icon.css('font-family') !== 'FontAwesome' && this.options.faUrl.length) {
			this.log('Loading FA');
			var $link = jQuery("<link rel='stylesheet' type='text/css' />").attr('href', this.options.faUrl);
			jQuery('head').append($link);
		}
		this.isInit = true;
	}, 
	
	// called when document markup rendered and ready
	ready: function() {
		if(this.isReady) return;
		if(jQuery('.ProfileForm').length) LoginRegisterPro.profile();
		if(jQuery('.RegisterForm').length) LoginRegisterPro.register();
		if(jQuery('.LoginForm').length) LoginRegisterPro.login();
		this.isReady = true;
	},

	// called as first document.ready event before any of ProcessWire’s 
	preflight: function() {
		this.jQueryCheck();
	},

	// check if jQuery is being loaded more than once	
	jQueryCheck: function() {
		if(ProcessWire.$ === jQuery) return; // good
		var v1 = jQuery.fn.jquery;
		var v2 = ProcessWire.$.fn.jquery;
		var howTo = 
			'To remove this warning, load your jQuery version before LoginRegisterPro output on this page ' + 
			'(like in <code>&lt;head&gt;</code>), or stop loading your jQuery version on this page.';
		if(v1 == v2) {
			// same version, warn only
			console.log('jQuery is being loaded twice, though the same version (' + v1 + '). ' + howTo); 
		} else {
			// different versions, make visible error
			var err = 'You have 2 versions of jQuery loading (' + v1 + ' and ' + v2 + '). ' + howTo; 
			jQuery('.LoginRegisterPro').prepend("<p class='LoginRegisterError'>" + err + "</p>");
		}
		// replace with LoginRegisterPro’s version of jQuery that was loaded first
		jQuery = ProcessWire.$;
		$ = ProcessWire.$;
	}
};

if(typeof jQuery != "undefined") {
	jQuery(document).ready(function($) {
		LoginRegisterPro.ready();
	});
}