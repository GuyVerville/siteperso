<?php

/**
 * ProcessWire Pro Drafts: Repeaters
 *
 * Copyright (C) 2017 by Ryan Cramer
 *
 * This is commercially licensed and supported software
 * PLEASE DO NOT DISTRIBUTE
 *
 * https://processwire.com/ProDrafts/
 *
 */

class ProDraftsRepeaters extends Wire {
	
	/**
	 * @var ProDrafts
	 *
	 */
	protected $drafts;

	/**
	 * True when an ajax file upload to a repeater is present and draftable 
	 * 
	 * @var bool
	 * 
	 */
	protected $isAjaxFileUpload = false;

	/**
	 * Construct
	 *
	 * @param ProDrafts $drafts
	 *
	 */
	public function __construct(ProDrafts $drafts) {
		$this->drafts = $drafts;
	}

	/**
	 * Initialize hooks
	 *
	 */
	public function init() {
		$this->addHookAfter('FieldtypeRepeater::wakeupValue', $this, 'hookFieldtypeRepeaterWakeupValue');
		$this->addHookAfter('FieldtypeRepeater::formatValue', $this, 'hookFieldtypeRepeaterFormatValue');
		$this->addHookAfter('FieldtypeRepeater::readyPageSaved', $this, 'hookFieldtypeRepeaterReadyPageSaved');
		$this->setupAjaxFileUpload();
	}

	/**
	 * API ready
	 *
	 */
	public function ready() {
	}

	/**
	 * Make sure that the owning page (forPage) is also a draft when file uploaded to repeater item
	 * 
	 */
	protected function setupAjaxFileUpload() {
		if(!$this->wire('config')->ajax) return; 
		if(!$this->wire('input')->get('InputfieldFileAjax')) return;
		$pageID = (int) $this->wire('input')->get('id');
		if(!$pageID) return;
		/** @var RepeaterPage|ProDraftPage $page */
		$page = $this->wire('pages')->get($pageID);
		if($page->isUnpublished()) return;
		if(!$this->drafts->isRepeater($page)) return;
		if(!$this->drafts->allowDraft($page)) return;
		/** @var ProDraftPage $forPage */
		$forPage = $page->getForPage();
		/** @var Field $forField */
		$forField = $page->getForField();
		if(!$this->drafts->allowDraft($forPage)) return;
		$draft = $forPage->isDraft();
		if(!$draft) $draft = $forPage->draft(true); 
		if($draft) $forPage->trackChange($forField->name); 	
		$this->isAjaxFileUpload = true; 
	}
	
	/**
	 * Hook to FieldtypeRepeater::readyPageSaved($readyPage, $ownerPage, $field)
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookFieldtypeRepeaterReadyPageSaved(HookEvent $e) {
		// add status PublishRequested to newly added repeater items
		$readyPage = $e->arguments(0);
		$ownerPage = $e->arguments(1);
		if($this->drafts->isPageUnpublished($ownerPage)) return;
		if(!$this->drafts->allowDraft($ownerPage)) return;
		$readyPage->addStatus(ProDrafts::pageStatusPublishRequested);
		$this->drafts->savePageStatus($readyPage);
	}


	/**
	 * Hook after FieldtypeRepeater::formatValue() 
	 * 
	 * Remove repeater items pending deletion from formatted value when on front-end.  
	 * 
	 * @param HookEvent $e
	 * 
	 */
	public function hookFieldtypeRepeaterFormatValue(HookEvent $e) {
		
		/** @var ProDraftPage $page */
		$page = $e->arguments(0);

		if($this->drafts->isPageUnpublished($page)) return;

		/** @var RepeaterPageArray $value */
		$value = $e->return;

		/** @var ProDraft $draft */
		$draft = $page->isDraft();
		
		if($draft) {
			// true when a draft is being viewed on the front-end
			$frontEnd = $this->wire('page')->template != 'admin' && $this->wire('input')->get('draft');
			if($frontEnd && $value instanceof PageArray) {
				foreach($value as $item) {
					if($item->get('_repeater_delete')) $value->removeQuietly($item);
				}
			}

		}
		/*
		else if($page->hasStatus(Page::statusDraft)) {
			foreach($value as $item) {
				if(!$item->hasStatus(ProDrafts::pageStatusPublishRequested)) continue;
				// remove newly added items that are only visible in draft mode
				$value->removeQuietly($item);
			}
		}
		*/
	}

	/**
	 * Hook to FieldtypeRepeater::wakeupValue
	 *
	 * When loading a repeater from a Page that is a draft, this makes all the repeater items drafts as well.
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookFieldtypeRepeaterWakeupValue(HookEvent $e) {
		
		static $cnt = 0;
		$cnt++;
		
		
		/** @var ProDraftPage $page */
		$page = $e->arguments(0);
		//$field = $e->arguments(1);

		if($this->drafts->isPageUnpublished($page)) return;
		
		/** @var ProDraft $draft */
		$draft = $page->isDraft();

		/** @var RepeaterPageArray|RepeaterPage $value */
		$value = $e->return;
		
		$processPage = $this->wire('page');
		$processName = $processPage ? (string) $processPage->process : '';
		if(!$processName && $this->isAjaxFileUpload) $processName = 'ProcessPageEdit';
		$inPageEditor = $processName == 'ProcessPageEdit';
	
		if(!$draft) {
			// page is not a draft (though it might still have one)
			if(!$value instanceof PageArray) return;
			$pageHasDraft = $page->hasStatus(ProDrafts::pageStatusDraft);

			foreach($value as $item) {
				/*
				if($item->hasStatus(ProDrafts::pageStatusPublishRequested) && !$inPageEditor) {
					// remove newly added items that are only visible in draft mode
					$value->removeQuietly($item);
				}
				if($pageHasDraft) {
					// remove newly added items that are only visible in draft mode
					// $value->removeQuietly($item);
				} else {
					// remove publishRequested status which is not applicable when no draft exists
					// $item->removeStatus(ProDrafts::pageStatusPublishRequested);
					// $this->drafts->savePageStatus($item);
				}
				*/
				
				$hasPublishRequested = $item->hasStatus(ProDrafts::pageStatusPublishRequested);
			
				if($pageHasDraft && $hasPublishRequested && !$inPageEditor) {
					// remove newly added items that are only visible in draft mode
					$value->removeQuietly($item);
					
				} else if($hasPublishRequested && !$pageHasDraft) {
					// remove publishRequested status which is not applicable when no draft exists
					$item->removeStatus(ProDrafts::pageStatusPublishRequested);
					$this->drafts->savePageStatus($item);
				}
			}
			
			return;
		}

		/** @var RepeaterPageArray $newValue */
		if($value instanceof PageArray) {
			$newValue = $value->makeNew();
		} else if($value instanceof Page) {
			// i.e. FieldsetPage
			$newValue = new PageArray();
			$value = clone $value;
			$value = array($value); 
		}

		// toggle delete for any repeater items that have delete pending
		$deleteIDs = $draft->getMeta('deleteRepeaterIDs');
		$publishIDs = $draft->getMeta('publishRepeaterIDs'); 
		if(!is_array($deleteIDs)) $deleteIDs = array();
		if(!is_array($publishIDs)) $publishIDs = array();

		foreach($value as $repeaterPage) {

			$deletePending = false;
			$repeaterID = $repeaterPage->id;

			if(in_array($repeaterID, $deleteIDs)) {
				//if(!$inPageEditor) continue;
				$deletePending = true;
			}

			$repeaterDraft = $repeaterPage->draft(true);
			$repeaterDraft->addFlag(ProDraft::flagRepeater);
			$repeaterDraftPage = $repeaterDraft->draftPage();
			
			if($deletePending) {
				$repeaterDraftPage->set('_repeater_delete', 1);
			}
			
			if(isset($publishIDs["_$repeaterID"])) {
				$state = $publishIDs["_$repeaterID"];
				if($state == -1) {
					// unpublish
					if(!$repeaterDraftPage->isUnpublished()) {
						$repeaterDraftPage->addStatus(Page::statusUnpublished);
						$repeaterDraftPage->removeStatus(Page::statusOn);
						$repeaterDraftPage->setQuietly('_repeater_unpublish', 1); // not used at present
					}
				} else if($state == 1) {
					// publish
					if($repeaterPage->isUnpublished()) {
						$repeaterDraftPage->removeStatus(Page::statusUnpublished);
						$repeaterDraftPage->addStatus(Page::statusOn);
						$repeaterDraftPage->setQuietly('_repeater_publish', 1); // not used at present
					}
				}
			}
			
		
			$newValue->add($repeaterDraftPage);
		}

		//$newValue->sort('sort');
		
		if(is_array($value)) $newValue = $newValue->first(); // i.e. FieldsetPage
		$this->drafts->debugMessage("Setup repeater value for page $page: $newValue"); 

		$e->return = $newValue;
	}

	/**
	 * Publish repeater field on $page
	 * 
	 * @param Page $page
	 * @param Field $field
	 * 
	 */
	public function publishRepeater(Page $page, Field $field) {
		// indicate that the repeater items should be published too
		$value = $page->getUnformatted($field->name);
		if($value instanceof Page) $value = array($value); // i.e. FieldsetPage
		foreach($value as $item) {	
			/** @var ProDraftPage $item */
			$repeaterDraft = $item->isDraft();
			if($repeaterDraft) {
				$item->removeStatus(ProDrafts::pageStatusPublishRequested); // newly added items will have this status
				$this->drafts->publishDraft($repeaterDraft);
			}
		}
	}

	/**
	 * Process a page editor POST request to detected deleted and published repeater items
	 * 
	 * @param Page $page
	 * 
	 */
	public function processPageEditPostRequest(Page $page) {
		
		// prevent repeater items from being deleted right now, but remember the IDs for storage in draft
		
		/** @var WireInput $input */
		$input = $this->wire('input');
		
		$deleteIDs = array();
		$publishIDs = array();
		
		foreach($_POST as $key => $value) {
			
			/*
			if(strpos($key, 'loaded_repeater') === 0) {
				$id = (int) str_replace('loaded_repeater', '', $key);
				$item = $this->wire('pages')->get($id);
				if($item->id && $this->drafts->isRepeater($item) && !$item->isDraft()) {
					$this->drafts->makeDraftPage($item);
				}
			}
			*/

			$value = (int) $value;
			if(!$value) continue;
			
			if(strpos($key, 'delete_repeater') === 0) {
				unset($_POST[$key]);
				$input->post->$key = null;
				$deleteIDs[] = (int) $value;
				
			} else if(strpos($key, 'publish_repeater') === 0) {
				$_POST[$key] = null;
				$input->post->$key = null;
				$id = (int) str_replace('publish_repeater', '', $key);
				$publishIDs["_$id"] = $value === -1 ? -1 : 1;
			}
		}
		
		$draft = $page->draft();
		$metaDeleteIDs = $draft->getMeta('deleteRepeaterIDs');
		$metaPublishIDs = $draft->getMeta('publishRepeaterIDs');
		$numUndeleted = 0;
		
		if(!is_array($metaDeleteIDs)) $metaDeleteIDs = array();
		if(!is_array($metaPublishIDs)) $metaPublishIDs = array();
		
		if(count($metaDeleteIDs)) {
			foreach($metaDeleteIDs as $key => $deleteID) {
				if(isset($_POST["sort_repeater$deleteID"]) && !in_array($deleteID, $deleteIDs)) {
					unset($metaDeleteIDs[$key]); // undelete
					$numUndeleted++;
				}
			}
		}
		
		if(count($deleteIDs) || $numUndeleted) {
			$deleteIDs = array_unique(array_merge($metaDeleteIDs, $deleteIDs));
			$draft->setMeta('deleteRepeaterIDs', $deleteIDs);
		}
		
		$publishIDs = array_merge($metaPublishIDs, $publishIDs);
		$draft->setMeta('publishRepeaterIDs', $publishIDs);
	}
	
}