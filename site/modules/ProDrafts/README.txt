ProcessWire ProDrafts
=====================

This is a commercially licensed and supported module. Do not distribute. 
Copyright (C) 2018 by Ryan Cramer


ABOUT PRODRAFTS
===============

ProDrafts enables you to maintain separate draft and live versions of
any page. You can make edits to a draft version of the page without 
affecting the live version until you are ready to publish your draft.
ProDrafts provides a comparison tool so that you can compare draft
values to live values and view a "diff" for text changes. Furthermore,
you can preview both the draft and live versions on the front-end of
your site. 

ProDrafts provides automatic save capability, ensuring that your work 
is never lost, even if you step away from the computer or otherwise
forget to save. Edits are automatically saved to a draft of your page.

ProDrafts provides live preview capability, enabling you to edit a 
page while seeing it update on the front-end in real time, side-by-side.


HOW TO INSTALL
==============

The current ProcessWire 3.x master or dev branch is recommended. 

1. Unzip the ProDrafts[version].zip file and place the files in:
   /site/modules/ProDrafts/
   
2. In the ProcessWire admin, go to Modules > Refresh. Then go to 
   Modules > Site and click "Install" next to ProDrafts. 
   
3. Next you will see the ProDrafts configuration screen. Configure
   ProDrafts and you are ready to go! 
   

NOTES FOR VERSION 6+
====================

Repeater support:

Repeater support is enabled automatically if the core module FieldtypeRepeater 
is installed. Nested repeaters are also supported. However, note that the 
Automatic Save and Live Preview features do not work with repeaters. If you 
decide to keep these features enabled in ProDrafts, that's fine, but just note
that they won't do anything when it comes to Repeaters and the fields within
them. Repeater support in ProDrafts includes support for these Fieldtypes: 

 • FieldtypeRepeater
 • FieldtypeFieldsetPage
 • FieldtypeRepeaterMatrix

Basic workflow support: 

Workflow support enables users without page-publish permission to submit their 
drafts for approval. Likewise it enables users with page-publish permission to
approve or reject drafts that have been submitted for approval. 

To enable workflow support, the "page-publish" permission must be installed. To
install this permission, go to Access > Permissions > Add New, and check the box
for the page-publish permission. Once installed, visit the ProDrafts module 
configuration page in the ProcessWire admin and click "Save". This will enable
basic workflow support. 

When drafts are submitted or rejected, the user can optionally add notes for the 
person that receives the request or rejection. These notes appear in a 
notification when the page is edited, and also appear in email notifications
that ProDrafts sends. 

Notifications can be sent automatically by email if the user has opted-in to 
receive them. This is done with checkbox fields when a user edits their profile. 
Three separate email templates are provided for these actions:

 • Request publish of draft: sent to user(s) with publish permission to page. 
 • Approval of draft: sent to the user that requested publish. 
 • Rejection of draft: sent to the user that requested publish. 
 
If you'd like to customize the emails that ProDrafts sends, see the instructions
located in these files:

 • email-approve-publish.php
 • email-reject-publish.php
 • email-request-publish.php

When workflow support is active, ProDrafts adds two checkbox fields to your 
system. These are opt-in fields added to the "user" template that enables users
to check whether they want to receive ProDrafts notifications. 

 • prodrafts_notify_pending: Receive pending publish request notifications.
 • prodrafts_notify_approve: Receive approve/reject draft notifications.

Drafts awaiting approval/publish can also be reviewed from the Drafts page
in the admin (Pages > Drafts). When there are pending drafts awaiting approval
there is an "Awaiting Approval" tab that appears. It contains a list of all
drafts awaiting approval. 



DOCUMENTATION
=============
Please see our full online documentation at:
https://processwire.com/api/modules/prodrafts/
 

PRODRAFTS VIP SUPPORT
=====================

Your ProDrafts service includes 1-year of VIP support through the ProcessWire
ProDrafts forum at https://processwire.com/talk/. Support may optionally be 
renewed every year. 

Please send a private message (PM) to 'ryan' or contact us at:
https://processwire.com/contact/ 

VIP support is also available by email: ryan@processwire.com


TERMS AND CONDITIONS
====================

You may not copy or distribute ProDrafts, except on site(s) you have registered it
for with Ryan Cramer Design, LLC. It is okay to make copies for use on staging
or development servers specific to the site you registered for.

This service/software includes 1-year of support through the ProcessWire ProDrafts
Support forum and/or email. 

In no event shall Ryan Cramer Design, LLC or ProcessWire be liable for any special,
indirect, consequential, exemplary, or incidental damages whatsoever, including,
without limitation, damage for loss of business profits, business interruption,
loss of business information, loss of goodwill, or other pecuniary loss whether
based in contract, tort, negligence, strict liability, or otherwise, arising out of
the use or inability to use ProcessWire ProDrafts, even if Ryan Cramer Design, LLC /
ProcessWire has been advised of the possibility of such damages.

ProDrafts is provided "as-is" without warranty of any kind, either expressed or
implied, including, but not limited to, the implied warranties of merchantability and
fitness for a particular purpose. The entire risk as to the quality and performance
of the program is with you. Should the program prove defective, you assume the cost
of all necessary servicing, repair or correction. If within 7 days of purchase, you
may request a full refund. Should you run into any trouble with ProDrafts, please
email for support or visit the ProDrafts Support forum at:
https://processwire.com/talk/.


Thank you for using ProDrafts!
