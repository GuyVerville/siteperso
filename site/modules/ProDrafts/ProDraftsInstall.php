<?php
/**
 * ProcessWire Pro Drafts: Installer
 *
 * Copyright (C) 2017 by Ryan Cramer
 *
 * This is commercially licensed and supported software
 * PLEASE DO NOT DISTRIBUTE
 *
 * https://processwire.com/ProDrafts/
 *
 */ 

class ProDraftsInstall extends Wire {
	
	/**
	 * Install the module: setup DB table
	 *
	 */
	public function install() {
		// create pages_drafts table
		$tableName = ProDrafts::tableName;
		$this->wire('database')->exec("
			CREATE TABLE $tableName (
				pages_id INT UNSIGNED NOT NULL, 
				version INT UNSIGNED NOT NULL, 
				created DATETIME NOT NULL, 
				modified DATETIME NOT NULL,
				created_users_id INT UNSIGNED NOT NULL, 
				modified_users_id INT UNSIGNED NOT NULL,
				flags INT UNSIGNED NOT NULL DEFAULT 0, 
				name VARCHAR(44), 
				data LONGTEXT, 
				meta TEXT, 
				PRIMARY KEY pages_id_version (pages_id, version), 
				INDEX created (created),
				INDEX modified (modified), 
				INDEX created_users_id (created_users_id), 
				INDEX modified_users_id (modified_users_id),
				UNIQUE name (name)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8
		");
		
		//$this->installApprovals();
	}

	/**
	 * Install fields used by approval features
	 *
	 */
	public function installApprovals() {

		$fields = $this->wire('fields');
		$modules = $this->wire('modules');
		$templates = $this->wire('templates');
		$config = $this->wire('config');
		$userFieldgroups = array();
		$addFields = array();

		foreach($config->userTemplateIDs as $templateID) {
			$template = $templates->get($templateID);
			if(!$template) continue;
			$userFieldgroups[] = $template->fieldgroup;
		}

		$name = 'prodrafts_notify_pending';
		$f = $fields->get($name);
		if(!$f) {
			$f = new Field();
			$f->type = $modules->get('FieldtypeCheckbox');
			$f->name = $name;
			$f->flags = Field::flagSystem;
			$f->label = 'Notify me when new drafts are submitted for approval';
			$f->save();
		}
		$addFields[] = $f;

		$name = 'prodrafts_notify_approve';
		$f = $fields->get($name);
		if(!$f) {
			$f = new Field();
			$f->type = $modules->get('FieldtypeCheckbox');
			$f->name = $name;
			$f->flags = Field::flagSystem;
			$f->label = 'Notify me when my draft submissions are approved or rejected';
			$f->save();
		}
		$addFields[] = $f;

		foreach($userFieldgroups as $fieldgroup) {
			/** @var Fieldgroup $fieldgroup */
			foreach($addFields as $f) {
				if($fieldgroup->hasField($f)) continue;
				$fieldgroup->add($f);
				$fieldgroup->save();
				$this->message("Added field '$f->name' to fieldgroup '$fieldgroup->name'", Notice::debug);
			}
		}
	}

	/**
	 * Check that uninstall is allowed?
	 * 
	 * @param bool $throw Throw Exception if not allowed? (default=true)
	 * @return bool
	 * @throws WireException
	 * 
	 */
	public function checkAllowUninstall($throw = true) {
		$draftQty = $this->wire('pages')->count("include=all, status=" . ProDrafts::pageStatusDraft);
		if($draftQty && $throw) throw new WireException(
			"There are still $draftQty drafts. Please delete or publish them before you uninstall ProDrafts."
		);
		return $draftQty == 0;
	}
	
	/**
	 * Uninstall the module: remove DB table and any fields added by ProDrafts
	 *
	 */
	public function uninstall() {
		
		$this->checkAllowUninstall();
		
		// remove pages_drafts table
		$tableName = ProDrafts::tableName;
		$this->wire('database')->exec("DROP TABLE $tableName");

		// remove fields added by ProDrafts
		$saveFieldgroups = array();
		$fieldNames = array(
			'prodrafts_notify_pending',
			'prodrafts_notify_approve',
		);
		foreach($fieldNames as $name) {
			/** @var Field $f */
			$f = $this->wire('fields')->get($name);
			if(!$f) continue;
			$f->addFlag(Field::flagSystemOverride);
			$f->removeFlag(Field::flagSystem);
			foreach($this->wire('config')->userTemplateIDs as $templateID) {
				$template = $this->wire('templates')->get($templateID);
				if(!$template) continue;
				$fieldgroup = $template->fieldgroup;
				if($fieldgroup && $fieldgroup->hasField($f)) {
					$fieldgroup->remove($f);
					$saveFieldgroups[$fieldgroup->id] = $fieldgroup;
				}
			}
		}
		foreach($saveFieldgroups as $fieldgroup) {
			$fieldgroup->save();
		}
	}


}