<?php

/**
 * This is the email template used by the 'Reject Publish' feature in ProDrafts
 *
 * CUSTOMIZE
 * =========
 * - To customize this email, copy to /site/templates/ProDrafts/email-reject-publish.php and edit.
 * - The contents of the <title> tag is used as the EMAIL SUBJECT.
 * - Inline styles are recommended in the markup since not all email clients support <style></style> declarations.
 * - Content after the closing </html> tag is used as the text-only version of the email.
 *
 * VARIABLES
 * =========
 * @var Page $page
 * @var User $user User that rejected the draft
 * @var ProDraft $draft
 * @var Config $config
 * @var string $editUrl URL to edit page
 * @var string $status
 * @var string $notes
 * @var array $labels
 *
 */

if(!defined("PROCESSWIRE")) die();

/******************************************************************************
 * HTML EMAIL CONTENT
 *
 */
?><!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php
		// Note the following is used as the email subject
		echo sprintf(__('Draft rejected by %s'), $user->name) . " ($config->httpHost)";
	?></title>
</head>
<body>
	<?php
	// The rows of information we output in our email
	echo proDraftsEmailTable(array(
		$labels['page'] => "<a href='$draft->httpUrl'>" . $page->get('title|path') . "</a>",
		$labels['user'] => "$user->name ($user->email)",
		$labels['status'] => $status,
		$labels['notes'] => $notes,
	));
	?>
	<p><a href='<?php echo $editUrl; ?>'><?php echo __('Please click here to make edits to this draft'); ?></a></p>
	<p><small>ProcessWire ProDrafts &bull; <?php echo date('Y/m/d g:ia'); ?></small></p>
</body>
</html><?php

/******************************************************************************
 * TEXT-ONLY EMAIL CONTENT
 *
 */

echo "$labels[page]: $page->title\n";
echo "$labels[url]: $draft->httpUrl\n";
echo "$labels[user]: $user->name ($user->email)\n";
echo "$labels[status]: $status\n";
echo "$labels[notes]: $notes\n\n";
echo __('Please make edits to this draft by clicking the URL below:') . "\n"; 
echo $editUrl;

