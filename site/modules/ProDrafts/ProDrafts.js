/**
 * ProcessWire Pro Drafts: Common/shared javascript
 *
 * Copyright (C) 2016 by Ryan Cramer
 *
 * https://processwire.com/ProDrafts/
 *
 */

/**
 * Initialize long-click events
 * 
 * @constructor
 * 
 */
function ProDraftsLongClick(selector) {
	if(typeof selector == "undefined") selector = ".pwpd-longclick";
	var timer = null;
	jQuery(document).on('mousedown', selector, function() {
		var $a = jQuery(this);
		timer = setTimeout(function(){
			var url = $a.prop('href');
			var $parent = $a.parent();
			
			if($a.attr('data-pwpd-longclick-new')) {
				window.open($a.attr('data-pwpd-longclick-new'));
			} else if($a.attr('data-pwpd-longclick')) {
				window.location.href = $a.attr('data-pwpd-longclick');
			} else if($parent.hasClass('PageListActionEdit') 
				&& $('body').hasClass('ProDraftsUseLivePreview')
				&& $parent.next().hasClass('PageListActionView')) {
				// specific to ProcessPageList and Lister
				url = url.replace('/page/edit/', '/page/prodrafts/edit/') + '&exit=list';
				window.location.href = url;
			} else {
				window.open(url);
			}
			
		}, 1000);
	}).on('mouseup mouseleave', selector, function(){
		clearTimeout(timer);
	});
}

/**
 * Close dialog from within dialog
 * 
 */
function ProDraftsCloseDialog() {
	jQuery(document).ready(function() { 
		window.parent.jQuery('.ui-dialog-content').dialog('close'); 
	});
}

/**
 * jQuery document.ready
 * 
 */
jQuery(document).ready(function($) {
	//ProDraftsLongClick('a.pwpd-longclick, .PageListActionEdit > a');
	ProDraftsLongClick('a.pwpd-longclick');
});