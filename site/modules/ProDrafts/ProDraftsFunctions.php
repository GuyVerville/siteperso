<?php

/**
 * Render a table for a ProDrafts notification email
 * 
 * @param array $rows Associative array of [ '<th> content' => '<td content'> ]
 * @return string
 * 
 */
function proDraftsEmailTable(array $rows) {
	
	// Styles for the table <th> tags	
	$thStyle =
		"width: 30%;" .
		"text-align: right; " .
		"font-weight: bold; " .
		"padding: 10px 10px 10px 0; " .
		"vertical-align: top; " .
		"border-top: 1px solid #ccc;";

	// Styles for the table <td> tags	
	$tdStyle =
		"width: 70%;" .
		"padding: 10px 0 10px 0;" .
		"border-top: 1px solid #ccc;";

	$out = "<table style='width: 100%; border-bottom: 1px solid #ccc;' cellspacing='0'>";
	
	foreach($rows as $th => $td) {
		$out .= "<tr><th style='$thStyle;'>$th</th><td style='$tdStyle'>$td</td></tr>";
	}
	
	$out .= "</table>";
	
	return $out;
}
