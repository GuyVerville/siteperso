<?php

/** 
 * ProcessWire ProDrafts: ProDraftPage
 * 
 * ProDraftPage is used only as an interface for phpdoc type hinting hooks and nothing more.
 * 
 * Copyright (C) 2017 by Ryan Cramer
 *
 * This is commercially licensed and supported software
 * PLEASE DO NOT DISTRIBUTE
 * 	
 * https://processwire.com/ProDrafts/
 * 
 * @property ProDraft|null $_isDraft ProDraft object, when page is currently populated with that ProDraft (set by ProDraft::populatePage).
 * @property bool|null $_wasDraft Runtime var that when true, indicates that Page was previously a draft during this request. 
 * @property bool|null $_hasDraft Whether or not the current page has a draft available (cached from $drafts->hasDraft($page));
 * @property int|null $_PWPD_status ProDraftsHooks temporarily stores previous page status in this at runtime.
 * @property array|null $_ProDrafts Cache of ProDraft objects used by ProDraftsHooks
 * 
 */
class ProDraftPage extends Page {
	
	public function __constuct(Template $tpl = null) {
		if($tpl) {}
		throw new WireException("This class is for phpdoc type hinting only and may not be instantiated"); 
	}

	/**
	 * Get a ProDraft for page, or other actions
	 *
	 * USAGE
	 * =====
	 * $page->draft(); returns ProDraft object for $page.
	 * $page->draft(true); to create a new draft of the page in the database, if one doesn't already exist. Returns ProDraft.
	 * $page->draft(123); to retrieve a past version draft (where 123 is version); null returned if version does not exist.
	 *
	 * @param bool|string|int $key Specify one of the following:
	 *  - omit to return a ProDraft object for $page.
	 *  - boolean true to create a new draft.
	 *  - integer to retrieve past version draft. 
	 *  - string name of property to get and return.
	 *  - string name of property to set (when combined with $value argument)
	 * @param mixed|null If using $key argument as a property to set, specify the value here. 
	 * @return ProDraft|mixed
	 *
	 */
	public function draft($key = false, $value = null) { 
		return null;
	}

	/**
	 * Returns instance of ProDraft if page is a draft or version, or boolean false if it's not.
	 * 
	 * Alternatively, specify an argument of false if not a draft, or ProDraft if it is a draft, and the
	 * value will be set.
	 *
	 * USAGE
	 * =====
	 * $draft = $page->isDraft(); // returns ProDraft object if draft/version or boolean false if not
	 * $page->isDraft($draft); // Provide a ProDraft object to set the current value...
	 * $page->isDraft(false); // ...or specify boolean false to unset it
	 *
	 * @param ProDraft|bool|null $isDraft
	 * @return ProDraft|bool
	 * @throws WireException if given invalid argument
	 *
	 */
	public function isDraft($isDraft = null) { 
		if($isDraft) {}
		return false;
	}

	/**
	 * Does the page have a Draft version available?
	 *
	 * Note that it populates a boolean _hasDraft property to the page to cache the result.
	 * Or a _hasDraft123 if version requested (where 123 is version number)
	 *
	 * @param int $version Optional version number, if checking to see if it has a particular draft version.
	 * @return bool|int True if has draft, false if not, or integer if has requested version (which indicates version number).
	 *
	 */
	public function hasDraft($version = 0) { 
		if($version) {}
		return false;
	}

	/**
	 * Get the draft version of the page with draft content populated to it
	 *
	 * Alias of the page() method.
	 *
	 * @return Page
	 *
	 */
	public function draftPage() { 
		return $this;
	}

	/**
	 * Get a copy of the original/live page without draft content
	 *
	 * @return Page
	 *
	 */
	public function livePage() { 
		return $this;
	}

	/**
	 * Get array of changed field names
	 *
	 * @return array
	 *
	 */
	public function draftChanges() { 
		return array();
	}

	/**
	 * Save a draft page
	 *
	 * This is the same as doing a $pages->save() except that it will create
	 * a new draft and save it if one doesn't already exist.
	 *
	 * Also note that if you provide this with a $page that isn't already a draft,
	 * it will load the current draft (if it exists) or create a new one.
	 *
	 * @param null|string|Field $field Optionally save only given field
	 * @return bool|ProDraft Returns ProDraft object on success, boolean false on fail
	 * @throws WireException
	 *
	 */
	public function saveDraft($field = null) {
		if($field) {}
		return false;
	}

	/**
	 * Publish the draft for the given draft or $page
	 *
	 * @param Field|int|string $field Optionally limit publish to a specific field
	 * @return bool
	 * @throws WireException if given invalid arguments or publish fails
	 *
	 */
	public function publishDraft($field = null) {
		if($field) {}
		return false;
	}

	/**
	 * Delete the draft for the given draft or $page
	 *
	 * @param Field|int|string $field Optionally limit delete to a specific field
	 * @return bool
	 * @throws WireException if given invalid arguments or delete fails
	 *
	 */
	public function deleteDraft($field = null) { 
		if($field) {}
		return false;
	}


	/**
	 * Retrieve a ProDraft version object
	 *
	 * USAGE
	 * =====
	 * $page->version(123456); to retrieve ProDraft version object, where 123456 is version number.
	 *
	 * @param int $versionID
	 * @throws WireException for invalid arguments
	 *
	 */
	public function version($versionID) { }
	
	/**
	 * Return array of all versions for the given $page
	 *
	 * @param array $options
	 * @return array of ProDraft objects, indexed by version number, newest to oldest
	 *
	 */
	public function versions($options = array()) { 
		if($options) {}
		return array();
	}
		
}