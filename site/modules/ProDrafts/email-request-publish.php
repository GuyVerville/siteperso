<?php

/**
 * This is the email template used by the 'Request Publish' feature in ProDrafts
 * 
 * CUSTOMIZE
 * =========
 * - To customize this email, copy to /site/templates/ProDrafts/email-request-publish.php and edit.
 * - The contents of the <title> tag is used as the EMAIL SUBJECT.
 * - Inline styles are recommended in the markup since not all email clients support <style></style> declarations.
 * - Content after the closing </html> tag is used as the text-only version of the email. 
 *
 * VARIABLES
 * =========
 * @var Page $page
 * @var User $user 
 * @var ProDraft $draft
 * @var Config $config
 * @var string $editUrl URL to approve, reject or edit $page
 * @var string $status
 * @var string $notes
 * @var array $labels
 *
 */
if(!defined("PROCESSWIRE")) die();

// The rows of information we output in our email

/******************************************************************************
 * HTML EMAIL CONTENT
 * 
 */
?><!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php 
		// Note the following is used as the email subject
		echo sprintf(__('New draft by %s submitted for approval'), $user->name) . " ($config->httpHost)";
	?></title>
</head>
<body>
	<?php
	// render a simple table with draft info
	echo proDraftsEmailTable(array(
		$labels['page'] => "<a href='$draft->httpUrl'>" . $page->get('title|path') . "</a>",
		$labels['user'] => "$user->name ($user->email)",
		$labels['changes'] => implode(', ', $draft->changes()),
		$labels['status'] => $status,
		$labels['notes'] => $notes,
	));
	?>
	<p><a href='<?php echo $editUrl; ?>'><?php echo __('Please click here to approve or reject this draft'); ?></a></p>
	<p><small>ProcessWire ProDrafts &bull; <?php echo date('Y/m/d g:ia'); ?></small></p>
</body>
</html><?php

/******************************************************************************
 * TEXT-ONLY EMAIL CONTENT
 *
 */

echo "$labels[page]: $page->title\n";
echo "$labels[url]: $draft->httpUrl\n";
echo "$labels[user]: $user->name ($user->email)\n";
echo "$labels[changes]: " . implode(', ', $draft->changes()) . "\n";
echo "$labels[status]: $status\n\n";
echo __('Please approve or reject this draft by clicking the URL below:') . "\n";
echo $editUrl;

