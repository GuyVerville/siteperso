<?php 

/**
 * ProcessWire ProDrafts: ProDraft
 * 
 * ProDraft manages the draft data for a Page. 
 * 
 * Copyright (C) 2017 by Ryan Cramer
 *
 * This is commercially licensed and supported software
 * PLEASE DO NOT DISTRIBUTE
 *
 * https://processwire.com/ProDrafts/
 * 
 * @property ProDraftPage|Page $page
 * @property string $name
 * @property int $version Always 0 for draft, or greater than 0 for past version
 * @property int $created
 * @property int $modified
 * @property int $modified_users_id
 * @property int $created_users_id
 * @property int $request_users_id User ID that requested publish, when applicable
 * @property int $reject_users_id User ID that rejected publish, when applicable
 * @property User|NullPage $createdUser
 * @property User|NullPage $modifiedUser
 * @property User|NullPage $requestUser
 * @property User|NullPage $rejectUser
 * @property int $flags
 * @property array $content All the draft content, indexed by field name
 * @property bool $publish Has publish been indicated?
 * @property string|null $publishField Has publish been indicated for just 1 field? (value is field name)
 * @property bool $populated Has the draft been populated?
 * @property int $pageStatus Status of page, before draft populated it
 * @property bool $contentLoaded Whether or not the data/content has been loaded from DB yet
 * @property array $changes Changed field names
 * @property string $versionName Name of this draft version, which is either "Draft" or version date. 
 * @property string url
 * @property string httpUrl
 * 
 * @method publish($fieldName = null) Publish entire draft or just a field from it
 * @method delete($fieldName = null) Delete entire draft or just a field from it
 * 
 */

class ProDraft extends WireData {

	/**
	 * Flag indicating publish has been requested by a user without publish permission (request_users_id)
	 * 
	 */
	const flagPublishRequested = 2;

	/**
	 * Flag indicating draft is owned and managed by another page (like repeater or PageTable items)
	 * 
	 */
	const flagRepeater = 1024;
	
	/**
	 * Page that this draft is for
	 * 
	 * @var ProDraftPage|NullPage|Page
	 * 
	 */
	protected $page;

	/**
	 * Page that contains the draft
	 * 
	 * @var null|Page
	 * 
	 */
	protected $draftPage;

	/**
	 * Instance of ProDrafts
	 * 
	 * @var ProDrafts
	 * 
	 */
	protected $drafts;

	/**
	 * Array where page content is stored, indexed by "_$field->id" or native property name
	 * 
	 * @var array
	 * 
	 */
	protected $content = array();

	/**
	 * Array of extra meta data
	 * 
	 * @var array
	 * 
	 */
	protected $meta = array(
		'changes' => array(),
		'request_users_id' => 0,
		'reject_users_id' => 0,
	);

	/**
	 * Construct the ProDraft
	 * 
	 * @param ProDrafts $drafts
	 * @param Page $page
	 * 
	 */
	public function __construct(ProDrafts $drafts, Page $page = null) {
		$this->drafts = $drafts; 
		$this->page = $page ? $page : new NullPage();
		
		// DB fields
		$this->set('name', '');
		$this->set('version', 0);
		$this->set('created', time());
		$this->set('modified', time());
		$this->set('created_users_id', 0);
		$this->set('modified_users_id', 0);
		$this->set('flags', 0);
		
		// runtime fields
		$this->set('publish', false);
		$this->set('publishField', null);
		$this->set('populated', false);
		$this->set('pageStatus', $page->status);
		$this->set('contentLoaded', false);
	}
	
	/**
	 * Publish entire draft or just a field from it
	 * 
	 * @param Field|int|string|null $fieldName Optional field to publish (default=publish all)
	 * @return bool
	 * 
	 */
	public function ___publish($fieldName = null) {
		$this->drafts->debugMessage("Publishing field: $fieldName", 'ProDraft.' . __FUNCTION__);
		return $this->drafts->publishDraft($this, $fieldName);
	}

	/**
	 * Is the current user allowed to publish this draft?
	 * 
	 * @param string $fieldName Optionally limit check to a specific field
	 * @return bool
	 * 
	 */
	public function publishable($fieldName = '') {
		$options = $fieldName ? array('fieldName' => $fieldName) : array();
		return $this->drafts->hasDraftPermission($this, 'publish', $options);
	}

	/**
	 * Delete entire draft or just a field from it
	 *
	 * @param Field|int|string|null $fieldName Optional field to delete (default=delete all)
	 * @return bool
	 *
	 */
	public function ___delete($fieldName = null) {
		$this->drafts->debugMessage("Deleting field: $fieldName", 'ProDraft.' . __FUNCTION__);
		return $this->drafts->deleteDraft($this, $fieldName);
	}

	/**
	 * Is the current user allowed to delete this draft?
	 *
	 * @param string $fieldName Optionally limit check to a specific field
	 * @return bool
	 *
	 */
	public function deletable($fieldName = '') {
		$options = $fieldName ? array('fieldName' => $fieldName) : array();
		return $this->drafts->hasDraftPermission($this, 'delete', $options);
	}

	/**
	 * Populate the page with draft content
	 * 
	 * @param ProDraftPage|Page $page Page to populate to, or omit to populate $page assigned to this ProDraft object.
	 * @param Field|int|string $field Optionally specify a field to populate (default=populate all)
	 * @return $this
	 * 
	 */
	public function populatePage(Page $page = null, $field = null) {
		
		if(is_null($page)) $page = $this->page; 		
		$natives = $this->drafts->getNativePageProperties();
		
		$onlyFieldName = is_null($field) ? null : $this->keyToField($field);
		$of = $page->of();
		$page->of(false);
		if(is_null($onlyFieldName)) $this->set('populated', true);
		$page->setTrackChanges(false);
		$page->isDraft($this); // indicate that the page is populated with this draft
		
		$content = $this->getContent();

		foreach($content as $fieldName => $sleepValue) {
			
			if(!is_null($onlyFieldName) && $onlyFieldName != $fieldName) continue; 
			
			if(in_array($fieldName, $natives)) {
				// native field
				$page->set($fieldName, $sleepValue);
				
			} else {
				// custom field
				$field = $this->wire('fields')->get($fieldName);
				if(!$field) continue; // field no longer exists?
				$page->of(false);
				$this->populatePageFieldValue($page, $field, $sleepValue);		
			}
		}

		$page->setTrackChanges(true);
		$page->of($of);
	
		return $this;
	}

	/**
	 * Accompanies populatePage() method, given a sleepValue, convert to wakeupValue and populate page
	 * 
	 * @param Page $page
	 * @param Field $field
	 * @param mixed $sleepValue
	 * 
	 */
	protected function populatePageFieldValue(Page $page, Field $field, $sleepValue) {
		
		if(1 == 2 && method_exists($field->type, 'hasDraft')) {
			// @todo future support for this (currently disabled via 1 == 2)
			// field providing its own draft loader, like FieldtypeTable
			$page->addStatus(Page::statusDraft);
			$sleepValue = $field->type->loadPageField($page, $field);
			$wakeupValue = $field->type->wakeupValue($page, $field, $sleepValue);
		} else {
			// populate the draft value to the page
			$wakeupValue = $field->type->wakeupValue($page, $field, $sleepValue);
		}
		
		$page->of(false);
		$isRepeater = $this->drafts->isRepeater($field);
		
		// check for repeater items, which need to be populated also
		if($isRepeater && !empty($wakeupValue) && $this->drafts->repeaterSupport) {
			if(is_array($sleepValue) && !empty($sleepValue['data']) && is_string($sleepValue['data'])) {
				$sorts = explode(',', $sleepValue['data']);
			} else {
				$sorts = array();
			}
			$needsSort = false;
			if($wakeupValue instanceof Page) {
				$wakeupValue = array($wakeupValue); // i.e. FieldsetPage
			}
			foreach($wakeupValue as $item) {
				/** @var ProDraftPage $item */
				$d = $item->draft();
				$d->addFlag(ProDraft::flagRepeater);
				$d->populatePage();
				$sort = array_search($item->id, $sorts);
				if(is_int($sort)) {
					$item->sort = $sort;
					$needsSort = true;
				}
			}
			if(is_array($wakeupValue)) {
				$wakeupValue = reset($wakeupValue); // i.e. FieldsetPage
			} else if($needsSort) {
				$wakeupValue->sort('sort');
			}
		}
		
		// populate the draft value to the page
		$page->setFieldValue($field->name, $wakeupValue, false);
	}

	/**
	 * Get a draft property value
	 * 
	 * @param string $key
	 * @return mixed
	 * 
	 */
	public function get($key) {
		if($key == 'page') return $this->page;
		if($key == 'draftPage') return $this->draftPage();
		if($key == 'pages_id') return $this->page->id;
		if($key == 'modifiedUser') return $this->wire('users')->get($this->modified_users_id);
		if($key == 'createdUser') return $this->wire('users')->get($this->created_users_id);
		if($key == 'content') return $this->getContent();
		if($key == 'changes') return $this->changes();
		if($key == 'meta') return $this->meta;
		if($key == 'request_users_id') return (int) $this->getMeta('request_users_id');
		if($key == 'reject_users_id') return (int) $this->getMeta('reject_users_id');
		if($key == 'url') return $this->url();
		if($key == 'httpUrl' || $key == 'httpURL') return $this->url(true);
		
		if($key == 'versionName') {
			$version = parent::get('version');
			if($version > 0) return wireDate('Y-m-d H:i:s', $version); 
			return $this->_('Draft');
			
		} else if($key == 'requestUser') {
			$request_users_id = (int) $this->getMeta('request_users_id');
			if(!$request_users_id) return new NullPage();
			return $this->wire('users')->get($request_users_id);
			
		} else if($key == 'rejectUser') {
			$reject_users_id = (int) $this->getMeta('reject_users_id');
			if(!$reject_users_id) return new NullPage();
			return $this->wire('users')->get($reject_users_id);
		}
		
		return parent::get($key);
	}

	/**
	 * Set a draft property value
	 * 
	 * @param string $key
	 * @param mixed $value
	 * @return ProDraft|WireData
	 * @throws WireException
	 * 
	 */
	public function set($key, $value) {
		
		if($key == 'pages_id') {
			$value = (int) $value;
			if($value == $this->page->id) {
				return $this;
			} else {
				$this->page = $this->wire('pages')->get($value);
			}
			return $this;
			
		} else if($key == 'page') {
			if($value instanceof Page) $this->page = $value;
			return $this;
			
		} else if($key == 'content') {
			if(!is_array($value)) throw new WireException("content must be an array"); 
			return $this->setContent($value);
			
		} else if($key == 'modified' || $key == 'created') {
			if(is_string($value)) $value = strtotime($value);
			if(!is_int($value)) $value = (int) $value; 
			
		} else if($key == 'modifiedUser') {
			$key = 'modified_users_id';
			$value = (int) (string) $value; 
			
		} else if($key == 'createdUser') {
			$key = 'created_users_id';
			$value = (int) (string) $value;

		} else if($key == 'requestUser' || $key == 'request_users_id') {
			$value = (int) (string) $value;
			$this->setMeta('request_users_id', $value);
			return $this;
			
		} else if($key == 'rejectUser' || $key == 'reject_users_id') {
			$value = (int) (string) $value;
			$this->setMeta('reject_users_id', $value);
			return $this;
			
		} else if($key == 'changes') {
			if(is_array($value)) $this->meta['changes'] = $value;
			return $this;
			
		} else if($key == 'meta') {
			return $this->setMeta($value);
			
		} else if($key == 'flags' || $key == 'version' || $key == 'modified_users_id' || $key == 'created_users_id') {
			$value = (int) $value;
		}
		
		return parent::set($key, $value);
	}

	/**
	 * Set the draft meta data
	 *
	 * @param string|Field|int|array $key 
	 * @param mixed $value Value to set, or omit if setting all $meta at once 
	 * @return $this
	 *
	 */
	public function setMeta($key, $value = null) {
		
		if(is_null($value) && is_array($key)) {
			// set all
			foreach($key as $k => $v) {
				$this->meta[$k] = $v;
			}

		} else {
			$this->meta[$key] = $value;
		}
		
		return $this;
	}

	/**
	 * Unset a meta property
	 * 
	 * @param $key
	 * @return $this
	 * 
	 */
	public function unsetMeta($key) {
		unset($this->meta[$key]);
		return $this;
	}

	/**
	 * Get draft meta data
	 * 
	 * @param null $key Meta property key or omit to return all
	 * @return array|null
	 * 
	 */
	public function getMeta($key = null) {
		if(is_null($key)) return $this->meta;
		return isset($this->meta[$key]) ? $this->meta[$key] : null;
	}
	
	/**
	 * Set the draft content for a particular field name
	 * 
	 * @param string|Field|int|array $field May be field name, id or Field object, or array of all $content (and omit 2nd arg)
	 * @param mixed $value Value to set, or omit if setting all $content at once 
	 * @param string $func Function that called setContent, for debugging purposes
	 * @throws WireException if given something invalid
	 * @return $this
	 * 
	 */
	public function setContent($field, $value = null, $func = null) {
		
		if(is_null($value) && is_array($field)) {
			// set all
			foreach($field as $k => $v) {
				$this->setContent($k, $v, $func);
			}
			$this->contentLoaded = true;
			
		} else {
			// set one field
			$field = $this->keyToField($field);
			if($field && $this->drafts->isAllowedField($field, $this)) {
				$this->content[$field] = $value;
				$this->setMetaChange($field);
				// $this->drafts->debugMessageLog("setContent($field, '$value') from $func", __FUNCTION__);
			}
		}
		
		return $this;
	}

	/**
	 * Unset the given field from the draft
	 * 
	 * @param $field
	 * @return $this
	 * 
	 */
	public function unsetContent($field) {
		$field = $this->keyToField($field);
		unset($this->content[$field]); 
		$this->unsetMetaChange($field);
		return $this;
	}

	/**
	 * Get all content, or value from content for a particular field that you specify
	 * 
	 * Specify boolean TRUE for $field if you want to receive all $content in DB storage format
	 * 
	 * @param null|string|int|Field|bool $field Omit to retrieve all or field name/key to retrieve for that field; or...
	 *   Specify boolean true if you want to receive all $content in DB storage format
	 * @return array|mixed
	 * 
	 */
	public function getContent($field = null) {

		if(!$this->contentLoaded && $this->exists()) {
			$this->drafts->loadContent($this);
		}
		
		if(is_null($field)) {
			// return all
			return $this->content;
		}
		
		if($field === true) {
			// return all in DB storage format
			$a = array();
			foreach($this->content as $fieldName => $value) {
				$key = $this->fieldToKey($fieldName);
				if($key) $a[$key] = $value; 
			}
			return $a;
		}
		
		// return content for just one field
		$field = $this->keyToField($field);
		if(empty($field)) return null;
		
		return isset($this->content[$field]) ? $this->content[$field] : null;
	}

	/**
	 * Set a field name to meta changes
	 * 
	 * @param string $fieldName
	 * @param bool $unset
	 * 
	 */
	public function setMetaChange($fieldName, $unset = false) {
		if(!isset($this->meta['changes'])) $this->meta['changes'] = array();
		if($unset) {
			$key = array_search($fieldName, $this->meta['changes']);
			if($key !== false) unset($this->meta['changes'][$key]);
		} else {
			if(!in_array($fieldName, $this->meta['changes'])) {
				$this->meta['changes'][] = $fieldName;
			}
		}
	}

	/**
	 * Unset a field name from meta changes
	 * 
	 * @param string $fieldName
	 * 
	 */
	protected function unsetMetaChange($fieldName) {
		return $this->setMetaChange($fieldName, true);
	}
	
	/**
	 * Get array of changed field names
	 *
	 * @return array
	 *
	 */
	public function changes() {
		return isset($this->meta['changes']) ? $this->meta['changes'] : array_keys($this->getContent());
	}

	/**
	 * Convert an unknown field key to a content key
	 *
	 * @param null|string|int|Field $field
	 * @return string
	 *
	 */
	public function fieldToKey($field) {
		if(is_object($field)) {
			if($field instanceof Field) {
				$field = "_$field->id";
			} else {
				$field = (string) $field;
			}
		} else if(is_int($field)) {
			// field ID, convert to string ID
			$field = "_$field";
		} else if(is_string($field)) {
			if(strpos($field, '_') === 0 && ctype_digit(substr($field, 1))) {
				// field is already in the proper storage format
			} else if(ctype_digit($field)) {
				// field ID
				$field = "_$field";
			} else if($this->drafts->isNativePageProperty($field)) {
				// native page property, current format is good
			} else {
				// field name
				$f = $this->wire('fields')->get($field);
				if($f) {
					$field = "_$f->id";
				} else {
					// unknown, just keep as-is
				}
			}
		}
		return $field;
	}

	/**
	 * Given string|int|Field, convert it to a field name, or NULL if not valid
	 * 
	 * @param string|int|Field $key
	 * @return string Field name
	 * 
	 */
	public function keyToField($key) {
		$result = null;
		if(is_string($key)) {
			$k = ltrim($key, '_');
			if(ctype_digit($k)) {
				$field = $this->wire('fields')->get((int) $k);
				$result = $field ? $field->name : null;
			} else {
				$result = $key;
			}
		} else if(is_int($key)) {
			$field = $this->wire('fields')->get($key);
			$result = $field ? $field->name : null;
		} else if($key instanceof Field) {
			$result = $key->name;
		}
		return $result;
	}

	/**
	 * Does an actual draft exist for the page this ProDraft represents?
	 * 
	 * @return bool
	 * 
	 */
	public function exists() {
		return strlen($this->name) > 0;
	}

	/**
	 * Get a copy of the Page with draft content populated to it, alias of draftPage() method
	 * 
	 * @param bool $clone Whether or not to return a copy/clone of the page (default=true)
	 * @return Page
	 * 
	 */
	public function page($clone = true) {
		return $this->draftPage($clone);
	}

	/**
	 * Get the draft version of the page with draft content populated to it
	 * 
	 * Alias of the page() method.
	 *
	 * @param bool $clone Whether or not to return a copy/clone of the page (default=true)
	 * @return Page
	 *
	 */
	public function draftPage($clone = true) {
		if($this->draftPage && $clone) return $this->draftPage;
		$page = $clone ? clone $this->page : $this->page;
		$this->populatePage($page);
		if($clone) $this->draftPage = $page;
		return $page;
	}

	/**
	 * Get a copy of the original/live page without draft content
	 * 
	 * @param bool $clone Whether to return a cloned copy (default=true)
	 * @return Page
	 * 
	 */
	public function livePage($clone = true) {
		$page = $this->page;	
		if(!$page->isDraft()) {
			// if the $page isn't yet populated, we can just return that
			return $clone ? clone $page : $page;
		}	
		$options = array(
			'getFromCache' => false,
			'cache' => false,
		);
		$livePage = $this->wire('pages')->getById(array($page->id), $options)->first();
		if($livePage === $page) {
			// for older PW versions that don't support getFromCache option
			$this->wire('pages')->uncacheAll();
			$livePage = $this->wire('pages')->getById(array($page->id), $options)->first();
		}
		if(!$livePage) $livePage = new NullPage();
		return $livePage;
	}

	/**
	 * Does this draft have any files present in it that differ from the live files?
	 * 
	 * @return bool
	 * 
	 */
	public function hasChangedFiles() {
		return count($this->getChangedFiles(true)) > 0;
	}

	/**
	 * Return array of files that differ between live and draft
	 *
	 * @param bool $quickCheck Specify true to stop scanning after first change is found 
	 * @return array Returns array where keys are the basenames and values are text description of differences
	 * 
	 */
	public function getChangedFiles($quickCheck = false) {
		$changes = array();
		$dirs = $this->drafts->getFileDirs($this->page);
		if(!is_dir($dirs['_path'])) return array();
		$dir = opendir($dirs['_path']);
		if(!$dir) return $changes;
		while(($file = readdir($dir)) !== false) {
			if($file === "." || $file === ".." || $file[0] === ".") continue;
			if(is_dir($file)) continue;
			$liveFile = $dirs['path'] . $file;
			$draftFile = $dirs['_path'] . $file;
			if(!file_exists($liveFile)) {
				// file exists in draft, but not in live
				$changes[$file] = $this->_('Exists in draft but not in live');
			} else {
				$draftMtime = filemtime($draftFile);
				$liveMtime = filemtime($liveFile);
				if($draftMtime > $liveMtime) {
					// file in draft has a newer modification time than one in live
					$changes[$file] = sprintf($this->_('Draft file newer than live (%s > %s)'), wireDate($draftMtime), wireDate($liveMtime));
				} else if($draftMtime < $liveMtime) {
					$changes[$file] = sprintf($this->_('Draft file older than live (%s < %s)'), wireDate($draftMtime), wireDate($liveMtime));
				}
			}
			if($quickCheck && count($changes)) break;
		}
		closedir($dir);
		return $changes;
	}

	/**
	 * Does this draft contain any changes?
	 * 
	 * Different from checking the 'changes' property because it accounts for other factors like files,
	 * where there might be a change to a specific file, but not to any of the DB meta data stored with it.
	 * Also able to compare values as well as identify redundancies. 
	 * 
	 * Note: if using $compareValues integer 1 or 2 (to get changed values) if live and draft /site/assets/files differ,
	 * then it will return an index of '__other' in the returned array with value(s) of 'files'.
	 * 
	 * @param bool|int $compareValues Specify boolean true to confirm the changes by actually comparing the values; or...
	 *   Specify integer 1 to return an array of changed field values indexed by name
	 *   Specify integer 2 to return same as 1, but with 'live' and 'draft' values both included (array of arrays)
	 *   Specify integer 3 to return same as 1, but with redundant values removed from this ProDraft's content
	 *   Specify integer 4 to return an array of values stored in draft that are redundant (same value as live), indexed by name
	 *   Specify integer 5 to return same as 4, and also remove the redundant value from this ProDraft's content
	 * @return bool|array
	 * 
	 * 
	 */
	public function hasChanges($compareValues = false) {

		$getValues = $compareValues && !is_bool($compareValues);
		$getBothValues = $compareValues === 2;
		$getRedundant = $compareValues === 4 || $compareValues === 5;
		$removeRedundant = $compareValues === 3 || $compareValues === 5;
		
		if(!$compareValues && !empty($this->meta['changes'])) return true;
		
		$content = $this->getContent();
		$hasChanges = count($content);
		if($hasChanges && !$compareValues) return true; 
		$hasChangedFiles = $this->hasChangedFiles();
		
		if(!$getValues) {
			if($hasChangedFiles) return true; // always return true if there are changed files
			if(!$hasChanges) return false;
		}
		
		// compare values
		// get copies of both draft and live page
		$livePage = $this->livePage();
		$hasChanges = false;
		$natives = $this->drafts->getNativePageProperties();
		$changes = array();
		$redundant = array();
		
		foreach($content as $fieldName => $draftValue) {
			
			if(in_array($fieldName, $natives)) {
				// native pages table column
				$liveValue = $livePage->get($fieldName);
				if($draftValue !== $liveValue) {
					$hasChanges = true;
					$changes[$fieldName] = $getBothValues ? array('live' => $liveValue, 'draft' => $draftValue) : $draftValue;
					if(!$getValues) break;
				} else {
					$redundant[$fieldName] = $draftValue;
					if($removeRedundant) {
						$this->unsetContent($fieldName);
						$this->drafts->debugMessageLog("Removed redundant value for: $fieldName", 'ProDraft.' . __FUNCTION__);
					}
				}
				continue;
			}
			
			// custom field
			$liveValue = $livePage->getUnformatted($fieldName);
			$field = $this->wire('fields')->get($fieldName); 
			if($field && $this->drafts->isRepeater($field) && $this->drafts->repeaterSupport) {
				// for Repeaters: convert $draftValue to awake value and don't sleep $liveValue 
				$draftValue = $this->draftPage()->getUnformatted($fieldName);
			} else if($field) {
				// for all other field types, use sleep values
				$liveValue = $field->type->sleepValue($livePage, $field, $liveValue);
			}
			
			$isDifferent = false;
			$isObject = is_object($draftValue); // currently only possible with repeaters
		
			if(empty($draftValue) && !empty($liveValue)) {
				// one is empty and the other not
				$isDifferent = true;
			} else if(is_array($draftValue)) {
				// array comparison
				if($draftValue !== $liveValue) $isDifferent = true;
			} else if(WireArray::iterable($draftValue) && (count($draftValue) != count($liveValue))) {
				// array with different counts
				$isDifferent = true;
			} else if($draftValue instanceof PageArray) {
				// PageArray: compare individual pages
				$a1 = $draftValue->getArray();
				$a2 = $liveValue->getArray();
				while(count($a1)) {
					$v1 = array_shift($a1);
					$v2 = array_shift($a2);
					if(count($this->drafts->getPageDifferences($v1, $v2, true))) {
						$isDifferent = true;
						break;
					}
				}
			} else if($isObject && $draftValue instanceof Page) {
				// Page: compare fields
				if(count($this->drafts->getPageDifferences($draftValue, $liveValue))) $isDifferent = true;
			} else if($draftValue != $liveValue || strlen($draftValue) != strlen($liveValue)) {
				// other type comparison (string, number, etc.)
				$isDifferent = true; 
			}
			
			if($isDifferent) {
				$this->drafts->debugMessageLog("Values differ for: $fieldName", 'ProDraft.' . __FUNCTION__);
				$hasChanges = true;
				$changes[$fieldName] = $getBothValues ? array('live' => $liveValue, 'draft' => $draftValue) : $draftValue;
				if(!$getValues) break;
			} else {
				$redundant[$fieldName] = $draftValue;
				if($removeRedundant) {
					$this->unsetContent($fieldName);
					$this->drafts->debugMessageLog("Removed redundant value for: $fieldName", 'ProDraft.' . __FUNCTION__);
				} else {
					$this->drafts->debugMessageLog("Values redundant for: $fieldName", 'ProDraft.' . __FUNCTION__);
				}
			}
		}
		
		if($getValues && !$getRedundant && $hasChangedFiles) {
			$changes['__other'] = $getBothValues ? array('live' => 'files', 'draft' => 'files') : 'files';
		}
		
		if($getRedundant) return $redundant;
		if($getValues) return $changes;
		
		return $hasChanges;
	}

	/**
	 * Add given flag
	 * 
	 * @param int $flag
	 * @return bool True if the given flag was removed, false if it was already present
	 * 
	 */
	public function addFlag($flag) {
		if($this->hasFlag($flag)) {
			return false;
		} else {
			$this->flags = $this->flags | $flag;
			$this->trackChange('flags');
			return true;
		}
	}

	/**
	 * Remove given flag
	 *
	 * @param int $flag
	 * @return bool True if flag was removed, false if it wasn't present
	 *
	 */
	public function removeFlag($flag) {
		if($this->hasFlag($flag)) {
			$this->flags = $this->flags & ~$flag;
			$this->trackChange('flags');
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Do we have the given flag?
	 *
	 * @param int $flag
	 * @return bool
	 *
	 */
	public function hasFlag($flag) {
		return $this->flags & $flag;
	}

	/**
	 * Get the URL to this draft
	 * 
	 * - The non-draft page must be viewable to 'guest', otherwise user will have to be logged in.
	 * 
	 * @param bool $http Include scheme and hostname?
	 * @return string
	 * 
	 */
	public function url($http = false) {
		$page = $this->page;
		if(!$page || !$page->id) $page = $this->livePage(false);
		$version = $this->version;
		if($version < 1) $version = 1;
		$url = $http ? $page->httpUrl() : $page->url();
		$url .= "?draft=$version";
		if($this->drafts->allowPreviewURLs) $url .= ",$this->name";
		return $url;
	}
	
	/**
	 * String value of ProDraft
	 * 
	 * @return string
	 * 
	 */
	public function __toString() {
		return 
			"ProDraft [" . 
			"page=$this->page, " .
			($this->version > 0 ? "version=$this->version, " : "") . 
			"changes=" . implode(',', $this->changes()) . 
			"]";
	}

}