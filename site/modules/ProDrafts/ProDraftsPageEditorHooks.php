<?php

/**
 * ProcessWire Pro Drafts: Page Editor Hooks
 *
 * Copyright (C) 2016 by Ryan Cramer
 *
 * PLEASE DO NOT DISTRIBUTE
 *
 * https://processwire.com/ProDrafts/
 * 
 * @todo add a "Draft" section in Settings tab with info about draft and options to abandon any change.
 * 
 */

class ProDraftsPageEditorHooks extends Wire {

	/**
	 * @var ProDrafts
	 * 
	 */
	protected $drafts;

	/**
	 * Current draft being edited
	 * 
	 * @var null|ProDraft
	 * 
	 */
	protected $draft = null;

	/**
	 * The page used by /processwire/page/drafts/ with assigned ProcessProDrafts process
	 * 
	 * @var Page
	 * 
	 */
	protected $proDraftsPage;

	/**
	 * @var Page Page being edited
	 * 
	 */
	protected $editPage;

	/**
	 * Whether or not this request is processing a page edit form
	 * 
	 * @var bool
	 * 
	 */
	protected $processing = false;

	/**
	 * Text for draft notification
	 * 
	 * @var string
	 * 
	 */
	protected $draftNotice = '';

	/**
	 * Actions to accompany $draftNotice
	 * 
	 * @var array of markup strings
	 * 
	 */
	protected $draftActions = array();

	/**
	 * Current Process module
	 * 
	 * @var string|null
	 *
	 */
	protected $currentProcess = null;

	/**
	 * Are we currently in live preview mode?
	 * 
	 * @var bool
	 * 
	 */
	protected $isLivePreview = false;
	
	/** 
	 * @var null|ProDraftsRepeaters 
	 *  
	 */
	protected $repeaters = null;

	/**
	 * Container class or ProDrafts notice
	 * 
	 * @var string
	 * 
	 */
	protected $noticeContainerClass = 'pw-container uk-container uk-container-expand container';

	/**
	 * Construct page editor hooks
	 * 
	 * @param ProDrafts $drafts
	 * @param Page $page
	 * @param ProDraftsRepeaters|null
	 * 
	 */
	public function __construct(ProDrafts $drafts, Page $page, $repeaters) {

		$this->drafts = $drafts;
		$this->isLivePreview = $this->wire('input')->get('pwpd_livepreview') ? true : false;
		$this->repeaters = $repeaters;
		$superuser = $this->wire('user')->isSuperuser();
		$config = $this->wire('config');
		$input = $this->wire('input');
		$skipPageEditRequest = $config->ajax && $input->get('nodraft'); // currently for repeaters
		// $useLivePreview = $this->drafts->livePreview;
	
		if($page->process == 'ProcessPageEditImageSelect') {
			$this->hookProcessPageEditImageSelect();	
			/*
			$this->addHookBefore('ProcessPageEditImageSelect::execute', $this, 'hookProcessPageEditImageSelect');
			$this->addHookBefore('ProcessPageEditImageSelect::executeResize', $this, 'hookProcessPageEditImageSelect');
			$this->addHookBefore('ProcessPageEditImageSelect::executeEdit', $this, 'hookProcessPageEditImageSelect');
			*/
			
		} else if($page->process == 'ProcessPageEdit' && !$skipPageEditRequest) {
			
			$this->processing = count($_POST) > 0 && !$config->ajax;
			$this->addHookAfter('ProcessPageEdit::loadPage', $this, 'hookEditLoadPage');
			$this->addHookAfter('ProcessPageEdit::buildForm', $this, 'hookEditBuildForm');
			$this->addHookBefore('Session::redirect', $this, 'hookSessionRedirect');

			if(!$this->processing) {
				$this->addHookBefore('ProcessPageEdit::buildFormView', $this, 'hookEditBuildFormView');
				if($drafts->maxVersions > 0) {
					$this->addHookAfter('ProcessPageEdit::buildFormSettings', $this, 'hookEditBuildFormSettings');
				}
			}

			$moduleID = $this->wire('modules')->getModuleID('ProcessProDrafts');
			$this->proDraftsPage = $this->wire('pages')->get("template=admin, process=$moduleID, include=all");

		} else if($page->process == 'ProcessProfile' && $this->wire('permissions')->has('page-publish')) {
			$this->addHookBefore('ProcessProfile::execute', $this, 'hookBeforeUserProfileEdit');
			
		} else if(strpos((string) $page->process, 'ProcessPageList') === 0) {
			$config->scripts->add($config->urls->ProDrafts . 'ProDrafts.js');
		} 
		
		if($this->drafts->livePreview) {
			// for detection from ProDrafts.js
			$adminTheme = $this->wire('adminTheme');
			if($adminTheme) $adminTheme->addBodyClass('ProDraftsUseLivePreview');
		}

		$this->currentProcess = $this->wire('page')->process;
		if(is_object($this->currentProcess)) $this->currentProcess = $this->currentProcess->className();
		
		if(!$superuser && $this->wire('permissions')->has('page-publish')) {
			// setup hooks for non-superusers on systems that have page-publish permission installed
			$processes = array(
				'ProcessPageList',
				'ProcessPageLister',
				'ProcessPageListerPro',
				'ProcessPageEdit',
				'ProcessProDrafts',
			);
			if(in_array($this->currentProcess, $processes)) {
				$this->addHookBefore('Page::editable', $this, 'hookBeforePageEditable');
				$this->addHookAfter('Page::editable', $this, 'hookAfterPageEditable');
				$this->addHookAfter('User::hasPagePermission', $this, 'hookAfterUserHasPagePermission');
			}
		}
		
	}
	
	/**
	 * Hook before Page::editable applicable for non-superuser installations with page-publish permission
	 *
	 * Temporarily makes page have unpublished status, so that we can make drafts of published pages editable.
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookBeforePageEditable(HookEvent $e) {
		/** @var Page $page */
		$page = $e->object;
		$status = $page->get('status');
		if($status & Page::statusUnpublished) return;
		if(!$this->drafts->allowDraft($page)) return;
		if(!$this->wire('user')->hasPermission('page-publish', $page)) {
			// user lacks page-publish permission to this page
			$page->setQuietly('_PWPD_status', $status);
			$status = $status | Page::statusUnpublished;
			$page->setQuietly('status', $status);
		}
	}

	/**
	 * Hook after Page::editable applicable for non-superuser installations with page-publish permission
	 *
	 * Follows up from the before hook above, restoring the previous status.
	 * Also revokes field edit permission when in ListerPro, to prevent editing of published pages,
	 * since ListerPro does not currently support drafts.
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookAfterPageEditable(HookEvent $e) {

		/** @var Page $page */
		$page = $e->object;
		$status = $page->get('_PWPD_status');
		if($status !== null) $page->setQuietly('_PWPD_status', null); // remove from page

		// if no _PWPD_status was set, then user has page-publish permission to $page, or it is unpublished, so exit
		if($status === null) return;

		// user lacks page-publish permission to this page
		// restore previous status and remove temporary var from page
		$page->setQuietly('status', $status);

		// check if a field argument was provided to the editable() call
		if($e->return && $e->arguments(0)) {
			// field argument provided to $page->editable() call
			if($this->currentProcess == 'ProcessPageListerPro' && !$page->hasStatus(Page::statusUnpublished)) {
				// prevent ListerPro editing of published pages in ListerPro edit mode
				$e->return = false;
			}
		}
	}

	/**
	 * Hook after User::hasPagePermission, for non-superuser users without page-publish permission only
	 *
	 * Revokes permissions for published pages that should require page-publish
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookAfterUserHasPagePermission(HookEvent $e) {

		if(!$e->return) return;

		$page = $e->arguments(1);
		if(!$page) return;

		$permission = $e->arguments(0);
		if($permission instanceof Permission) $permission = $permission->name;
		if($permission == 'page-publish' || $permission == 'page-edit') return;

		// if PWPD_status is set then it indicates user does not have page-publish permission
		$status = $page->get('_PWPD_status');

		// if page is unpublished, and not as a result of our own hooks, then let the permission through
		if($page->hasStatus(Page::statusUnpublished) && $status === null) return;

		// user assumed to have requested permission at this point, determine if it should be revoked
		// the following permissions are those that we will revoke if needed
		static $checkPermissions = array(
			'page-move',
			'page-sort',
			'page-delete',
			'page-trash',
			'page-template',
			'page-hide',
			'page-lock',
			'page-rename',
		);

		if(in_array($permission, $checkPermissions)) {
			/** @var User $user */
			$user = $e->object;
			if(!is_null($status) || !$user->hasPermission('page-publish', $page)) {
				// revoke permission
				$e->return = false;
			}
		}
	}


	/**
	 * Hook before ProcessPageEditImageSelect::execute, ::executeResize and ::executeEdit
	 * 
	 */
	public function hookProcessPageEditImageSelect() {
		// @todo this needs to account for versions
		$id = (int) $this->wire('input')->get('id');
		if(!$id) {
			$file = $this->wire('input')->get('file');
			if($file) {
				list($id, $unused) = explode(',', $file);
				if($unused) {} // ignore
			}
			$id = (int) $id; 
		}
		if($id) {
			$p = $this->wire('pages')->get($id);
			if($this->drafts->allowDraft($p)) {
				if($p->id && !$p->isUnpublished()) $this->drafts->makeDraftPage($p);
			}
		}
	}

	/**
	 * Hook to ProcessPageEdit::loadPage()
	 *
	 * Populate draft content to the $page being edited.
	 *
	 * @param HookEvent $e
	 * @throws WireException
	 *
	 */
	public function hookEditLoadPage(HookEvent $e) {

		/** @var ProDraftPage $page */
		$page = $e->return;
		$this->editPage = $page; 
		$input = $this->wire('input');
		$drafts = $this->drafts;
		$deleteDraft = false;
		$autosave = false;
		$publishable = $page->publishable();
		
		$this->wire('modules')->get('JqueryUI')->use('modal');
		$this->wire('modules')->get('JqueryCore')->use('cookie');
	
		// if drafts not allowed for this page, exit now
		if(!$this->drafts->allowDraft($page)) return;
	
		/*
		if($input->post('submit_save') && !$user->hasPermission('page-publish', $page)) {
			if($this->wire('permissions')->has('page-publish')) {
				throw new WireException("You do not have permission to publish this page");
			}
		}
		*/

		// optional notes that can accompany request_publish or request_reject_publish
		$notes = $this->wire('sanitizer')->textarea($input->post('pwpd_notes'));
		
		if($input->post('request_publish')) {
			$draft = $page->draft();
			$input->post->submit_save_draft = 1;
			$draft->setMeta('notes', $notes);
			if($this->drafts->requestPublish($draft)) {
				// $this->drafts->message($this->_('Publish has been requested'));
			}
			
		} else if($input->post('request_cancel_publish')) {
			$draft = $page->draft();
			if($page->isUnpublished()) {
				$input->post->submit_save = 1;
			} else {
				$input->post->submit_save_draft = 1;
				if($input->post('request_cancel_publish') === 'delete') $deleteDraft = true; 
			}
			$this->drafts->cancelRequestPublish($draft);
			
		} else if($input->post('request_reject_publish') && $publishable) {
			$draft = $page->draft();
			$input->post->submit_save_draft = 1;
			$draft->setMeta('notes', $notes);
			$this->drafts->rejectRequestPublish($draft); 
			if($input->post('request_reject_publish') === 'delete') {
				$deleteDraft = true;
			} else {
				$message = $this->_('The draft has been rejected.') . ' ';
				if(!$page->isUnpublished()) {
					$message .= $this->_('If you also want to abandon the draft changes, click the “Abandon” link.');
				}
				$this->drafts->message($message);
			}
		}
		
		if($input->post('submit_save_draft')) {
			// SAVE DRAFT
			// set a _wasDraft var for inspection by ProDraftsHooks::hookFieldtypeSavePageField
			if(!$page->hasStatus(Page::statusUnpublished)) {
				$wasDraft = $drafts->hasDraft($page) ? true : false;
				$page->setQuietly('_wasDraft', $wasDraft);
				// save draft requested
				$drafts->makeDraftPage($page);
			}

			// make ProcessPageEdit proceed with its regular save by making it think the submit_save 
			// button was clicked rather than the submit_save_draft button
			$input->post->submit_save = 1;
			$drafts->debugMessage('Save draft', __CLASS__ . '.' . __FUNCTION__);

			// exclude autosave fields that match the autosaveNotRegex strings
			if($input->post('submit_save_draft') == 'autosave') {
				$autosave = true;
				foreach(array_keys($_POST) as $name) {
					if(!$this->drafts->allowAutosave($name)) {
						unset($_POST[$name]);
						$input->post->__unset($name);
					}
				}
			}

			if($this->drafts->repeaterSupport && !$autosave && $input->post->count()) {
				// prevent repeater items from being deleted right now, but remember the IDs for storage in draft
				$this->repeaters->processPageEditPostRequest($page); 
			}


		} else if($this->wire('config')->ajax && isset($_SERVER['HTTP_X_FIELDNAME'])) {
			// AJAX FILE UPLOAD
			if(!$page->isUnpublished()) {
				$fieldName = $this->wire('sanitizer')->fieldName($_SERVER['HTTP_X_FIELDNAME']);
				if(!$this->drafts->repeaterSupport && strpos($fieldName, '_repeater') && ctype_digit(substr($fieldName, -1))) {
					// don't allow a draft to be made from a file field in a repeater, if repeater support disabled
				} else {
					$drafts->makeDraftPage($page);
				}
			}

		} else if($input->post('submit_delete_draft') && $input->post('delete_draft')) {
			// DELETE DRAFT
			// delete draft requested and confirmed
			$deleteDraft = true;

		} else {
			// REGULAR LOAD OR PUBLISH REQUESTED
			// populate draft only if there is already one
			// @todo for versions see EXTRAS #1
			
			/** @var ProDraft $draft */
			$draft = $page->draft();
			$this->draft = $draft;
			$isUnpublished = $page->isUnpublished();
			$submitPublish = $input->post('submit_publish');
			$submitSave = $input->post('submit_save');
			
			if(!$publishable && !$draft->exists() && !$isUnpublished) {
				// regular page edit, where edit is only allowed if page is a draft
				if($input->post->count()) {
					$drafts->makeDraftPage($page);
				}
			}
			
			if($draft->exists()) {
				if($submitSave) {
					// publish requested, save unpopulated page to new version
					if($this->drafts->maxVersions > 0) {
						$version = $this->drafts->savePageVersion($page, $draft->changes);
						$this->drafts->debugMessage("Saved page version ($version)", __FUNCTION__);
					}
					$submitPublish = true; 
				}
				$draft->populatePage();
				// see EXTRAS #2
			}

			// if save requested and page is a draft, publish it
			if($submitPublish && $publishable) {
				$draftApproved = false;
				if($isUnpublished) {
					// ...	
					if($draft->requestUser->id) {
						$this->drafts->approveRequestPublish($draft);
						$draftApproved = true;
					}
				} else if($draft->exists()) {
					$page->setQuietly('_wasDraft', true);
					// publish draft
					if($draft->requestUser->id) {
						$this->drafts->approveRequestPublish($draft);
						$draftApproved = true;
					}
					$draft->publish = true;
					// mark all fields in the draft as changed
					foreach($draft->changes() as $change) {
						$page->trackChange($change);
					}
				} 
				if($draftApproved) {
					$this->drafts->message($this->_('The draft has been approved and published.'));
				}
			}
		}
		
		if($deleteDraft && $this->drafts->hasDraftPermission($page, 'delete')) {
			$drafts->deleteDraft($page);
			$drafts->debugMessage('Delete draft', __CLASS__ . '.' . __FUNCTION__);
		}
	}

	/**
	 * Hook to ProcessPageEdit::buildForm()
	 *
	 * Adds a "Save Draft" button
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookEditBuildForm(HookEvent $e) {

		/** @var ProcessPageEdit $process */
		$process = $e->object;
		
		/** @var ProDraftPage $page */
		$page = $process->getPage();
		if(!$this->drafts->allowDraft($page)) return;
		
		/** @var InputfieldForm $form */
		$form = $e->return;
		
		$isDraft = $page->isDraft();
		$wasDraft = $page->get('_wasDraft');

		// exclude page editors that are not in contexts we support
		$context = $this->wire('input')->get('context');
		if($context == 'PageTable' || ($context == 'repeater' && !$this->drafts->repeaterSupport)) {
			return; 
		}
	
		// make sure that pwpd_livepreview is retained between PageEdit requests
		if($this->isLivePreview) {
			$form->set('action', $form->get('action') . '&pwpd_livepreview=1');
		}

		// updates to form specific to non-processing state
		if(!$this->processing && ($this->draft || $this->drafts->livePreview)) {
			$this->pageEditBuildForm($page, $form);
			// add ProDrafts notification to top
			if(!$this->wire('input')->get('uploadOnlyMode')) { 
				// @todo support live preview with unpublished pages
				$this->wire('adminTheme')->addHookAfter('getExtraMarkup', $this, 'hookAdminThemeGetExtraMarkup');
			}
		}

		if(!$page->isUnpublished()) { // && !$this->processing) {
			// identify fields from the form that cannot be drafted
			$publishable = $page->publishable();
			
			foreach($this->drafts->getDisallowFields() as $name) {
				if($name == 'name') continue;

				/** @var Inputfield $inputfield */
				$inputfield = $form->getChildByName($name);
				if(!$inputfield) continue;
				
				$inputfield->addClass('InputfieldNoProDraft', 'wrapClass');

				// if processing form now, and wasn't previously a draft, allow non-editable fields to be processed
				// if($this->processing && $wasDraft === false) continue;

				if($isDraft || $wasDraft) {
					$inputfield->required = false;
					if($inputfield->notes) $inputfield->notes .= "\n";
					if($publishable) {
						$inputfield->notes .= $this->_('Changes to this field publish immediately');
					} else {
						$inputfield->notes .= $this->_('This field cannot be edited in a draft');
						$inputfield->collapsed = Inputfield::collapsedYesLocked; 
					}
				}
			}
		}
	
		if($this->drafts->allowAutosave()) {
			// identify fields that may not be autosaved
			foreach($this->drafts->autosaveNotField as $f) {
				list($id, $name) = explode('-', $f);
				$inputfield = $form->getChildByName($name);
				if(!$inputfield) {
					$field = $this->wire('fields')->get((int) $id);
					if($field->name != $name) $inputfield = $form->getChildByName($field->name);
				}
				if(!$inputfield) continue;
				$inputfield->addClass('InputfieldNoAutosave', 'wrapClass');
			}
			// exclude repeater fields from autosave
			foreach($this->drafts->getRepeaterFields() as $name => $field) {
				$inputfield = $form->getChildByName($name);	
				if($inputfield) $inputfield->addClass('InputfieldNoAutosave', 'wrapClass');
			}
		}
	}

	/**
	 * Updates to page edit form when not processing
	 * 
	 * Expects $this->draft to be populated with ProDraft
	 * 
	 * @param ProDraftPage|Page $page
	 * @param InputfieldForm $form
	 *
	 */
	protected function pageEditBuildForm(Page $page, InputfieldForm $form) {

		$draft = $this->draft;
		$isDraft = $page->isDraft();
		$publishRequested = $isDraft && $draft->hasFlag(ProDraft::flagPublishRequested);
		$publishButton = $form->getChildByName('submit_publish');
		$changes = $draft->get('changes');
		$config = $this->wire('config');
		$isUnpublished = false;
		$saveAndUpdateLabel = $this->_('Save + Update');
		$publishLabel = $this->_('Publish');
		$isPublishable = $page->publishable();
		
		// $user = $this->wire('user');
		// $uploadOnlyMode = (int) $this->wire('input')->get('uploadOnlyMode');
		
		// if page is unpublished, then no draft is needed
		if($draft->pageStatus & Page::statusUnpublished) {
			$isUnpublished = true;
			//$this->message($this->_('This page is currently unpublished and not visible on the site.'));
			//return;
		}

		// add custom css/js 
		$config->styles->add($config->urls->ProDrafts . 'ProDraftsPageEditorHooks.css');
		$config->scripts->add($config->urls->ProDrafts . 'ProDrafts.js');
		$config->scripts->add($config->urls->ProDrafts . 'md5.min.js');
		$config->scripts->add($config->urls->ProDrafts . 'ProDraftsPageEditorHooks.js');

		// publish button
		if(!$isUnpublished) {
			if($publishButton) {
				$publishButton->parent->remove($publishButton);
			}
		}
		
		/** @var InputfieldSubmit $submit */
		$submit = $form->getChildByName('submit_save');
		
		if($submit) {
			// Set existing submit button to be a "Publish" button rather than a "Save" button
			// And add a new "Save Draft" button
			$saveDraftLabel = $this->_('Save Draft');
			$saveDraftButton = null;
			$notesUser = null;
		
			if(version_compare($config->version, '3.0.45', '>=')) {
				$this->wire('modules')->get('JqueryUI')->use('vex');
			}
			
			if($isUnpublished) {
				// page is not yet published
				if($isPublishable && !$publishButton) {
					$submit->attr('name', 'submit_publish');
				}
				if($publishRequested && $publishButton) {
					// convert publish button to just be a 'Save + Update' button
					$publishButton->value = $saveAndUpdateLabel;
					$publishButton->attr('name', 'submit_save');
				}
			} else {
				// page is published: add a secondary "Save Draft" button
				$submit->attr('value', $publishLabel);
				if($isDraft) $submit->addClass('pwpd-submit-publish');
				$submit->icon = 'check-circle';
				/** @var InputfieldSubmit $button */
				$button = $this->wire('modules')->get('InputfieldSubmit');
				$button->attr('id+name', 'submit_save_draft');
				if($publishRequested) {
					$button->attr('value', $saveAndUpdateLabel);
				} else {
					$button->attr('value', $saveDraftLabel);
				}
				$button->icon = $this->drafts->icon;
				$button->addClass('head_button_clone');
				$form->insertAfter($button, $submit);
				$saveDraftButton = $button;
			}
			
			if($isPublishable) {
				// user has page-publish permission
				if($isDraft && $publishRequested) {
					// publish requested for this draft
					$submit->attr('value', $this->_('Approve'));
					$submit->icon = 'check-circle';
					$submit->attr('name', 'submit_publish');
					$submit->addClass('pwpd-submit-publish');
					$this->drafts->warning(sprintf(
						$this->_('User “%s” has submitted this draft for publish/approval. Please Approve or Reject this draft.'),
						$draft->requestUser->name
					));
					$notesUser = $draft->requestUser;
					$button = $this->wire('modules')->get('InputfieldSubmit');
					$button->attr('id+name', 'request_reject_publish');
					$button->attr('value', $this->_('Reject'));
					$button->attr('title', $this->_('Reject the publish request, draft changes will not be deleted.'));
					$button->attr('data-pwpd-title', $this->_('Reject draft'));
					$button->attr('data-pwpd-placeholder', $this->_('Optional notes to draft author'));
					$button->icon = 'times-circle';
					$button->addClass('head_button_clone ui-priority-secondary pwpd-button-prompt');
					/*
					if($this->drafts->hasDraftPermission($page, 'delete')) {
						$button->addActionValue('delete', $this->_('Reject + Delete Draft'), 'trash-o');
					}
					*/
					if($saveDraftButton) $saveDraftButton->addClass('ui-priority-secondary');
					$form->insertAfter($button, $submit);
					
				} else if($isDraft && !$isUnpublished) {
					// draft, but no publish requested
					$submit->attr('value', $publishLabel);
					$submit->attr('name', 'submit_publish'); 
					$submit->addClass('pwpd-submit-publish');
					$submit->addClass('ui-priority-secondary');
				} else {
					// published page with no draft
				}
			} else {
				// user does not have page-publish permission to this page
				if($submit->attr('id') == 'submit_save_unpublished') {
					$submit->removeClass('ui-priority-secondary');
					$saveDraftButton = $submit;
				} else {
					$form->remove($submit); // remove 'Publish' button
				}
				if($isDraft || $isUnpublished) {
					// user is editing a draft
					/** @var InputfieldSubmit $button2 */
					$button2 = $this->wire('modules')->get('InputfieldSubmit');
					if($publishRequested) {
						// draft where publish has been requested: show 'update' and 'cancel' buttons
						$this->drafts->message($this->_('This draft is awaiting approval.'));
						$saveDraftButton->attr('value', $saveAndUpdateLabel);
						$button2->attr('id+name', 'request_cancel_publish');
						$button2->attr('value', $this->_('Withdraw Approval Request'));
						$button2->attr('title', $this->_('Simply cancels the publish request, nothing will be deleted.'));
						$button2->icon = 'times-circle';
						/*
						if($this->drafts->hasDraftPermission($page, 'delete')) {
							$button2->addActionValue('delete', $this->_('Withdraw + Delete Draft'), 'trash-o');
						}
						*/
					} else {
						// draft with no requests: show a 'submit for approval' button
						$button2->attr('id+name', 'request_publish');
						$button2->attr('value', $this->_('Submit for Approval'));
						$button2->attr('title', $this->_('Submits this draft for approval and publish request.'));
						$button2->icon = 'check-circle';
						$button2->addClass('pwpd-button-prompt');
						$button2->attr('data-pwpd-title', $this->_('Request draft publish/approval'));
						$button2->attr('data-pwpd-placeholder', $this->_('Optional note to accompany request'));
						
						// display who rejected the draft
						if($draft->rejectUser->id) {
							$this->drafts->warning(sprintf(
								$this->_('The last request for draft publish/approval was rejected by user “%s”'), 
								$draft->rejectUser->name
							));
							$notesUser = $draft->rejectUser;
						}
					}
					$button2->addClass('head_button_clone ui-priority-secondary'); 
					if($saveDraftButton) $form->insertAfter($button2, $saveDraftButton);
				}
			}
			
			$notes = $draft->getMeta('notes');
			if($notes && $notesUser) {
				$this->drafts->warning(
					sprintf($this->_('User “%s” also added this note:'), $notesUser->name) . ' “' . $notes . '”'
				);
			}
		}
		
		if(!$isUnpublished) $this->pageEditBuildFormDraft($page, $form, $draft);

		$jsconfig = $this->wire('config')->js('ProDrafts');
		if(empty($jsconfig)) $jsconfig = array();
		if(empty($jsconfig['urls'])) $jsconfig['urls'] = array();
		if(empty($jsconfig['labels'])) $jsconfig['labels'] = array();
		
		$notes = $this->_('This field contains unpublished changes');
		$jsconfig['labels']['hasChanges'] = $notes . ' ' . $this->_('(click icon to compare)');
		$jsconfig['labels']['notSupported'] = $this->_('This field does not support drafts. Changes will always save to the live version.');
		$jsconfig['labels']['saveDraftWarn'] = $this->_('Warning, changes to the following fields will be published immediately because they do not support drafts.');
		$jsconfig['labels']['saving'] = $this->_('Saving');
		$jsconfig['labels']['editingDraft'] = $this->_('1 change');
		$jsconfig['labels']['editingDraftNone'] = $this->_('No changes');
		$jsconfig['labels']['editingDraftPlural'] = $this->_('%d changes');
		$jsconfig['labels']['confirmExit'] = $this->_('There are unsaved changes. Are you sure you want to exit?');
		$jsconfig['labels']['confirmPublish'] = $this->_('Are you sure you want to publish?');
		$jsconfig['icon'] = $this->drafts->icon;
		$jsconfig['autosave'] = (int) $this->drafts->autosave;
		$jsconfig['autosaveInterval'] = (int) $this->drafts->autosaveInterval;
		$jsconfig['autosaveDelay'] = (int) $this->drafts->autosaveDelay;
		$jsconfig['livePreview'] = (int) $this->drafts->livePreview;
		$jsconfig['confirmPublish'] = (int) $this->drafts->confirmPublish;
		$jsconfig['unpublished'] = $this->editPage->isUnpublished() ? true : false;
		$jsconfig['debug'] = ProDrafts::debug;
		$jsconfig['urls']['compare'] = "{$this->proDraftsPage->url}changes/?id={$this->editPage->id}&field=";
		
		if($page->isUnpublished()) {
			$unpublishedLabel = $this->_('Unpublished');
			$jsconfig['labels']['editingDraft'] = $unpublishedLabel;
			$jsconfig['labels']['editingDraftNone'] = $unpublishedLabel;
			$jsconfig['labels']['editingDraftPlural'] = $unpublishedLabel;
		}

		foreach($changes as $name) {
			if($name == 'name') $name = '_pw_page_name';
			$field = $form->getChildByName($name);
			if(!$field) continue;
			$field->addClass('InputfieldHasProDraft', 'wrapClass');
			//$jsconfig['urls']["compare_$name"] = "{$this->proDraftsPage->url}changes/?id={$this->editPage->id}&field=$name";
		}
		
		$this->wire('config')->js('ProDrafts', $jsconfig);
	}

	/**
	 * Updates specific to page edit form when not processing AND page is already a draft
	 * 
	 * Expects $this->draft to be populated with ProDraft
	 * 
	 * @param Page $page
	 * @param InputfieldWrapper $form
	 * @param ProDraft|null $draft
	 * 
	 */
	protected function pageEditBuildFormDraft(Page $page, InputfieldWrapper $form, ProDraft $draft = null) {
	
		if($draft === null) $draft = $this->draft;
		$changes = $draft->get('changes');
		
		if($changes || $form) {} // ignore
	
		/*
		// Convert existing "Delete" button to be a "Delete Draft" button
		$submit = $form->getChildByName('submit_delete');
		
		if($submit) {
			$submit->attr('name', 'submit_delete_draft');
			$submit->attr('value', $draft->version > 1 ? $this->_('Delete Version') : $this->_('Delete Draft'));
			$submit->attr('type', 'submit');

			$checkbox = $form->getChildByName('delete_page');
			$checkbox->attr('name', 'delete_draft');
			if($draft->version > 1) {
				$checkbox->attr('value', $checkbox->attr('value') . ':' . $draft->version);
				$checkbox->label = sprintf($this->_('Delete Version %s ?'), $draft->versionName);
				$checkbox->description = $this->_('Check the box to confirm you want to delete this version.');
			} else {
				$checkbox->label = $this->_('Delete/Abandon Draft?');
				$checkbox->description = $this->_('Check the box to confirm you want to abandon this draft. The page itself will remain, only the draft changes will be removed.');
			}
			if(count($changes)) {
				$checkbox->notes = $this->_('Changes have been made to the following fields:') . "\n• " . implode("\n• ", $changes);
			}
		}
		*/
	
		$actions = $this->getAllActions($page, $draft); 
	
		if($draft->version > 1) {
			// draft version notice
			$this->draftNotice = 
				sprintf($this->_('Warning: You are editing version %s.'), $draft->versionName) . ' ' .
				$this->_('Publish/save buttons will make this version the current live/draft page.') . '<br />';
			$this->draftActions['editCurrent'] = $actions['editCurrent'];
			if($page->viewable()) {
				$this->draftActions['viewVersion'] = $actions['viewVersion'];
				$this->draftActions['viewPublished'] = $actions['viewPublished'];
			}
			$this->draftActions['compare'] = $actions['compare'];
			
		} else {
			// regular draft notice
			$this->draftNotice = '';
			if($page->viewable()) {
				if($this->drafts->livePreview) $this->draftActions['livePreview'] = $actions['editPreview'];
				if(!$page->isUnpublished()) $this->draftActions['viewBoth'] = $actions['viewBoth'];
				$this->draftActions['viewDraft'] = $actions['viewDraft'];
				$this->draftActions['viewPublished'] = $actions['viewPublished'];
			}
			$this->draftActions['compare'] = $actions['compare'];
			if($this->drafts->hasDraftPermission($page, 'delete')) {
				$this->draftActions['abandon'] = $actions['abandon'];
			}
		}
	}

	/**
	 * Hook before Session::redirect
	 * 
	 * This is to ensure pwpd_livepreview GET vars are retained in query string
	 * 
	 * @param HookEvent $event
	 * 
	 */
	public function hookSessionRedirect(HookEvent $event) {
		$url = $event->arguments(0);	
		$changed = false;
		if($this->isLivePreview) {
			$url .= (strpos($url, '?') ? '&' : '?') . 'pwpd_livepreview=1';
			$changed = true;
		}
		// ensure 'context' GET param is retained through redirects
		// @todo the PW core should be doing this instead
		$context = $this->wire('input')->get('context');
		if($context && ($context === 'PageTable' || $context === 'repeater') && strpos($url, 'context=') === false) {
			$url .= (strpos($url, '?') ? '&' : '?') . "context=$context";
			$changed = true; 
		}
		if($changed) $event->arguments(0, $url);
	}

	/**
	 * Get action links for draft actions
	 * 
	 * @param Page $page
	 * @param ProDraft $draft
	 * @return array
	 * 
	 */
	protected function getAllActions(Page $page = null, ProDraft $draft = null) {
		
		if(is_null($page)) $page = $this->editPage;
		if(is_null($draft)) $draft = $this->draft;
		
		$modal = (int) $this->wire('input')->get('modal');
		$modal = $modal ? "&modal=$modal" : "";
		$modalClass = 'pw-modal pw-modal-large pwpd-longclick pwpd-if-changes';
		$pageURL = $page->url;
		$draftURL = $draft->url();
		
		$actions = array(
			'viewDraft' => "<a target='_blank' class='$modalClass' href='$draftURL'>" .
				$this->_('Draft') . "</a>",
			'viewVersion' => "<a target='_blank' class='$modalClass' href='$draftURL'>" .
				$this->_('Version') . "</a>",
			'viewPublished' => "<a target='_blank' class='$modalClass' href='$pageURL'>" .
				$this->_('Published') . "</a>",
			'viewBoth' => "<a target='_blank' class='pwpd-if-changes' href='{$this->proDraftsPage->url}view/?id={$this->editPage->id}'>" .
				$this->_('Both') . "</a>",
			'compare' => "<a target='_blank' class='$modalClass' href='{$this->proDraftsPage->url}changes/?id=$page->id&draft=$draft->version'>" .
				$this->_('Differences') . "</a>",
			'abandon' => "<a class='pwpd-if-changes' href='{$this->proDraftsPage->url}action/?action=delete&ids[]=$page->id$modal&edit_after=$page->id'>" .
				$this->_('Abandon') . "</a>",
			'editCurrent' => "<a href='./?id=$page->id$modal'>" .
				$this->_('Return to Current') . "</a>",
			'editPreview' => "<a href='{$this->proDraftsPage->url}edit/?id={$this->editPage->id}'>" .
				$this->_('Live Preview') . "</a>",
		);
		
		return $actions;
	}

	/**
	 * Hook to AdminTheme::getExtraMarkup to add ProDrafts notification to page
	 * 
	 * Expects $this->draftNotice and $this->draftActions to be populated
	 * 
	 * @param HookEvent $e
	 * 
	 */
	public function hookAdminThemeGetExtraMarkup(HookEvent $e) {
		
		if(!count($this->draftActions)) {
			if(!$this->drafts->livePreview) return;
			$actions = $this->getAllActions();
			$this->draftActions = array('editPreview' => $actions['editPreview']);
		}
	
		/** @var AdminTheme $adminTheme */
		$adminTheme = $e->object;
		$adminClass = $adminTheme->className();
		$markup = $e->return;
		$draftIcon = "<i class='fa fa-fw fa-{$this->drafts->icon}'></i>";
		$reno = $adminClass == 'AdminThemeReno' || in_array('AdminThemeReno', class_parents($adminTheme));
		
		if($this->isLivePreview) {
			// live preview
			$adminTheme->addBodyClass('ProDraftsLivePreview');
			$editURL = $this->editPage->editUrl();
			$noticeType = $reno ? 'detail' : 'NoticeWarning';
			$markup['notices'] .= 
				"<div class='ProDraftsNotice $noticeType'>" .
					"<div class='$this->noticeContainerClass'>" .
						"<p>$draftIcon " . 
							"<span class='ProDraftsNoticeText'>$this->draftNotice</span> &nbsp; " . 
							"<a class='pwpd-exit' href='#' data-url='$editURL'>" . 
								$this->_('Exit') . 
								"<i class='fa fa-fw fa-times-circle'></i> " .
							"</a>" . 
							"<a class='pwpd-refresh' href='#'>" . 
								$this->_('Refresh') .
								"<i class='fa fa-fw fa-refresh'></i> " . 
							"</a>" . 
						"</p>" . 
					"</div>" .
				"</div>";
			
		} else {
			// regular editor
			
			$actions = array(
				'view' => array(),
				'action' => array(),
			);
			
			foreach($this->draftActions as $key => $action) {
				if(strpos($key, 'view') === 0) {
					$actions['view'][] = $action;
				} else {
					$actions['action'][] = $action;
				}
			}
		
			$sep = " <span class='pwpd-if-changes'>&nbsp;/&nbsp;</span> ";
			$out =
				"<div class='ProDraftsViews'>" .
					(count($actions['view']) ?	
						"<i class='ProDraftsViewsIcon'><i class='fa fa-fw fa-eye'></i>&nbsp;</i>" . 
						"<strong class='pwpd-if-changes'>" . $this->_('View') . " <i class='fa fa-fw fa-angle-right'></i></strong>&nbsp;" . 
						implode($sep, $actions['view']) : '') . 
					//"&nbsp; &nbsp; " . 
					//"<a id='ProDraftsNoticeCloser' href='#' title='" . 
					//$this->_('Click to hide drafts info') . "'><i class='fa fa-fw fa-times-circle'></i></a>&nbsp;" .
				"</div>" . 
				(count($actions['action']) ? 
					"<div class='ProDraftsActions'>" . 
						"$draftIcon <strong class='ProDraftsNoticeText'>$this->draftNotice</strong> " . 	
						"&nbsp;<i class='fa fa-angle-right'></i>&nbsp; " . 
						implode($sep, $actions['action']) . " &nbsp; " . 
					"</div>" : '');
			
			$class = 'ProDraftsNotice ';
			$class .= count($this->draft->changes()) ? 'pwpd-changes ' : 'pwpd-no-changes ';

			if((int) $this->wire('input')->cookie('ProDraftsNoticeClosed') > 0) {
				$adminTheme->addBodyClass('ProDraftsNoticeClosed');
				$class .= 'pwpd-notice-closed ';
			}

			if($reno) {
				$markup['content'] .=
					"<div class='$class AdminThemeReno detail ui-helper-clearfix'>" .
						$out . 
					"</div>";
			} else {
				$markup['notices'] .=
					"<div class='$class uk-margin-small NoticeWarning'>" .
						"<div class='$this->noticeContainerClass ui-helper-clearfix'>" .
							$out . 
						"</div>" .
					"</div>";
			}
			
			$markup['notices'] .= "<a id='ProDraftsNoticeOpener' class='' href='#' title='" .
				$this->_('Click to show drafts info') . "'>$draftIcon&nbsp;</a>";
		}
		
		$e->return = $markup;
		$this->draftNotice = '';
	}

	/**
	 * Hook to ProcessPageEdit::buildFormView()
	 *
	 * Make the "View" link go to a draft view, when editing a draft
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookEditBuildFormView(HookEvent $e) {
		/** @var ProcessPageEdit $editor */
		$editor = $e->object;
		/** @var Page $page */
		$page = $editor->getPage();
		if(!$this->drafts->allowDraft($page)) return;
		$version = (int) $this->wire('input')->get('draft');
		$draft = $version > 1 ? $page->draft($version) : $page->draft();
		if($draft && $draft->exists()) {
			$url = $page->httpUrl() . "?draft=" . ($draft->version > 0 ? $draft->version : 1);
			$e->arguments(0, $url);
		}
	}

	/**
	 * Hook to ProcessPageEdit::buildFormSettings()
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookEditBuildFormSettings(HookEvent $e) {
		/** @var WirePageEditor $process */
		$process = $e->object;
		/** @var ProDraftPage $page */
		$page = $process->getPage();
		if(!$this->drafts->allowDraft($page)) return;
		$form = $e->return;
		/** @var InputfieldMarkup $f */
		$f = $this->modules->get('InputfieldMarkup');
		$f->attr('id+name', 'ProDraftsVersions');
		$f->label = $this->_('Versions');
		$f->icon = $this->drafts->icon;
	
		/** @var MarkupAdminDataTable $table */
		$table = $this->wire('modules')->get('MarkupAdminDataTable');
		$table->setEncodeEntities(false);
		$table->headerRow(array(
			$this->_x('Version', 'version-th'), 
			$this->_x('Differences from live', 'version-th'), 
			$this->_x('User', 'version-th'),
			$this->_x('Actions', 'version-th')
		));
		
		$versions = $this->drafts->getVersions($page);
		if($this->drafts->hasDraft($page, 0)) {
			array_unshift($versions, $page->draft());
		}
		
		$labels = array(
			'view' => $this->_x('View', 'version-action'),
			'edit' => $this->_x('Edit', 'version-action'),
			'compare' => $this->_x('Differences', 'version-action'),
			'compareIcon' => "<i class='fa fa-exchange fa-fw ui-priority-secondary detail'></i>",
			'modal' => 'pw-modal pw-modal-large', 
		);
		
		$table->row(array(
			$this->_('Live'),
			$this->_('[none]'),
			$page->modifiedUser->name,
			/*
			"<span class='ProDraftsCompare' style='float:right;margin-right:2em;'>" .
				"<input type='radio' class='ProDraftsCompare1' name='_prodraft1' value='1' checked /> $labels[compareIcon] " . 
				//"<input type='radio' class='ProDraftsCompare2' name='_prodraft2' value='1' />" .
			"</span>" . 
			*/
			"<a class='$labels[modal]' href='$page->httpUrl'>$labels[view]</a>" . 
			($page->isDraft() ? " / <a href='./?id=$page->id'>$labels[edit]</a>" : "") 
			//($page->isDraft() ? " / <a class='pw-modal pw-modal-large' href='./?id=$page->id'>$labels[edit]</a>" : "")
		));
		
		$checkedVersion = $this->draft->version;
		if($checkedVersion) {} // future
		
		foreach($versions as $draft) {
		
			//$ver = $draft->version;
			//$checked = $ver == $checkedVersion ? " checked" : "";
			$actions =
				"<a class='$labels[modal]' href='$page->httpUrl?draft=$draft->version'>$labels[view]</a> / " .
				"<a href='./?id=$page->id&draft=$draft->version'>$labels[edit]</a> / " .
				"<a class='$labels[modal]' href='{$this->proDraftsPage->url}changes/?id=$page->id&draft=$draft->version'>$labels[compare]</a>";
				/*
				"<span class='ProDraftsCompare' style='float:right;margin-right:2em;'>" . 
					"<input type='radio' class='ProDraftsCompare1' name='_prodraft1' value='$ver' /> $labels[compareIcon] " . 
					"<input type='radio' class='ProDraftsCompare2' name='_prodraft2' value='$ver'$checked />" . 
				"</span>" . 
				*/
			
			$table->row(array(
				$draft->versionName, 
				$this->wire('sanitizer')->entities(implode(', ', $draft->changes)), 
				$this->wire('users')->get($draft->modified_users_id)->name, 
				$actions
			));
		}

		/*
		$href = $this->proDraftsPage->url . "changes/?id=$page->id&draft=";
		$btn = $this->wire('modules')->get('InputfieldButton');
		//$btn->href = $href;
		$btn->attr('data-basehref', $href);
		$btn->value = $labels['compare'];
		$btn->attr('id', 'ProDraftsCompareButton');
		$btn->icon = 'arrow-up';
		$btn->addClass('pw-modal pw-modal-large');
		$btn->attr('style', 'float:right');
		*/
	
		$info = ProDrafts::getModuleInfo();
		//$f->value = $table->render() . $btn->render() . "<p class='detail'>ProDrafts v$info[version]</p>";
		$f->value = $table->render() . "<p class='detail'>ProDrafts v$info[version]</p>";
		$form->add($f);

	}
	
	/**
	 * Hooks before ProcessProfile::execute and updates profileFields to have ProDrafts checkboxes where appropriate
	 *
	 * @param HookEvent $e
	 *
	 */
	public function hookBeforeUserProfileEdit(HookEvent $e) {

		/** @var ProcessProfile $process */
		$process = $e->object;
		$user = $this->wire('user');
		$profileFields = $process->get('profileFields');

		$pending = 'prodrafts_notify_pending';
		$approve = 'prodrafts_notify_approve';
		
		$install = false;	
		$f = $this->wire('fields')->get($pending);
		if(!$f) {
			$install = true;
		} else {
			$fieldgroup = $this->wire('fieldgroups')->get('user');				
			if(!$fieldgroup->hasField($f)) $install = true;
		}
		if($install) {
			require_once(dirname(__FILE__) . '/ProDraftsInstall.php');
			$installer = new ProDraftsInstall();
			$installer->installApprovals();
		}

		if($user->isSuperuser()) {
			$profileFields[] = $pending;
		} else if($user->hasPermission('page-publish')) {
			$profileFields[] = $pending;
			$profileFields[] = $approve;
		} else if($user->hasPermission('page-edit')) {
			$profileFields[] = $approve;
		}

		$process->set('profileFields', $profileFields);
	}
	
	/* 
	 * EXTRAS #1
     * @todo when we add back version support
	 * 
	$version = (int) $this->wire('input')->get('draft');
	if($version) {
		$draft = $page->draft($version);
		if(!$draft) {
			$this->error($this->_('Unknown version requested')); 
			$draft = $page->draft();
		} else {
		}
	} else {
		$draft = $page->draft();
	}
	 *
	 */
	
	/* 
	 * EXTRAS #2
	 *  
	if($page->modified > $draft->created) {
		$this->warning(
			$this->_('Live version of this page is newer than the draft.') . ' ' . 
			date('Y-m-d H:i:s', $page->modified) . " live modified time<br />" . 
			date('Y-m-d H:i:s', $draft->created) . " draft created time",
			Notice::allowMarkup
		);
	}
	*
	*/

	
}