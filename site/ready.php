<?php namespace ProcessWire;
$laPage = page();
if($laPage->path == '/promenades/') {
    $laPage = page()->children->first;
}
if($laPage->couleur_principale) {
    $c = '#' . $laPage->couleur_principale;
} else {
    $c = '#1a3958';
}
if($laPage->couleur_secondaire) {
    $d = '#' . $laPage->couleur_secondaire;
} else {
    $d = '#1a3958';
}
define('COULEUR_P', $c);
define('COULEUR_S', $d);

wire()->addHookAfter('TextformatterTypographer::customTypographerSettings', function (HookEvent $event) {
    /** @var \PHP_Typography\Settings $settings */
    $settings = $event->return;
    $lang = $event->user->language->name;
    if ($lang === 'fr') {
        $settings->set_hyphenation_language('fr');
    }
});


$forms->addHook('FormBuilderProcessor::processInputDone', function (HookEvent $event) {
    $form = $event->arguments(0);
    if ($form->name != 'astrologer_preferences') {
        return;
    }
    $points = $form->getChildByName('points_to_show');
    $data = $points->attr('value');
    $save = $form->getChildByName('save_to_defaults');
    $dataSave = $save->attr('value');
    $values = [];
    foreach ($data as $datum) {
        $values["points"][] = $datum->id;
    }
    $v = json_encode($values);
    session()->set('points', $v);
    if ($dataSave == "1") {
        $user = $event->wire('user');
        $user->preferences = $v;
        $user->save();
    }
});

$forms->addHookBefore('FormBuilderProcessor::renderReady', function (HookEvent $event) {
    $form = $event->arguments(0);
    if ($form->name != 'astrologer_preferences') {
        return;
    }
    $pr = session()->get('points');
    if ($pr === null) {
        $userPr = user()->preferences;
        if ($userPr != '') {
            session()->set('points', user()->preferences);
        }
    }
    $pr = json_decode($pr,true);
    $pr = implode("\n",$pr['points']);

    $points = $form->getChildByName('points_to_show');
    $points->set('defaultValue',$pr);
});

wire()->addHookAfter('LoginRegisterPro::createdUser', function ($event) {

    $user = $event->arguments(0);
    $userFields = $user->getFields();
    $body = __text('A user was created').'\n';
    $bodyHtml = '<h2>'.__text('A user was created').'</h2>';
    foreach($userFields as $userField){
        if($userField->name != 'pass') {
            $body .= $userField->getLabel() . ' : ' . $user->$userField .'\n';
            $bodyHtml .= '<p>' . $userField->getLabel() . ' : ' . $user->$userField . '</p>';
        }
    }
    $m = wire()->mail->new();
    $m->to('gv@guyerville.com')
        ->from('gv@guyverville.com')
        ->subject(__text('New user'))
        ->body($body)
        ->bodyHTML($bodyHtml)
        ->send();
});
pages()->addHookAfter("Pages::saved", function($event) {
    $page = $event->arguments(0);
    if ($page->template == "promenade") {
        $dateSent = $page->mc_date_sent;
        $toSend = $page->mc_send_campaign;
        if($dateSent === "" && $toSend === 1 ){
            $twitterLoginPage = pages()->get('template=login-twitter');
            $tokenPresent = $twitterLoginPage->oauth_token != "";
            if($tokenPresent){
                $url = $page->url . 'envoi';
               session()->redirect($url);
            }else{
                $message = "Vous n’avez pas encore autorisé ce site web à publier sur votre compte Twitter. Faites-le ici : <a href='". $twitterLoginPage->url . "'>Connexion à Twitter</a>";
                wire()->error($message, Notice::allowMarkup);
                session()->redirect($page->editURL);
            }
        }
    }
});