$jh = jQuery.noConflict();
$jh(function () {
    $jh(document).ready(function () {
        // Handle language switch
        var url = $jh.url();
        // Handle language switch
        var links = $jh("#language-switcher li");
        $jh(links).on("click", function (e) {
            var language = $jh(this).find("a").text().toLowerCase().substring(0, 2);
            var domain = '.' + url.attr('host');
            Cookies.set('chosenlanguage', language, {domain: domain, expires: 365});
        });
    })
});