$(document).ready(function() {

    /*--------------------------------------------*/
    /* Diaporama  */

     $("#foto").slidesjs({
        width: 1200,
        height: 237,
        pagination: {
            active: true,
            effect: 'fade'
        },
        play: {
            active: true,
            effect: 'fade',
            interval: 8000,
            auto: true,
            pauseOnHover: true,
            restartDelay: 8000
        },
        effect: {
            fade: {
                speed: 1000,
                crossfade: true
            }
        }
    });
});