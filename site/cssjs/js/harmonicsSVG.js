$jsvg = jQuery.noConflict();
$jsvg(document).ready(function () {
        let hc = $jsvg('#houses').text().split('|');
        let longitude = $jsvg('#planets').text().split('|');
        let choosenPoints = $jsvg('#choosenPoints').text();
        let planetSymbols = JSON.parse($jsvg('#symbols').text());
        let harmonicData = JSON.parse($jsvg('#angles').text());
        let pl_glyph = [];
        let pl_name = [];
        for (let i = 0; i < planetSymbols.length; i++) {
            pl_glyph[i] = planetSymbols[i]['symbol']
            pl_name[i] = planetSymbols[i]['title']
        }
        //let angularities = JSON.parse($jsvg('#angularities').text());
        let sign_glyph = ["a", "s", "d", "f", "g", "h", "j", "k", "l", "v", "x", "c"];
        let size_of_rect = 600;
        let center_pt = size_of_rect / 2;   // center of circle
        let outer_outer_diameter = size_of_rect - 50;      // diameter of circle drawn
        let diameter = size_of_rect * .8;            // diameter of circle drawn
        let inner_diameter_offset = 75;   // diameter of nextmost inner circle drawn
        let inner_circle = diameter - (inner_diameter_offset * 2);
        let radius = diameter / 2;        // radius of circle drawn
        let inner_radius = inner_circle / 2;        // radius of circle drawn
        let middle_radius = (outer_outer_diameter + diameter) / 4 - 3;   //the radius for the middle of the two outer circles

        let grey = '#d5dee1';
        let white = '#FFF';
        let black = '#000';
        let orange = '#bc5507';
        let lightgreen = '#408479';
        let red = '#953c5d';
        let blue = '#08439c';
        let purple = '#800080';

        let ascendant = hc[0];

        let fontSpec = {
            family: 'astro',
            size: 20,
            anchor: 'middle',
            leading: '1.5em'
        }
        let fontSpecPl = {
            family: 'astro',
            size: 27,
            anchor: 'middle',
            leading: '1.5em'
        }
        let fontText = {
            family: 'Arial',
            size: 16,
            anchor: 'middle',
            leading: '1.5em'
        }
        let fontTextSmall = {
            family: 'Arial',
            size: 12,
            anchor: 'left',
            leading: '1.5em'
        }
        let draw = SVG().addTo('#birthchart').size(size_of_rect, size_of_rect)
        draw.circle(outer_outer_diameter + 10).attr({fill: black}).center(center_pt, center_pt).opacity(.4)
        draw.circle(outer_outer_diameter).attr({fill: grey}).center(center_pt, center_pt)
        draw.circle(diameter).attr({fill: white}).center(center_pt, center_pt)
        //draw.circle(inner_circle+3).attr({fill: black}).center(center_pt, center_pt).opacity(.8)
        draw.circle(inner_circle).attr({fill: grey}).center(center_pt, center_pt).opacity(.8)

        let clr_to_use = '';
        for (let i = 0; i <= 11; i++) {
            let angle = -(ascendant - hc[i]);
            let sign_pos = Math.floor(hc[i] / 30) + 1;
            let sign = Math.floor(hc[i] / 30);
            clr_to_use = getColorToUse(sign_pos)

            // sign glyph
            let xy = display_house_cusp(i, angle);
            draw.text(sign_glyph[sign]).css({fill: clr_to_use}).font(fontSpec).center(xy[0], xy[1]);

            // house cusp degree
            if (i >= 0 && i <= 5) {
                xy = display_house_cusp(i, angle - 5);
            } else {
                xy = display_house_cusp(i, angle + 5);
            }
            // text for cusp
            let textDeg = getRemainingDeg(hc[i]);
            //degrees
            draw.text(textDeg[0]).font(fontText).css({fill: clr_to_use}).center(xy[0], xy[1]);
            //minutes
            let adjustedAngle = getAdjustedAngleForHouses(angle, i);
            xy = display_house_cusp(i, adjustedAngle);
            draw.text(textDeg[1]).font(fontText).css({fill: clr_to_use}).center(xy[0], xy[1]);
        }

        // draw the lines for the house cusps
        for (let i = 0; i <= 11; i++) {
            let angle = ascendant - hc[i];
            let thickness = 2;
            let tint = red;
            let opacity = .25;
            createSimpleLine(inner_diameter_offset, angle, tint, thickness, opacity)
        }
        // Check if some points collide. If so, put symbols on another level
        let levels = [];
        for (let i = 0; i < longitude.length; i++) {
            levels[i] = 0;
        }

        for (let i = 0; i < longitude.length; i++) {
            if (choosenPoints.includes(i.toString())) {
                for (let j = i + 1; j < longitude.length; j++) {
                    if (choosenPoints.includes(j.toString())) {
                        let angle = Math.abs(longitude[i] - longitude[j]);
                        if (angle > 180) angle = 360 - angle;
                        if (angle <= 5 && levels[i] == 0) {
                            levels[j] = 1
                        }
                        if (angle <= 5 && levels[i] == 1) {
                            levels[j] = 0
                        }
                    }
                }
            }
        }
        // draw the planet point
        for (let i = 0; i < longitude.length; i++) {
            if (choosenPoints.includes(i.toString())) {
                placePoint(longitude[i], i, levels[i])
            }
        }
        // draw in the aspect lines
        for (let i = 0; i < harmonicData.length; i++) {
            let x1 = center_pt + (-inner_radius) * Math.cos(deg2rad(harmonicData[i]['p1position'] - ascendant));
            let y1 = center_pt + (inner_radius) * Math.sin(deg2rad(harmonicData[i]['p1position'] - ascendant));
            let x2 = center_pt + (-inner_radius) * Math.cos(deg2rad(harmonicData[i]['p2position'] - ascendant));
            let y2 = center_pt + (inner_radius) * Math.sin(deg2rad(harmonicData[i]['p2position'] - ascendant));
            let classes = harmonicData[i]['class'].join(" ");
            classes = classes + " harmLine";
            let howThick = 1;
            if (classes.includes('tight')) howThick = 3;
            if (classes.includes('close')) howThick = 2;
            draw.line(x1, y1, x2, y2).stroke({color: black, width: howThick}).opacity(1).addClass(classes);
        }
        let harmpoints = $jsvg('.harmPoint');

        harmpoints.on('click', function (e) {
            $jsvg(this).toggleClass('selected');
            let harmNum = '.harm' + $jsvg(this).text();
            draw.find(harmNum).toggleClass('montrer');
        })

        // Functions ==================================

        function getColorToUse(num) {
            let clr_to_use
            switch (num) {
                case 1:
                case 5:
                case 9:
                    clr_to_use = red;
                    break;
                case 2:
                case 6:
                case 10:
                    clr_to_use = blue;
                    break;
                case 3:
                case 7:
                case 11:
                    clr_to_use = lightgreen;
                    break;
                case 4:
                case 8:
                case 12:
                    clr_to_use = orange;
                    break;
            }
            return clr_to_use
        }

        function getAdjustedAngleForHouses(angle, num) {
            let adjustedAngle = 0;
            switch (num) {
                case 1:
                case 2:
                case 3:
                    adjustedAngle = angle + 4;
                    break;
                case 0:
                case 4:
                case 5:
                    adjustedAngle = angle + 5;
                    break;
                case 6:
                    adjustedAngle = angle - 5;
                    break;
                default:
                    adjustedAngle = angle - 5;
                    break;
            }
            return adjustedAngle
        }

        function addZeroIfNeeded(num) {
            let t = "";
            if (num < 10) {
                t = "0" + num.toString();
            } else {
                t = num.toString();
            }
            return t;

        }

        function getRemainingDeg(deg) {
            let degrees = Math.floor(deg % 30);
            let minutes = Math.floor((deg - Math.floor(deg)) * 60)
            return [addZeroIfNeeded(degrees), addZeroIfNeeded(minutes)]
        }

        function display_house_cusp(num, angle) {
            let char_width = 18;
            let half_char_width = char_width / 2;
            let char_height = 12;
            let half_char_height = char_height / 2;

            let xpos0 = -half_char_width;
            let ypos0 = half_char_height;

            let x_adj = -Math.cos(deg2rad(angle));
            let y_adj = Math.sin(deg2rad(angle));

            return [
                center_pt + 10 + xpos0 + x_adj - (middle_radius * Math.cos(deg2rad(angle))),
                center_pt - 5 + ypos0 + y_adj + (middle_radius * Math.sin(deg2rad(angle)))
            ];
        }

        function display_house_number(num, angle) {
            //puts center of character right on circumference of circle
            let x_adj = -Math.cos(deg2rad(angle));
            let y_adj = Math.sin(deg2rad(angle));
            return [
                center_pt + x_adj - ((radius - 10) * Math.cos(deg2rad(angle + 12))),
                center_pt + y_adj + ((radius - 10) * Math.sin(deg2rad(angle + 12)))
            ]
        }

        function placePoint(angle, ind, level) {
            //puts center of character right on circumference of circle
            angle = angle - ascendant;
            if (angle >= 360) {
                angle = angle - 360
            }
            let planetOffset = 30;
            if (level == 1) {
                planetOffset = 50;
            }
            let x_adj = -Math.cos(deg2rad(angle));
            let y_adj = Math.sin(deg2rad(angle));
            let xx = center_pt + x_adj - ((inner_radius + 2) * Math.cos(deg2rad(angle)))
            let yy = center_pt + y_adj + ((inner_radius + 2) * Math.sin(deg2rad(angle)))
            let xx_text = center_pt + x_adj - ((inner_radius + planetOffset) * Math.cos(deg2rad(angle)))
            let yy_text = center_pt + y_adj + ((inner_radius + planetOffset) * Math.sin(deg2rad(angle)))

            draw.circle(8).attr({fill: '#000'}).center(xx, yy).opacity(.5)
            draw.text(pl_glyph[ind]).font(fontSpecPl).center(xx_text, yy_text)
        }

        function createSimpleLine(offset, angle, color = '#000', stroke = 1, opacity = 1) {
            let x1 = center_pt - (radius - offset) * Math.cos(deg2rad(angle));
            let y1 = center_pt - (radius - offset) * Math.sin(deg2rad(angle));
            let x2 = center_pt - radius * Math.cos(deg2rad(angle));
            let y2 = center_pt - radius * Math.sin(deg2rad(angle));
            return draw.line(x1, y1, x2, y2).stroke({color: color, width: stroke}).opacity(opacity);

        }

        function createLine(x1, y1, x2, y2, angle1, angle2) {
            x1 = center_pt - x1;
            y1 = center_pt - y1 * Math.sin(deg2rad(angle1));
            x2 = center_pt - x2;
            y2 = center_pt + y2 * Math.sin(deg2rad(angle2));
            return draw.line(x1, y1, x2, y2).stroke({color: black, width: 2});

        }

        function deg2rad(angle) {
            return angle / 180 * Math.PI
        }


    }
)
