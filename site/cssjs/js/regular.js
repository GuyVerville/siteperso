let $jq = jQuery.noConflict();
$jq(document).ready(function () {
    var url = $jq.url();
    // Handle language switch
    var links = $jq("#language-switcher li");
    $jq(links).on("click", function (e) {
        var language = $jq(this).find("a").text().toLowerCase().substring(0, 2);
        var domain = '.' + url.attr('host');
        Cookies.set('chosenlanguage', language, {domain: domain, expires: 365});
    });

    $jq('#smallCircle img').hover(function () {
        $jq('#lateralMenu').css('opacity', "1");
        $jq('#lateralMenu').css('display', "block");
    }, function () {

    });
    $jq('#smallCircle img').click(function () {
        $jq('#lateralMenu').css('opacity', "1");
        $jq('#lateralMenu').css('display', "block");
    });
    $jq('#closeMenu').click(function () {
        $jq('#lateralMenu').css('opacity', "0");
        $jq('#lateralMenu').css('display', "none");

    })
    if ($jq('#corps p').length) {
        var laDivhasSearch = $jq('#corps').hasClass('search');
        if (laDivhasSearch === false) {
            var lesPages = $jq('#corps p');
            var premiere = lesPages.eq(0);
            var text = premiere.html();
            var lettre = text.charAt(0);
            var couleurP = $jq('#couleurP').text();
            var couleurS = $jq('#couleurS').text();
            var style = '<style>';
            style += '.img_content img{border-color:' + couleurP + '}';
            style += '.dropcap{color:' + couleurS + ' !important}';
            style += 'blockquote{border-color:' + couleurS + ' !important}';
            style += '</style>';
            $jq('head').append(style);
            var first = $jq('<span>' + text.charAt(0) + '</span>').addClass('dropcap');
            premiere.html(text.substring(1)).prepend(first);
            premiere.addClass("lettrine");
            if (lettre == 'J') {
                $jq('.dropcap').addClass('mince');
            }
            if (lettre == 'R') {
                $jq('.dropcap').addClass('large');
            }
        }
    }
})