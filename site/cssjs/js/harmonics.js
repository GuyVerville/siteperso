$jww = jQuery.noConflict();
$jww(document).ready(function (e) {
    let messages = $jww('#messages');
    let buttonSave = $jww('#saveData');
    let buttonWarning = $jww('#warning');
    let action = buttonSave.attr('data-action');
    buttonSave.on("click", function (e) {
        $jww.ajax({
            type: 'POST',
            url: window.location.href,
            dataType: 'text',
            data: {
                'request': action
            },
            beforeSend: function (xhr) {
            },
            success: function (data) {
                messages.toggle();
                buttonSave.hide();
                buttonWarning.hide();
            }
        });
    })


    let yearHarmonic = $jww("#yearHarmonic");
    let monthHarmonic = $jww("#monthHarmonic");
    let dayHarmonic = $jww("#dayHarmonic");
    let year = yearHarmonic.val();
    let month = monthHarmonic.val();
    let day = dayHarmonic.val();

    let incButtonYear = $jww("#plusyearHarmonic");
    incButtonYear.on("click", function (e) {
        e.preventDefault();
        incNumber(yearHarmonic, 2050);
        year = yearHarmonic.val();
    });

    let decButtonYear = $jww("#minusyearHarmonic");
    decButtonYear.on("click", function (e) {
        e.preventDefault();
        decNumber(yearHarmonic);
        year = yearHarmonic.val();
    })

    let incButtonMonth = $jww("#plusmonthHarmonic");
    incButtonMonth.on("click", function (e) {
        e.preventDefault();
        incNumber(monthHarmonic, 12);
        month = monthHarmonic.val();
    });

    let decButtonMonth = $jww("#minusmonthHarmonic");
    decButtonMonth.on("click", function (e) {
        e.preventDefault();
        decNumber(monthHarmonic);
        month = monthHarmonic.val();
    })

    let incButtonDay = $jww("#plusdayHarmonic");
    incButtonDay.on("click", function (e) {
        e.preventDefault();
        incNumber(dayHarmonic, 31);
        day = dayHarmonic.val();
    });

    let decButtonDay = $jww("#minusdayHarmonic");
    decButtonDay.on("click", function (e) {
        e.preventDefault();
        decNumber(dayHarmonic);
        day = dayHarmonic.val();
    })

    function incNumber(buttonElement, limit) {
        buttonElement.val(function (i, oldval) {
            let newval = ++oldval;
            if (newval <= limit) {
                return newval;
            } else {
                return 1;
            }
        });
    }

    function decNumber(buttonElement) {
        buttonElement.val(function (i, oldval) {
            let newval = --oldval;
            if (newval > 0) {
                return newval;
            } else {
                return 1;
            }
        });
    }

    let buttonHarmonicUpdate = $jww("#updateHarmonic");
    buttonHarmonicUpdate.on("click", function () {
        year = yearHarmonic.val();
        month = monthHarmonic.val();
        day = dayHarmonic.val();
        $jww.ajax({
            type: 'POST',
            url: window.location.href,
            dataType: 'text',
            data: {
                'request': 'harmonics',
                'year': year,
                'month': month,
                'day': day},
            beforeSend: function (xhr) {
            },
            success: function (data) {
                let don = JSON.parse(data);
                let th = $jww("#transitHarmonics");
                let dh = $jww("#tableAspectTransits");
                th.html(don[0]);
                dh.html(don[1]);
            }
        });
    })

    let chartInput = $jww('#charts');
    chartInput.on('change',function(e){
        e.preventDefault();
        $jww.ajax({
            type: 'POST',
            url: window.location.href,
            dataType: 'text',
            data: {
                'request': 'synastry',
                'id': chartInput.val(),
            },
            beforeSend: function (xhr) {
            },
            success: function (data) {
                let don = JSON.parse(data);
                let th = $jww("#synastryHarmonics");
                let dh = $jww("#tableAspectSynastry");
                th.html(don[0]);
                dh.html(don[1]);
                $jww("#titleSynastryAspects").css('display','block');
            }
        });
    })
})
