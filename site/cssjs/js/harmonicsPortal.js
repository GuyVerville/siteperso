let $jwq = jQuery.noConflict();

$jwq(document).ready(function () {
    $jwq('#chartTable').DataTable({
        "order": [[1, 'asc'], [2, 'asc']],
    });

});