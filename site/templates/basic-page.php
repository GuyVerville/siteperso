<?php namespace ProcessWire;

?>
<div id="mainArticle">
    <div class="img_content">
        <?php if(count(page()->images)>0){
            echo page()->formatSrcImg(page()->images->first(), 'first',"auto", "auto", true);
        } ?>
    </div>
    <div id="title"><h1><?= page()->title; ?></h1></div>
    <div class="content">
        <div id="corps">
            <?= page()->body; ?>
        </div>
    </div>
</div>