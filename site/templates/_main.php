<?php namespace ProcessWire;

/** Controller code ===================================================================== */
if (config()->ajax() || page()->template->name === "svg")  return ;
$scriptArray = [
    config()->paths->vendors . "lazysizes.min.js",
    config()->paths->vendors . "purl.js",
    config()->paths->vendors . "js.cookie.js",
    config()->paths->scripts . "regular.js",
];
/** View code =========================================================================== */
?>

<!DOCTYPE html>
<html lang="<?= page()->getLanguageCode(user()->language->name) ?>">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link rel='shortcut icon' type='image/x-icon' href='/favicon.ico'/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <?= page()->getSEO(); ?>
    <link href="https://fonts.googleapis.com/css?family=Julius+Sans+One|Lato" rel="stylesheet">
    <?= procache()->link(['scss/principal.scss']) ?>
    <?= procache()->script([config()->paths->scripts . "jquery.js"]) ?>
    <region id="additionalCSS"></region>
    <?php include_once "chunks/google.php"; ?>
</head>
<body id="page-<?= page()->id; ?>" class="<?= page()->getBodyClass() ?>">
<a href="#mainArticle" class="skip">Skip to content</a>
<div id="mainGrid">
    <?php if (page()->id != 1): ?>
        <div id="mainArticle">content</div>
        <div id="arrowDown"><a href="#smallCircle"><img
                        src="/site/templates/visuels/svg/arrow-circle-down.svg"></a>
        </div>
        <div id="mainNav"><?php include_once "chunks/header.php"; ?></div>
        <div id="cRight"></div>
        <footer id="pageFooter">
            <?php include_once "chunks/footer.php"; ?>
        </footer>
    <?php endif; ?>
</div>

<?php
echo procache()->script($scriptArray);
echo page()->getAdminLinks()
?>
<region id="additionalScripts"></region>
<div id='couleurP'><?= page()->couleurPrincipale() ?></div>
<div id='couleurS'><?= page()->couleurSecondaire() ?></div>
<div id="newplace" style="display: none">yes!</div>
</body>
</html>
