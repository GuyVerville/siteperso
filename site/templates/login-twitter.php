<?php namespace ProcessWire;

switch (input()->urlSegment1) {
    case "twitter-login":
        page()->loginTwitter();
        break;
    case "twitter-callback":
        page()->callbackTwitter();
        break;
    default:
        session()->redirect('twitter-login');
        break;
}


