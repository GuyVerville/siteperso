<?php namespace ProcessWire;
$taxos = page()->references('sort=-date_publication');
?>

<div id="mainArticle">
    <h1><?= page()->title ?></h1>
    <div id="taxonomies">
        <div id="taxonomiesLink"><span><a href="<?= page()->parent->url ?>"><?= page()->parent->title ?></a></span></div>
        <?php if (count($taxos) > 0):
            foreach ($taxos as $taxo) : ?>
                <div class="taxo">
                    <div class="photo">
                        <?php if (count($taxo->images) > 0): ?>
                        <img src="<?= $taxo->images->eq(0)->size(150,150)->URL ?>" <?= page()->makeTimestampAlt() ?> />
                        <?php endif; ?>
                    </div>
                    <div class="texte">
                    <h2><a href="<?= $taxo->url ?>"><?= $taxo->title ?> &mdash; <span class="date"><?= $taxo->date_publication ?></span></a></h2>
                        <p><?= $taxo->summary ?></p>
                    <div class='voirPlus'><a href="<?= $taxo->url ?>"><?= page()->getTranslations()->read ?></a></div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <h3>Aucun résultat.</h3>
        <?php endif; ?>
    </div>
</div>


