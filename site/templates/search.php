<?php namespace ProcessWire;
$searchCSS = $css = procache()->link(['scss/search.scss']);
?>
<div id="mainArticle">

    <div class="content">
        <div id="corps" class="search">
            <h1><?= page()->title; ?></h1>
            <?= modules()->get('SearchEngine')->render() ?>
            <?= $searchCSS ?>
        </div>
    </div>
</div>