<?php namespace ProcessWire;


$translations = [
	"available" => __text('Available from your bookseller or in ePub format','available','Available'),
	"review" => __text('Review','review','Review'),
	"critiques" => __text('Reviews','critiques','Reviews'),
	"noresult" => __text('No result','noresult','No result'),
	"result" => __text('Result for','result','Result'),
	"search" => __text('Search','search','Search'),
	"placeholder" => __text('Search text','placeholder','Placeholder'),
	"read" => __text('Read','read','Read'),
	"mytexts" => __text('My published texts','mytexts','My published texts'),
	"recently" => __text('Recently, in the Promenades','recently','Recently, in the promenades'),
	"made" => __text('Made width','made','Made width'),
	"droits" => __text('All rights reserved','droits','All rights'),
	"recent" => __text('Recent blogs','recent','Recent blogs'),
	"articlesfound" => __text('Articles found containing the term','articlesfound','Articles found'),
	"tags" => __text('Tags:','tags','Classé dans :'),
	"alltags" => __text('All tags:','alltags','All tags :'),
	"writer" => __text('writer','writer','Écrivain')
];

