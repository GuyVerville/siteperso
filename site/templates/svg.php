<?php namespace ProcessWire;

/** Controller code =========================================================================== */
$js = procache()->script(['/site/cssjs/js/jquery.js', '/site/cssjs/svg/svg.min.js', '/site/cssjs/js/harmonicsSVG.js']);
$arrayCSS = ['DataTables/datatables.min.css', 'scss/harmonics.scss'];
$css = procache()->link($arrayCSS);

switch (input()->urlSegment(1)) {
    case "view":
        $url2 = input()->urlSegment(2);
        $data = page()->getPositionsByID($url2);

        break;
    default:
        return '<p>' . __text('No dada. Please save your chart to be able to see a wheel chart here.') . '</p>';
        break;
}
$preferences = page()->getPreferences();
$planets = [];
$choosenPoints = [];
$houses = [];
foreach ($data[0] as $key => $datum) {
    $planets[] = $datum['position'];
    if (in_array($datum['id'], $preferences)) $choosenPoints[] = $key;
}
foreach ($data[1] as $datum) {
    $houses[] = $datum['position'];
}

$simpleHarmonics = page()->simpleHarmnicList($data[0], 0);
$angles = json_encode($simpleHarmonics[0]);
$choosenPoints = implode("|", $choosenPoints);
$presentHarmonics = $simpleHarmonics[1];
$planets = implode("|", $planets);
$houses = implode("|", $houses);
$symbols = json_encode(page()->getPlanets());

/** View code =========================================================================== */
?>

<?= $css ?>

<?= $js ?>

<div id="synastryChart">
    <div id="birthchart"></div>
    <div id="pointBar">
        <?php
        for ($i = 1; $i < 33; $i++) {
            $harm = "harm" . $i;
            if (!in_array($harm, $presentHarmonics)) {
                $cl = "harmPointAbsent";
            } else {
                $cl = "harmPoint";
            }
            echo '<div id="' . $harm . '" class="' . $cl . '">' . $i . '</div>';
        } ?>
    </div>
</div>
<div id="birthData">
    <div id="idData" style="display: none">
        <div id="angles"><?= $angles ?></div>
        <div id="planets"><?= $planets ?></div>
        <div id="houses"><?= $houses ?></div>
        <div id="symbols"><?= $symbols ?></div>
        <div id="choosenPoints"><?= $choosenPoints ?></div>
    </div>
</div>
