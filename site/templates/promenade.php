<?php namespace ProcessWire;

switch (input()->urlSegment1) {
    case "envoi":
        page()->postTweetUpdate();
        break;
    default:
        break;
}
/**
 * Promenade template
 */
$taxoTexte = "";
$pageNext = "";
$pagePrev = "";
$pictures = "";
$gallery = "";
$imagePageNext = "";
$imagePagePrevious = "";
$date = "";

$propLanguage = "French";
if (user()->language->name == "default") {
    $propLanguage = "English";
}

if (count(page()->images) > 1 && page()->en_album != 1 && page()->ne_pas_inserer_photo === 0) {
    $pictures .= "<div class='gallerie'>";
    for ($i = 1; $i < count(page()->images); $i++) {
        $pictures .= page()->formatSrcImg(page()->images->eq($i), "album");
    }
    $pictures .= "</div>";
}
if (count(page()->images_album) > 0 && page()->ne_pas_inserer_photo === 0) {
    $gallery .= "<div class='gallerie'>";
    foreach (page()->images_album as $photo) {
        $pictures .= page()->formatSrcImg($photo, "album");
    }
    $gallery .= "</div>";
}

if (count(page()->taxonomie)) {
    $taxoTexte = '<div class="taxonomies"><p>' . page()->getTranslations()->tags;
    $taxos = page()->taxonomie->sort('title');
    foreach ($taxos as $taxo) {
        $taxoTexte .= '<span><a href="' . $taxo->url . '">' . $taxo->title . '</a></span>';
    }
    $taxoTexte .= '</p ></div>';
}
if (page()->prev->id !== 0) {
    $pagePrev = page()->prev;
    if (count($pagePrev->images) > 0) {
        $imagePagePrevious = page()->sizedThumbnail($pagePrev->images->eq(0), 100, 100);
    }
}
if (page()->next->id !== 0) {
    $pageNext = page()->next;
    if (count($pageNext->images) > 0) {
        $imagePageNext = page()->sizedThumbnail($pageNext->images->eq(0), 100, 100);
    }
}
$comments = page()->comments->render();
$commentForm = page()->comments->renderForm();

if (page()->getLanguageCode(user()->language->name) == "fr") {
    $comments = str_replace("Reply</a>", "Répondre</a>", $comments);
    $comments = str_replace("Comments", "Commentaires", $comments);
    $commentForm = str_replace("Post Comment", "Commenter", $commentForm);
    $commentForm = str_replace("Your E-Mail", "Adresse électronique", $commentForm);
    $commentForm = str_replace("Comments", "Commentaires", $commentForm);
    $commentForm = str_replace("E-Mail Notifications", "M'avertir de réponses ", $commentForm);
    $commentForm = str_replace("Off", "Non", $commentForm);
    $commentForm = str_replace("Replies", "Oui", $commentForm);
    $commentForm = str_replace("Submit", "Soumettre", $commentForm);
    $commentForm = str_replace("Thank you, your submission has been saved.", "Merci, votre commentaire a été soumis. Il apparaîtra sous peu.", $commentForm);
    $commentForm = str_replace("Your Name", "Votre nom", $commentForm);
}

?>
<div id="mainArticle" itemscope="http://schema.org/BlogPosting" itemprop="inLanguage" content="<?= $propLanguage ?>">
    <div class="img_content">
        <?php if (wireCount(page()->images)) {
            echo page()->formatSrcImg(page()->images->first, 'first', "auto", "auto", true);
        } else {
            echo "<div style='height:300px'></div>";
        } ?>
    </div>
    <?php if (page()->legende): ?>
        <div class='legende'><?= page()->legende ?></div>
    <?php endif; ?>
    <div itemprop="headline" id="title"><h1><?= page()->title; ?></h1>
        <div itemprop="datePublished" content="<?= page()->date_publication ?>"
             class="signatureDate"><?= page()->date_publication ?></div>
        <?php
        if (page()->date_publication != page()->modification) {
            if (page()->modification != '') {
                $date = str_replace('/', '-', page()->modification);
            }
        }

        if ($date != ""): ?>
            <div itemprop="dateModified" content="<?= $date ?>" class="signatureDateModification">
                Modifié le : <?= page()->modification ?></div>
        <?php endif; ?>
    </div>


    <div class="content">
        <div itemprop="articleBody" id="corps">
            <?= page()->body ?>
            <?= $pictures ?>
            <?= $gallery ?>
            <?= $taxoTexte ?>
        </div>
    </div>
    <div id="navigationArticles">
        <div class="lienG">
            <?php if ($pagePrev): ?>
                <a href="<?= $pagePrev->url ?>">
                    <?= $imagePagePrevious ?>
                    <br/><span
                            class="titre"><?= $pagePrev->title ?><br/>[<?= $pagePrev->date_publication ?>]</span></a>
            <?php endif; ?>
        </div>
        <div class="lienD">
            <?php if ($pageNext): ?>
                <a href="<?= $pageNext->url ?>">
                    <?= $imagePageNext ?>
                    <br/><span
                            class="titre"><?= $pageNext->title ?><br/>[<?= $pageNext->date_publication ?>]</span></a>
            <?php endif; ?>
        </div>
    </div>
    <div id="allComments">
        <?= $comments ?>
        <?= $commentForm ?></div>

</div>