<?php namespace ProcessWire;

/** Controller code =========================================================================== */
$text = "";
$loginRegister = modules()->get('LoginRegisterPro');
$loginRegister->setRedirectUrl('/en/harmonic-studies');
$content = $loginRegister->execute();
$request = sanitizer()->text(input()->get('register'));
$forgot = sanitizer()->text(input()->get('forgot'));
if ($request == "1") {
    $text = __text('This form is used to register for the private section on harmonics. You will be asked to confirm your email address by email.');
} elseif ($forgot == "1") {
    $text = "";
} else {
    $text = __text('This login allows you to create harmonics calculation.');
}
$arrayCSS = ['/site/templates/scss/harmonicsPortal.scss','/site/templates/scss/registration.scss'];
$css = procache()->link($arrayCSS);

/** View code ================================================================================= */
?>

<region id="additionalCSS">
    <?= $css ?>
</region>

<region id="additionalScripts">

</region>

<region id="mainArticle">
    <div class="twoCol">
        <div class="explanation">
            <?= page()->body ?>
        </div>
        <div class="registration">
            <div class="warning">
                <?= $text ?>
            </div>
            <?= $content ?>
        </div>
    </div>
</region>



