<?php namespace ProcessWire;

switch (input()->urlSegment1) {
    case "twitter-login":
        page()->getTwitterCredentials();
        break;
    case "twitter-callback":
        page()->callbackTwitter();
        break;
    default:
        session()->redirect(config()->twitterlogin);
        break;
}


