<?php namespace ProcessWire; ?>
<div id="mainArticle">
    <h1><?= page()->title ?></h1>
    <div id="presentationLivre">
        <div class="livre">
            <?php foreach (page()->children as $livre): ?>
                <a href="<?= $livre->url ?>"><?= page()->pictureThumbnail($livre->images->eq(0)) ?></a>
            <?php endforeach; ?>
        </div>
        <div id="corps" class="presentation">
            <?= page()->body ?>
        </div>
    </div>
</div>
