<?php namespace ProcessWire;?>

<div id="mainGrid" class="home">
    <div id="homeCircles">
        <div id="largeCircle">
            <img src="/site/templates/visuels/site2020/largeCercle.svg" alt="">
        </div>
        <div id="backgroundMenu">
            <div id="menu"><?= page()->getMenu() ?></div>
        </div>
        <div id="smallCircle">
            <img src="/site/templates/visuels/site2020/petitCercle.svg" alt="">
        </div>
        <div id="guyverville">
            <h1 class="nom">
                <a href="/">Guy Verville</a>
            </h1>
            <p class="titre"><?= page()->getTranslations()->writer ?></p>
        </div>
    </div>
</div>

<div id="additionalScripts">
    <?= procache()->script([config()->paths->scripts . "home.js"]) ?>
</div>
