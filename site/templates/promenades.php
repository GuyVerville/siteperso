<?php namespace ProcessWire;
$articles = pages()->find("template=promenade,sort=-date_publication, limit=36");
$pagination = page()->renderPagination($articles);
$read = page()->getTranslations()->read;
$c = 0;
$grid = [];
$taxonomies = pages()->get('template=taxonomies');
foreach ($articles as $article) {

    if (!session()->get('isMobile') && ($c < 4)) {
        $cl = "firstArticles w2";
        $img = page()->sizedThumbnail($article->images->first,150,150);
        $text = $article->summary;
        $r = $read;
    } else {
        $cl = "remainingArticles w1";
        $img = page()->sizedThumbnail($article->images->first,50,50);
        $text = "";
        $r = "";
    }
    $grid[] = [
        "class" => $cl,
        "title" => $article->title,
        "date" => $article->date_publication,
        "url" => $article->url,
        "text" => $text,
        "img" => $img,
        "read" => $r
    ];
    $c++;
}
?>

<div id="mainArticle">
    <h1><?= page()->title ?></h1>
    <h2 id="taxonomiesLinkTop"><span><a href="<?= $taxonomies->url ?>"><?= $taxonomies->title ?></a></span></h2>
    <?= $pagination ?>
    <div class="dynamic-grid">
        <?php foreach ($grid as $item): ?>
            <div class="itemPromenade <?= $item['class'] ?>">
                <div class="miniature">
                    <a href="<?= $item['url'] ?>"><?= $item['img'] ?></a>
                </div>
                <div class="textePromenade">
                    <div class="date"><?= $item['date'] ?></div>
                    <h3><a href="<?= $item['url'] ?>"><?= $item['title'] ?></a></h3>
                    <?php if ($item['text'] !== ""): ?>
                        <p class="text"><a href="<?= $item['url'] ?>"><?= $item['text'] ?></a></p>
                    <?php endif; ?>
                    <?php if($item['read'] != ""): ?>
                        <p class="suite"><a href="<?= $item['url'] ?>"><?= $item['read'] ?></a></p>
                    <?php endif ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?= $pagination ?>
</div>

<region id="additionalCSS">
    <?= procache()->link(['scss/dynamic-grid.scss']) ?>
</region>