<?php namespace ProcessWire;
$view = wire("view");
$page = wire("page");
$view->set('generalClass','normale');
$view->set('id','promenades');
$view->set('billet',$page->children->first);
$view->set('billets',$page->children);
if(count($page->children->first->images)) {
  $image = $page->children->first->images->eq(0);
  $view->set('photo', uneImagePromenade($image));
}