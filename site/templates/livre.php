<?php namespace ProcessWire;
$prix = "$" . page()->prix;
if (page()->getUserLanguage() == "fr") {
    $prix = str_replace('.', ',', page()->prix) . "&nbsp;$";
}
$titreCritiques = page()->getTranslations()->critiques;
if (page()->numChildren < 2) {
    $titreCritiques = page()->getTranslations()->review;
}
?>
<div id="mainArticle" class="bookDescription" itemscope itemtype="http://schema.org/Book" class="livre">
    <h1><?= page()->title ?></h1>
    <div id="bookDescription">
        <div class="imageLivre">
            <?= page()->uneImageMeta(page()->images->eq(0)) ?>
            <div class="details">
                <p itemprop="name" class="itemMetaHidden"><?= page()->title ?></p>
                <p itemprop="author" class="itemMetaHidden">Guy Verville</p>
                <p itemprop="inLanguage" class="itemMetaHidden">French</p>
                <?php if (page()->ISBN): ?>
                    <p itemprop="isbn">ISBN : <?= page()->ISBN ?> | <span
                                itemprop="numberOfPages"><?= page()->nombre_pages ?></span> pages | <span
                                itemprop="price" content="<?= page()->prix ?>"><?= $prix ?></span> | <span
                                itemprop="datePublished"><?= page()->annee_publication ?></span> | <span
                                itemprop="publisher"><?= page()->editeur ?></span>
                    </p>
                <?php endif ?>
                <div class="autreDetails">
                    <?= page()->details ?>
                </div>
            </div>
        </div>
        <div class="texteLivre">
            <div id="corps"><?= page()->body ?></div>
            <?php if (page()->extrait) {
                echo '<div class="extrait">' . page()->extrait . '</div>';
            }
            ?>
            <?php if (page()->numChildren > 0): ?>
                <div class="critiques">
                    <h2><?= $titreCritiques ?></h2>
                    <?php foreach (page()->children as $uneCritique): ?>
                        <div class="critique">
                           <h3><span><?= $uneCritique->title ?></span></h3>
                            <?= $uneCritique->body ?>
                            <p class="auteur">
                                <?= $uneCritique->auteur_critique ?> (<?= $uneCritique->date_publication ?>)
                            </p>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>
