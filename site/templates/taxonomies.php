<?php namespace ProcessWire;

$taxonomies = page()->children('sort=title');
$letter = "A";
$index = "<div class='taxoLettre'><span>{$letter}</span></div>";
foreach ($taxonomies as $taxonomy) {
    $taxoLetter = page()->replaceAccents(strtoupper(mb_substr($taxonomy->title, 0, 1,"UTF-8")));
    if ($taxoLetter !== $letter) {
        $letter = $taxoLetter;
            $index .= "<div class='taxoLettre'><span>{$letter}</span></div>";
    }
    $index .= "<div class='taxoData'><a href='{$taxonomy->url}'>" . ucfirst($taxonomy->title) . "</a></div>";
}
?>
    <div id="mainArticle">
        <h1><?= page()->title ?></h1>
        <div id="taxonomiesList">
            <?= $index ?>
        </div>
    </div>
