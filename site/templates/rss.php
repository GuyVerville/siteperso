<?php /*namespace ProcessWire;

// retrieve the RSS module
$rss = modules()->get("MarkupRSS");

// configure the feed. see the actual module file for more optional config options.
$rss->title = "Les plus récentes promenades";
$rss->description = "Les plus récentes promenades";

// find the pages you want to appear in the feed.
// this can be any group of pages returned by $pages->find() or $page->children(), etc.
$items = pages()->find("limit=10, template=promenade, sort=-published");

// send the output of the RSS feed, and you are done
$rss->render($items);*/

namespace ProcessWire;


/**
 * Class rssFeedController
 * @package ProcessWire
 */
Class rssFeedController {


	/**
	 * @var string
	 */
	protected $language = "";

	/**
	 * @var string
	 */
	protected $host = "";

	/**
	 * @var string
	 */
	protected $dateFormat = "";


	/**
	 * Set the default config data
	 */
	public function __construct() {

		$page = wire('page');

		$this->host = $page->parent->httpUrl;

		if($this->language == 'en') {
			$this->dateFormat = "%a, %b %d %Y %X %Z";
		} else {
			$this->dateFormat = "%a, %d %b %Y %X %Z";
		}
	}

	public function render(): void {
		$this->language = 	user()->language->name === "fr" ? "fr" : "en";
		$content = pages()->find('limit=10, template=promenade, sort=-published');
		/** @var \ProcessWire\ProcessWire $view */
		$this->renderContent($content);
		exit;
		//$view->set('content', $this->renderContent($content));
	}


	/**
	 * Render RSS header
	 */
	protected function renderHeader($lastModifiedDate) {
		$page = wire("page");
		$promenade = pages()->get("template=promenades");
		$cat = 	user()->language->name === "fr" ? "Blogue" : "Blog";

		$out = "<?xml version='1.0' encoding='utf-8' ?>\n";

		$out .= "<rss version='2.0' xml:base='" . $page->httpUrl . "' xmlns:atom='http://www.w3.org/2005/Atom'
         xmlns:dc='http://purl.org/dc/elements/1.1/'>\n" . "<channel>\n" . "\t<title>$promenade->title | Guy Verville</title>\n" . "\t<description>$promenade->summary</description>\n" . "\t<link>$this->host</link>\n" . "\t<atom:link rel='self' href='" . $page->httpUrl . "'/>\n" . "\t<language>$this->language</language>\n";


		$out .= "<category>$cat</category>";

		$out .= "\t<image>
                <url>$this->host/favicon.ico</url>
                <title>Guy Verville</title>
                <link>$this->host</link>        
                <width>48</width>
                <height>48</height>
            </image>\n";

		$out .= "\t<copyright>@2019 Guy Verville</copyright>\n";
		$out .= "\t<managingEditor>gv@guyverville.com</managingEditor>\n";
		$out .= "\t<webMaster>gv@guyverville.com</webMaster>\n";
		$out .= "\t<ttl>60</ttl>\n";
		$out .= "\t<lastBuildDate>" . strftime($this->dateFormat, $lastModifiedDate) . "</lastBuildDate>\n";

		return $out;
	}

	/**
	 * Render individual RSS item
	 */
	protected function renderItem(Page $page) {

		$title = strip_tags($page->title);
		$title = html_entity_decode($title, ENT_QUOTES, 'UTF-8');
		$title = htmlspecialchars($title, ENT_QUOTES, 'UTF-8');
		$title = str_replace('&#039;', '&apos;', $title);
		$title = htmlspecialchars($title);
		$pubDate = "\t\t<pubDate>" . strftime($this->dateFormat, intval($page->created)) . "</pubDate>\n";


		$authorName = "Guy Verville";

		$link = $page->httpUrl;
		$guid = $page->httpUrl;

		$description = $page->summary;

		$summary = true;
		if($description == "" || $description == null) {
			$summary = false;
			$description = $page->body;
		}

		if($summary == false) {
			$description = $this->truncateDescription(trim($description));
			$description = htmlspecialchars($description);
		}
		$img = "";
		if(count($page->images)) {
			$img = $page->images->eq(0)->width(100)->httpUrl;
		}


		$content = "<content type=\"html\"><img alt=\"\" src=\"$img\" width=\"100\" /><p>$description</p></content>";
		$out = "\t<item>\n" . "\t\t<title>$title</title>\n" . "\t\t<description>$description</description>\n" . "\t\t<link>$link</link>\n" . $authorName . "\t\t<guid>$guid</guid>\n" . $pubDate . $content . "\t</item>\n";
		return $out;
	}

	/**
	 * Render the feed and return it
	 */
	public function renderFeed(PageArray $feedPages) {
		$out = $this->renderHeader($feedPages[0]->blog_publication_date);

		foreach($feedPages as $page) {
			$out .= $this->renderItem($page);
		}
		$out .= "</channel>\n</rss>\n";

		return $out;
	}

	/**
	 * Render the feed and echo it (with proper http header)
	 */
	public function renderContent(PageArray $feedPages) {
		header("Content-Type: application/xml; charset=utf-8;");
		echo $this->renderFeed($feedPages);
		return true;
	}

	/**
	 * Truncate the description to a specific length and then truncate to avoid splitting any words.
	 */
	protected function truncateDescription($str) {

		$maxlen = 1000;
		if(!$maxlen) {
			return $str;
		}

		// note: tags are not stripped if itemDescriptionLength == 0
		$str = strip_tags($str);

		if(strlen($str) < $maxlen) {
			return $str;
		}

		$str = trim(substr($str, 0, $maxlen));

		// boundaries that we can end the summary with
		$boundaries = [
			'. ',
			'? ',
			'! '
		];
		$bestPos = 0;

		foreach($boundaries as $boundary) {
			if(($pos = strrpos($str, $boundary)) !== false) {
				// find the boundary that is furthest in string
				if($pos > $bestPos) {
					$bestPos = $pos;
				}
			}
		}

		// determine if we should truncate to last punctuation or last space.
		// if the last punctuation is further away then 1/4th the total length, then we'll
		// truncate to the last space. Otherwise, we'll truncate to the last punctuation.
		$spacePos = strrpos($str, ' ');
		if($spacePos > $bestPos && (($spacePos - ($maxlen / 4)) > $bestPos)) {
			$bestPos = $spacePos;
		}

		if(!$bestPos) {
			$bestPos = $maxlen;
		}

		return trim(substr($str, 0, $bestPos + 1));
	}
}

(new rssFeedController())->render();

