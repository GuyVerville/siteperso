<?php namespace ProcessWire;

if ($status == UPDATING): ?>
    <button id="messages" class="uk-button uk-button-primary"><?= __("Updated!") ?></button>
    <button id="saveData" class="uk-button uk-button-primary"
            data-action="update"><?= __('Update Data') ?></button>
<?php elseif ($status != NOACTION && $status != EXISTING): ?>
    <button id="messages" class="uk-button uk-button-primary"><?= __("Saved!") ?></button>
    <button id="saveData" class="uk-button uk-button-primary"
            data-action="save"><?= __('Save Data') ?></button>
<?php endif; ?>
<a class="uk-button uk-button-secondary"
   href="/harmonics/edit/<?= $idEdited ?>"><?= __('Edit') ?></a>
<a class="uk-button uk-button-secondary" href="/harmonics/create"><?= __('New chart') ?></a>
<a class="uk-button uk-button-secondary backToPortal"
   href="/harmonic-studies"><?= __('Back to portal') ?></a>
<button class="uk-button uk-button-secondary backToPortalTop" uk-toggle="target: #preferences" type="button"><?= __text('Preferences') ?></button>
<?php if ($status == EXISTING || $status == UPDATING): ?>
    <a class="uk-button uk-button-danger"
       href="/harmonics/delete/<?= $idEdited ?>"><?= __('Delete') ?></a>
<?php endif; ?>
<?php if($status == NOTSAVED): ?>
<div id="warning"><?= __('This chart is not saved yet. Please revise it and then save it.') ?></div>
<?php endif; ?>
