<?php namespace ProcessWire;
if(!user()->isLoggin):
?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-13289199-3"></script>
<script>function gtag() {
        dataLayer.push(arguments)
    }
    window.dataLayer = window.dataLayer || [], gtag("js", new Date), gtag("config", "UA-13289199-3");</script>
<!-- Facebook Pixel Code -->
<script>!function (e, t, n, c, o, a, f) {
        e.fbq || (o = e.fbq = function () {
            o.callMethod ? o.callMethod.apply(o, arguments) : o.queue.push(arguments)
        }, e._fbq || (e._fbq = o), o.push = o, o.loaded = !0, o.version = "2.0", o.queue = [], (a = t.createElement(n)).async = !0, a.src = "https://connect.facebook.net/en_US/fbevents.js", (f = t.getElementsByTagName(n)[0]).parentNode.insertBefore(a, f))
    }(window, document, "script"), fbq("init", "931442880348269"), fbq("track", "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=931442880348269&ev=PageView&noscript=1"/></noscript>
<!-- End Facebook Pixel Code -->
<?php
endif;
?>
