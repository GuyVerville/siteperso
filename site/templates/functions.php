<?php namespace ProcessWire;

/** @var \ProcessWire\ProcessWire $page */

$laPage = page();
if($laPage->path == '/promenades/') {
	$laPage = page()->children->first;
}
if($laPage->couleur_principale) {
	$c = '#' . $laPage->couleur_principale;
} else {
	$c = '#1a3958';
}
if($laPage->couleur_secondaire) {
	$d = '#' . $laPage->couleur_secondaire;
} else {
	$d = '#1a3958';
}
define('COULEUR_P', $c);
define('COULEUR_S', $d);

/**
 * @param \ProcessWire\Page $p
 *
 * @return string
 */
function tousLesLivres() {
	$miniatures = pages()->get('/livres')->children;
	$compteur = 0;
	$contenu = '';
	foreach($miniatures as $miniature) {
		$compteur++;
		$contenu .= '<div class="grille';
		if($compteur == 6) {
			$contenu .= ' dernier';
		}
		$contenu .= '">';
		$contenu .= '<div class="interieur"><a href="' . $miniature->url . '">';
		$contenu .= uneImageSimple($miniature->miniature->eq(0));
		$contenu .= '</a></div>';
		$contenu .= '<div class="legendeLivre"><a href="' . $miniature->url . '">' . $miniature->title . '</a></div></div>';
	}
	return $contenu;
}

/**
 * @param $image
 *
 * @return string
 */
function checkIfDescription($image) {
	/** @var \ProcessWire\Sanitizer $sanitizer */
	$san = wire("sanitizer");
	$desc = $image->description;
	if($desc == "") {
		$desc = "picture for " . $san->pageName($image->page->title);
	}
	return $desc;
}
function fairePromenadeFrontale($offset) {
	$generalTranslations = pages()->get("template=translations")->translated_texts;

	$output = '<div class="itemPromenade">';
	$output .= '<div class="photoPromenade">';
	$pagePromenade = pages()->get('/promenades/');
	$laPage = $pagePromenade->children->eq($offset);
	$output .= '<a href="' . $laPage->url . '">' . frontThumbnail($laPage->images->eq(0)) . '</a>';
	$output .= '</div><div class="textePromenade">';
	$output .= '<h3><a href="' . $laPage->url . '"><span class="date">' . $laPage->date_publication;
	$output .= '</span>' . $laPage->title . '</a></h3>';
	$output .= '<p>' . $laPage->summary . '</p>';
	$output .= '<p class="suite"><a href="' . $laPage->url . '">'. $generalTranslations->read.'</a></p>';
	$output .= '</div></div>';
	return $output;
}
function uneImage($image) {
	return '<img loading="lazy" itemprop="sharedContent" src="' . $image->url . '" width="' . $image->width . '" height="' . $image->height . '" alt="' . checkIfDescription($image) . '" srcset="' . $image->srcset() . '" />';
}

function uneImageAdaptee($image) {
	$photo = '<img loading="lazy" src="' . $image->url . '" alt="' . checkIfDescription($image) . '"';
	$photo .= ' srcset="' . $image->srcset() . '" />';
	return $photo;
}
function frontThumbnail($image) {
	$photo = '<img loading="lazy" src="' . $image->url . '" alt="' . checkIfDescription($image) . '"';
	$photo .= ' srcset="' . $image->srcset( "267 1024w, 320") . '" />';
	return $photo;
}

/**
 * @param $image
 *
 * @return string
 */
function uneImagePromenade($image) {
	return '<img loading="lazy" id="photo" itemprop="sharedContent" src="' . $image->url . '" width="' . $image->width . '" height="' . $image->height . '" alt="' . checkIfDescription($image) . '" srcset="' . $image->srcset() . '" />';
}

/**
 * @param $image
 *
 * @return string
 */
function uneImageMeta($image) {
	return '<img loading="lazy" itemprop="image" src="' . $image->url . '" width="' . $image->width . '" height="' . $image->height . '" alt="' . checkIfDescription($image) . '">';
}

/**
 * @param $image
 *
 * @return string
 */
function uneImageSimple($image) {
	$check = checkIfDescription($image);
	return '<img loading="lazy" itemprop="sharedContent" src="' . $image->url . '" alt="' . $check . '">';
}

/**
 * @param $image
 *
 * @return string
 */
function uneImageAlbum($image) {
	return '<a href="' . $image->url . '" data-fancybox="images"><img src="' . $image->getCrop('petit')->url . '" alt="' . checkIfDescription($image) . '" /></a>';
}


/**
 * Language switcher
 *
 * @param \ProcessWire\Page $page
 *
 * @return object
 */
function getLanguageSwitcher(Page $page) {
	$languages = wire("languages");
	$languageSwitcher = [];

	foreach($languages as $language) {
		if($page->viewable($language) === false) {
			continue;
		}

		$name = $language->name;

		$languageSwitcher[] = [
			"title" => $name,
			"code" => getLanguageCode($name),
			"page_url" => $page->localUrl($language)
		];
	}
	return $languageSwitcher;
}

function getLanguageCode(string $languageName) {
	$code = "";

	if($languageName === "fr") {
		$code = "fr";

	} else if($languageName === "default") {
		$code = "en";
	}

	return $code;
}

