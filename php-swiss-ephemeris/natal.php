<?php
$birthDate = $_GET["birthdate"];
$birthTime = $_GET["birthtime"];
$h_sys = $_GET["h_sys"];
$lng = $_GET["lng"];
$lat = $_GET["lat"];
$pl = $_GET["pl"];
$ast = $_GET["ast"];
// path to swiss ephemeris library files
$sweph = './sweph/';

putenv("PATH=$sweph");
switch ($ast) {
    case '0':
        exec("swetest -edir$sweph -b$birthDate -ut$birthTime -p$pl -fPlsj -eswe -g, -head", $out);
        break;
    case '1':
        exec("swetest -edir$sweph -b$birthDate -ut$birthTime -fPlj -house$lng,$lat,$h_sys -eswe -g, -head", $out);
        break;
}

# OUTPUT ARRAY
echo json_encode($out);
exit;
